<?php
include("includes/header.php");
?>
		<div id="body_container" style="">
			<div id="body_right">
				<div id="fixedscroll">
					<script src="js/json2.js"></script>
					<script src="js/dumbFormState-1.js"></script>
					<?php
						include('includes/left_menu.php');
					?>
				</div>
			</div>
			<div id="body_left"> 
				<h2 style="text-align:left; margin-left:25px;"><br>Links<br><br></h2>
				<div id="page_content">
					<p><a href="http://www.racgp.org.au/" target="_blank" style="color:black;text-decoration:none;">The Royal Australian College of General Practitioners</a></p>
					<p><a href="http://www.medicareaustralia.gov.au/" target="_blank" style="color:black;text-decoration:none;">Medicare Australia</a></p>
					<p><a href="http://www.uwa.edu.au/" target="_blank" style="color:black;text-decoration:none;">The University of Western Australia</a></p>
					<p><a href="http://www.medicalobserver.com.au/" target="_blank" style="color:black;text-decoration:none;">Medical Observer 	&#8208; for Australian General Practitioners</a></p>
					<p><a href="http://www.australiandoctor.com.au/" target="_blank" style="color:black;text-decoration:none;">Australian Doctor</a></p>
					<p><a href="http://www.gpet.com.au/" target="_blank" style="color:black;text-decoration:none;">Australian General Practice Training</a></p>
					<p><a href="http://www.wadems.org.au/" target="_blank" style="color:black;text-decoration:none;">Western Australian Deputising Medical Service</a></p>
				<br><br><br>	
				</div>
	

﻿			</div>
		</div>
<?php
include("includes/footer.php");
?>