<div id="body_left">
	<?php if ($ban_image != '' && is_file(BANNER_PATH.$ban_image)){
	?>
		<img src="<?php echo BANNER_PATH.$ban_image ?>" style="width:743px;height:216px;">  
	<?php
	}
	?>
	<h2 style="<?php echo $style;?>"><?php echo str_replace("Specialist Referral Services (for our existing patients only)", "Affiliated Services", $page_title);?></h2>
	<div id="page_content" style="min-height: 500px;">
		<?php echo stripslashes($page_content); ?>
		<div class="col-md-12" style="text-align: center;margin:auto;float:none;">
			<h4 style="font-size: 19px;font-weight:600;">Download the Walk-in GP app now for the real-time queue.</h4>
			<div class="col-md-12" id="nurseicon">
				<a href="javascript:void(0);"><img src="images/qr_code_ios.png" class="img-responsive qrcodeimg"></a>
				<a  href="javascript:void(0);"><img src="images/qr_code_android.png" class="img-responsive qrcodeimg appimageleft"></a><br>
				<a href="https://itunes.apple.com/us/app/the-walk-in-gp/id1268271518?ls=1&mt=8" target="_balnk"><img src="images/app-store.png"></a>
				<a href="https://play.google.com/store/apps/details?id=com.thewalkingp" target="_balnk"><img src="images/playstore.png" class="appimageleft"></a>
			</div>
			<div class="col-md-12" id="mobilenurseicon">
				<a href="javascript:void(0);"><img src="images/qr_code_ios.png" class="img-responsive qrcodeimg"></a>
				<a  href="javascript:void(0);"><img src="images/qr_code_android.png" class="img-responsive qrcodeimg appimageleft"></a><br>
				<a href="https://itunes.apple.com/us/app/the-walk-in-gp/id1268271518?ls=1&mt=8" target="_balnk"><img src="images/app-store.png"></a>
				<a href="https://play.google.com/store/apps/details?id=com.thewalkingp" target="_balnk"><img src="images/playstore.png" class="appimageleft"></a><br>
				<a  href="javascript:void(0);"><img src="images/blue3.png" style="width: 100px;margin-bottom: -20px;" class="img-responsive qrcodeimg appimageleft"></a>
			</div>
		</div>
	</div>
</div>