<!--=== Navigation ===-->
<ul id="leftmenu_nav">
 <li>
		<a href="queue/" target="_blank" style="text-shadow: none;">
			Real Time Queue<i class="icon-angle-right"></i>
		</a>
	</li>
	 <li>
		<a href="patient_information.php">
			Information For Patients<i class="icon-angle-right"></i>
		</a>
	</li>	
	<li>
		<a href="emp_offer.php">
			Employment Opportunity<i class="icon-angle-right"></i>
		</a>
	</li>
    <li>
		<a href="services.php">
			Affiliated Services<i class="icon-angle-right"></i>
		</a>
	</li>
	<li>
		<a href="opening_hours.php">
			Opening Hours<i class="icon-angle-right"></i>
		</a>
	</li>	
	<li><a href="contact_us.php">
			Contact Us<i class="icon-angle-right"></i>
		</a>		
	</li>	
	<li>	
		<a href="faq.php">
			FAQs<i class="icon-angle-right"></i>
		</a>
	</li>	
	<li>
		<a href="links.php">
		
			Links<i class="icon-angle-right"></i>
		</a>
	</li>	
	
	

	
</ul>
<!--=== /Navigation ===-->