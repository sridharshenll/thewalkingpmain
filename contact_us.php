<?php
include('queue/includes/configure.php');
include("includes/header.php");
$getcontetQry = "select * from tbl_content where cnt_pagename='Contact'";
$getcontentRes = $DBCONN->query($getcontetQry);
$getcontentRow = $getcontentRes->fetch(PDO::FETCH_ASSOC);
$page_content = stripslashes($getcontentRow["cnt_content"]);
$ban_image = stripslashes($getcontentRow["cnt_banimage"]);

?>	
<style>
p {
margin: 0;
padding: 0 0 0 0;
line-height: 1.3em;
}
</style>
<div id="body_container" style="min-height:1510px;">
			<div id="body_right">
				<div id="fixedscroll">
					<script src="js/json2.js"></script>
					<script src="js/dumbFormState-1.js"></script>
					<?php
						include('includes/left_menu.php');
					?>
				</div>
			</div>
			<div id="body_left">
			<?php if ($ban_image!=''&& is_file(BANNER_PATH.$ban_image)){ 
			?>
			<img src="<?php echo MOVE_BANNER_PATH.$ban_image ?>" >  
			<?php
			}
			?>
				<h2 style="text-align:left; margin-left:25px;"><br>We are located at:<br></h2>
				<h2 style="text-align:left; margin-left:25px;">Burswood<br></h2>
				<div id="page_content" style="min-height: 500px;">
					<?php echo $page_content ?>
					<div class="col-md-12" style="text-align: center;margin:auto;float:none;">
						<h4 style="font-size: 19px;font-weight:600;">Download the Walk-in GP app now for the real-time queue.</h4>
						<div class="col-md-12" id="nurseicon">
							 <a href="javascript:void(0);"><img src="images/qr_code_ios.png" class="img-responsive qrcodeimg"></a>
							 <a  href="javascript:void(0);"><img src="images/qr_code_android.png" class="img-responsive qrcodeimg appimageleft"></a><br>
							 <a href="https://itunes.apple.com/us/app/the-walk-in-gp/id1268271518?ls=1&mt=8" target="_balnk"><img src="images/app-store.png"></a>
							 <a href="https://play.google.com/store/apps/details?id=com.thewalkingp" target="_balnk"><img src="images/playstore.png" class="appimageleft"></a>
						</div>

					</div>
					<div class="col-md-12" id="mobilenurseicon">
						 <a href="javascript:void(0);"><img src="images/qr_code_ios.png" class="img-responsive qrcodeimg"></a>
						 <a  href="javascript:void(0);"><img src="images/qr_code_android.png" class="img-responsive qrcodeimg appimageleft"></a><br>
						 <a href="https://itunes.apple.com/us/app/the-walk-in-gp/id1268271518?ls=1&mt=8" target="_balnk"><img src="images/app-store.png"></a>
						 <a href="https://play.google.com/store/apps/details?id=com.thewalkingp" target="_balnk"><img src="images/playstore.png" class="appimageleft"></a><br>
						 <a  href="javascript:void(0);"><img src="images/blue3.png" style="width: 100px;    margin-bottom: -17px;" class="img-responsive qrcodeimg appimageleft"></a>
					</div>
				</div>
            </div>
<?php
include("includes/footer.php");
?>