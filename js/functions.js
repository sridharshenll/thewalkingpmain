/* Trim String Function */
function Trim(str) {
	str = str.replace(/\s+/,'');
	str = str.replace(/\s+$/,'');
	return str;
}


/* IsNumeric Function */
function IsNumeric(val) {
	if (val != "") {
		for (i=0; i<val.length; i++) {
			if (isNaN(val.charAt(i)) &&
			   (val.charAt(i) != "," &&
				val.charAt(i) != ".")) {
				return false;
			}
		}
	}
	else {
		return false;
	}
	return true;
}


/* Valid E-Mail */
function ValidEmail(str) {
	str = Trim(str);
	ComPos = str.indexOf(',');
	PerPos = str.indexOf('..');
	SpcPos = str.indexOf(' ');
	AccPos = str.indexOf('@');
	AccStr = str.substr(0,AccPos);
	AccLen = AccStr.length;
	DomPos = str.lastIndexOf('.');
	DomStr = str.substr(AccPos+1,(DomPos-AccPos)-1);
	DomLen = DomStr.length;
	ExtPos = DomPos + 1;
	ExtLen = str.length - ExtPos;
	ExtStr = str.substr(ExtPos,ExtLen);
	if(	   (ComPos == -1)
		&& (PerPos == -1)
		&& (SpcPos == -1)
		&& (AccPos != -1)
		&& (DomPos != -1)
		&& (AccLen >= 2)
		&& (DomLen >= 2)
		&& (ExtLen >= 2)
		&& (ExtLen <= 4)) {
		return true;
	}
	else{return false}
}


/* Valid Telephone */
function ValidTelephone(str) {
	str = Trim(str);
	str = str.replace("(","");
	str = str.replace(")","");
	str = str.replace(".","");
	str = str.replace(".","");
	str = str.replace("-","");
	str = str.replace("-","");
	str = str.replace(" ","");
	strLen = str.length;
	if((strLen == 10 || strLen == 7) && IsNumeric(str)) {
		return true;
	}
	else{
		return false;
	}
}


/* Check if radio or checkbox items are selected */
function CheckSelected(arr) {
	if(arr.length > 0) {
		for(var index = 0; index < arr.length; index++) {
			if(arr[index].checked) {
				return true;
				break;
			}
		}
	}
	else{
		if(arr.checked) {
			return true;
		}
		else{
			return false;
		}
	}
	return false;
}


/* Capitalize Words */
function capitalizeWords(s) {
	return s.toLowerCase().replace(/\b./g, function(a){ return a.toUpperCase(); });
}


/* Validate Panel */
function ValidatePanel() {
	var strInfo = '';
	var objForm = document.frmPanel;

	if (objForm.elements['Name'].value == '') {
		strInfo += '\n\t- ' + 'Name';
	}
	if (!ValidEmail(objForm.elements['E-Mail'].value)) {
		strInfo += '\n\t- ' + 'E-Mail';
	}
	if (!ValidTelephone(objForm.elements['Phone'].value)) {
		strInfo += '\n\t- ' + 'Phone';
	}

	if (strInfo != '') {
		strInfo = 'The following information was entered incorrectly:\n' +
		strInfo + '\n\nPlease re-enter the information and try again...';
		alert(strInfo);
		return false;
	}
	else {
		return true;
	}
}


/* Google Map */
function googleMap(strAddress, strTitle, strContainer){
	var blnMapLinks   = true;
	var blnMapWindow  = true;
	var blnMapWinOpen = true;

	google.maps.visualRefresh = true;

	$(strContainer).gmap3({
		marker: {
			values: [
				{
					address: strAddress,
					options: {
						address: strAddress,
						title: strTitle
						//title: '<img src="images/img.php?w=230&src=images/Logo_STIMS.png" alt="STI Managed Services" />'
					}
				}
				/*,
				{
					latLng: [28.690898,-81.386062],
					options: {
						address: '28.690898,-81.386062',
						title: 'Coordinates',
						icon: 'http://maps.google.com/mapfiles/marker_greyA.png'
					}
				}*/
			],
			events: {
				click: function(marker, event, context) {
					var map = $(this).gmap3('get');
					var infowindow = $(this).gmap3({get:{name:'infowindow'}});
					if (infowindow){
						infowindow.open(map, marker);
						infowindow.setOptions({
							disableAutoPan: false,
							content: mapWindow(marker.address, marker.title, blnMapLinks, blnMapWindow)
						});
					}
					else {
						$(this).gmap3({
							infowindow: {
								anchor: marker,
								options: {
									disableAutoPan: false,
									content: mapWindow(marker.address, marker.title, blnMapLinks, blnMapWindow)
								}
							}
						});
					}
				}
			}
		},
		//autofit: {},
		map: {
			options: {
				zoom: 12,
				mapTypeId: 'roadmap', //hybrid, roadmap, satellite, terrain
				mapTypeControl: true,
				mapTypeControlOptions: {
					style: google.maps.MapTypeControlStyle.DEFAULT
					//DEFAULT, DROPDOWN_MENU, HORIZONTAL_BAR
				},
				scrollwheel: true,
				navigationControl: true,
				streetViewControl: true,
				overviewMapControl: true,
				overviewMapControlOptions: { opened: true },
				panControl: true,
				rotateControl: true,
				scaleControl: true,
				zoomControl: true
			},
			callback: function(map) {
				if (blnMapWinOpen) {
					marker = $(this).gmap3({get:{ name:'marker', all:true }})[0];
					google.maps.event.trigger(marker, 'click');
				}
			}
		}
	});
}


/* Create Map Window */
function mapWindow(strAddress, strTitle, blnMapLinks, blnMapWindow) {
	if (blnMapWindow) {
		intCommas = (strAddress.split(',').length - 1);

		if (intCommas >= 2) {
			strStreet   = strAddress.substr(0,strAddress.indexOf(', '));
			strLocation = strAddress.substr(strAddress.indexOf(', ')+2);
		}
		else {
			strStreet   = strAddress;
			strLocation = '';
		}

		strWindow = '<div class="gmap_infowin">';

		if (strTitle != '') {
			strWindow += '<strong>' + strTitle + '</strong><br />';
		}

		strWindow += strStreet;

		if (strLocation != '') {
			strWindow += '<br />' + strLocation;
		}

		if (strWindow.lastIndexOf('.') > strWindow.indexOf('</strong><br />')) {
			var intBrIdx = strWindow.lastIndexOf('<br />');
			var intHrIdx = strWindow.lastIndexOf('<hr />');
			var intIndex = (intHrIdx > intBrIdx) ? intHrIdx : intBrIdx;

			if (intIndex > 0) {
				var strCrd = strWindow.substr(intIndex+6).replace(' ','');
				var arrCrd = strCrd.split(',');
				strWindow  = strWindow.substr(0,intIndex+6);
				strWindow += '<span style="font-style:italic;">' + arrCrd[0] + '&deg;, ' + arrCrd[1] + '&deg;</span>';
			}
		}

		if (blnMapLinks) {
			strWindow += '<br /><span class="gmap_links">';
			strWindow += '<a href="http://maps.google.com/maps?q=' + strAddress + '" target="_blank">Full Map</a> &nbsp;|&nbsp; ';
			strWindow += '<a href="http://maps.google.com/maps?lsm=1&daddr=' + strAddress + '" target="_blank">Get Directions</a>';
			strWindow += '</span>';
		}

		strWindow += '</div>';

		return strWindow;
	}
}


/* jQuery: Validate Form */
//<input type="radio" name="FirstRadio" value="Sample Value" class="required" data-required-name="Input_Other" data-required-value="Other..." />
//<input type="text" name="E-Mail" id="E-Mail" class="required_email" data-optional-name="Phone" />
function jValidateForm(strFormName) {
	var strInfo     = '';
	var strName     = '';
	var strReqName  = '';
	var strReqValue = '';

	$('form[name="' + strFormName + '"] .required, ' +
	  'form[name="' + strFormName + '"] .required_email, ' +
	  'form[name="' + strFormName + '"] .required_phone, ' +
	  'form[name="' + strFormName + '"] .required_alpha, ' +
	  'form[name="' + strFormName + '"] .required_numeric, ' +
	  'form[name="' + strFormName + '"] .required_hidden').each(function(i,obj) {

		if ($('form[name="' + strFormName + '"] input[name="' + obj.name + '"], ' +
			  'form[name="' + strFormName + '"] select[name="' + obj.name + '"], ' +
			  'form[name="' + strFormName + '"] textarea[name="' + obj.name + '"]').is(':visible')	
			|| obj.name.substring(0, 9) == 'plupload_') {

			strReqName  = obj.getAttribute('data-required-name');
			strReqValue = obj.getAttribute('data-required-value');
			strOptName  = obj.getAttribute('data-optional-name');

			switch (obj.type) {
				case 'hidden':
					if (obj.name.substring(0, 9) == 'plupload_') {
						var intCount = $('form[name="' + strFormName + '"] #plupload_count').val();
						var intDone  = $('form[name="' + strFormName + '"] #plupload_done').val();

						if (intCount == 0 && obj.name == 'plupload_count') {
							strInfo += jFormatValidateForm('Add at least 1 file to the Upload Queue', null, strOptName);
						}
						else if (intCount != intDone && obj.name == 'plupload_done') {
							strInfo += jFormatValidateForm('Upload or Remove files in Upload Queue', null, strOptName);
						}
					}
					break;

				case 'radio':
				case 'checkbox':
					if (strName == '') {
						if (!CheckSelected(eval("document." + obj.form.name + ".elements['" + obj.name + "']"))) {
							strInfo += jFormatValidateForm(obj.name, null, strOptName);
						}
						if (strReqName != null) {
							strInfo += jSubValidateForm(strFormName, obj, strReqName, strReqValue);
						}
					}
					strName = obj.name;
					break;

				case 'text':
				//case 'password':
					switch (obj.className) {
						case 'required_email':
							if (!ValidEmail(obj.value)) {
								//alert("Test");
								strInfo += jFormatValidateForm(obj.name, null, strOptName);
								jHighlightValidateForm(strFormName, obj.name,'add');
							}
							else {
								jHighlightValidateForm(strFormName, obj.name,'remove');
							}
							break;

						case 'required_phone':
							if (!ValidTelephone(obj.value)) {
								strInfo += jFormatValidateForm(obj.name, null, strOptName);
								jHighlightValidateForm(strFormName, obj.name,'add');
							}
							else {
								jHighlightValidateForm(strFormName, obj.name,'remove');
							}
							break;

						case 'required_alpha':
							if (IsNumeric(obj.value)) {
								strInfo += jFormatValidateForm(obj.name, 'letters only', strOptName);
								jHighlightValidateForm(strFormName, obj.name,'add');
							}
							else {
								jHighlightValidateForm(strFormName, obj.name,'remove');
							}
							break;

						case 'required_numeric':
							if (!IsNumeric(obj.value)) {
								strInfo += jFormatValidateForm(obj.name, 'numbers only', strOptName);
								jHighlightValidateForm(strFormName, obj.name,'add');
							}
							else {
								jHighlightValidateForm(strFormName, obj.name,'remove');
							}
							break;

						default:
							if (Trim(obj.value) == '') {
								strInfo += jFormatValidateForm(obj.name, null, strOptName);
								jHighlightValidateForm(strFormName, obj.name,'add');
							}
							else {
								jHighlightValidateForm(strFormName, obj.name,'remove');
							}
					}
					strName = '';
					break;

				case 'select-one':
					if (Trim(obj.value) == '') {
						strInfo += jFormatValidateForm(obj.name, null, strOptName);
						jHighlightValidateForm(strFormName, obj.name,'add');
					}
					else {
						jHighlightValidateForm(strFormName, obj.name,'remove');
					}
					if (strReqName != null) {
						strInfo += jSubValidateForm(strFormName, obj, strReqName, strReqValue);
					}
					break;

				case 'select-multiple':
				case 'textarea':
					if (Trim(obj.value) == '') {
						strInfo += jFormatValidateForm(obj.name, null, strOptName);
						jHighlightValidateForm(strFormName, obj.name,'add');
					}
					else {
						jHighlightValidateForm(strFormName, obj.name,'remove');
					}
					break;
			}

			strReqName  = '';
			strReqValue = '';
			strOptName  = '';
		}
	});

	if (strInfo != '') {
		strInfo = 'The following fields were entered incorrectly:<br /><br />' +
		'<ul>' + strInfo + '</ul>' +
		'<br />Please correct the information and try again...';

		$.alerts.dialogClass = 'form_error';
		jAlert(strInfo, 'Invalid Form Data');
		return false;
	}
	else {
		return true;
	}
}

function jSubValidateForm(strFormName, obj, strReqName, strReqValue) {
	if (strReqName != '') {
		var strSubValue = Trim(eval("document." + obj.form.name + ".elements['" + strReqName + "']").value);

		switch (obj.type) {
			case 'radio':
			case 'checkbox':
				var blnChecked = $('input[name="' + obj.name + '"]:radio[value="' + strReqValue + '"]').is(':checked');
				if (blnChecked) {
					if (strSubValue == '') {
						jHighlightValidateForm(strFormName, strReqName,'add');
						return jFormatValidateForm(strReqName);
					}
					else {
						jHighlightValidateForm(strFormName, strReqName,'remove');
					}
				}
				break;

			default:
				if (strReqValue != '') {
					if (Trim(obj.value) == strReqValue) {
						if (strSubValue == '') {
							jHighlightValidateForm(strFormName, strReqName,'add');
							return jFormatValidateForm(strReqName);
						}
						else {
							jHighlightValidateForm(strFormName, strReqName,'remove');
						}
					}
				}
				else {
					if (Trim(strNewValue) == '') {
						jHighlightValidateForm(strFormName, strReqName,'add');
						return jFormatValidateForm(strReqName);
					}
					else {
						jHighlightValidateForm(strFormName, strReqName,'remove');
					}
				}
				break;
		}
	}
	return '';
}

function jFormatValidateForm(strName, strExtra, strOptional) {
	var blnReturn = true;

	if (strName.substr(0,2) == 'x_') {
		strName = strName.substr(2);
		strName = capitalizeWords(strName);
	}
	if (strName.indexOf('_') >= 0) {
		strName = strName.replace('_',' ');
		strName = capitalizeWords(strName);
	}
	strName  = (strName == 'captcha')   ? 'Security Code' : strName;
	strName  = (strName == 'captcheck') ? 'Spam Question' : strName;
	strName  = (strName == 'username')  ? 'Username' : strName;
	strName  = (strName == 'un')        ? 'Username' : strName;
	strName  = (strName == 'password')  ? 'Password' : strName;
	strName  = (strName == 'pw')        ? 'Password' : strName;

	strExtra = ((strExtra != null) ? ' <em>(' + strExtra + ')</em>' : '');

	if (strOptional != null) {
		strOptionalValue = $('[name="' + strOptional + '"]').val();
		if (strOptionalValue != '') {
			blnReturn = false;
		}
	}

	return (blnReturn) ? '<li><strong>' + strName + '</strong>' + strExtra + '</li>' : '';
}

function jHighlightValidateForm(strFormName, strName, strAction) {
	var strClass = 'required_highlight';
	if (strAction == 'add') {
		$('form[name="' + strFormName + '"] [name="' + strName + '"]').addClass(strClass);
	}
	else {
		$('form[name="' + strFormName + '"] [name="' + strName + '"]').removeClass(strClass);
	}
}

function jLoadValidateForm(strFormName, blnIcons) {
	blnIcons = typeof blnIcons !== 'undefined' ? blnIcons : true;
	if (blnIcons) {
		$('form[name="' + strFormName + '"] .required:not(:input:radio, input:checkbox), ' +
		  'form[name="' + strFormName + '"] .required_email, ' +
		  'form[name="' + strFormName + '"] .required_phone, ' +
		  'form[name="' + strFormName + '"] .required_alpha, ' +
		  'form[name="' + strFormName + '"] .required_numeric').after(
			'<span class="icon_required_small" style="margin-left:2px;" title="Required"></span>'
		);
	}
	$('form[name="' + strFormName + '"]').submit(function() {
		return jValidateForm(strFormName);
	});
}
function userValidate()
{
	
	email_address=Trim($("#E-Mail").val());
	$.alerts.dialogClass = 'form_error';
	jHighlightValidateForm("frmPanel", "Name",'remove');
	jHighlightValidateForm("frmPanel", "Phone",'remove');
	jHighlightValidateForm("frmPanel", "E-Mail",'remove');
	if(Trim($("#First_name").val())==""){
		jAlert("Please enter first name", 'Invalid Form Data');
		jHighlightValidateForm("frmPanel", "First Name",'add');
		return false;
	}
	else if(Trim($("#Name").val())==""){
		jAlert("Please enter last name", 'Invalid Form Data');
		jHighlightValidateForm("frmPanel", "Last Name",'add');
		return false;
	}
	else if(Trim($("#Address").val())==""){
		jAlert("Please enter property address", 'Invalid Form Data');
		jHighlightValidateForm("frmPanel", "LEADCF42",'add');
		return false;
	}
	else if(Trim($("#City").val())==""){
		jAlert("Please enter property city", 'Invalid Form Data');
		jHighlightValidateForm("frmPanel", "LEADCF43",'add');
		return false;
	}
	
	else if(Trim($("#Phone").val())==""){
		jAlert("Please enter phone", 'Invalid Form Data');
		jHighlightValidateForm("frmPanel", "Phone",'add');
		return false;
	}
	else if(!ValidTelephone(Trim($("#Phone").val()))){
		jAlert("Please enter valid phone number", 'Invalid Form Data');
		jHighlightValidateForm("frmPanel", "Phone",'add');
		return false;
	}
	else if(Trim($("#Best_Time").val())==""){
		jAlert("Please enter best time to contact", 'Invalid Form Data');
		jHighlightValidateForm("frmPanel", "LEADCF44",'add');
		return false;
	}
	
	else if(Trim($("#E-Mail").val())==""){
		jAlert("Please enter email", 'Invalid Form Data');
		jHighlightValidateForm("frmPanel", "E-Mail",'add');
		return false;
	}
	else if(!ValidEmail(Trim($("#E-Mail").val()))){
		jAlert("Please enter valid email", 'Invalid Form Data');
		jHighlightValidateForm("frmPanel", "E-Mail",'add');
		return false;
	}
	else{
		$("#loader_td").css('display','block');
		$.ajax({url:"email_verify.php?email="+email_address,success:function(result){
			$("#loader_td").css('display','none');
			if(result=="valid"){
				$('#body_container').css('min-height','1000px');
				$('form #table1_form').css('display','none');
				$('form #table2_form').show('slide', {direction: 'left'}, 5000);
				$("form #request_send").val("yes");
			}
			else{
				jAlert("Please enter valid email", 'Invalid Form Data');
				jHighlightValidateForm("frmPanel", "E-Mail",'add');
				return false;
			}
		}});
	}
}