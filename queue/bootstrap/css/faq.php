<?php
include("includes/header.php");
?>
		<div id="body_container" style="min-height:1100px;">
			<div id="body_right">
				<div id="fixedscroll">
					<script src="js/json2.js"></script>
					<script src="js/dumbFormState-1.js"></script>
					<?php
						include('includes/left_menu.php');
					?>
				</div>
			</div>
			<div id="body_left"> 
				<h2 style="text-align:left; margin-left:25px;"><br>Frequently Asked Questions<br><br></h2>
				<div id="page_content">
					<p>1. Do you guys still bulk bill?<br>

						Yes, we bulk bill everyone who has a valid Medicare card.<br><br>

						2. Can I make an appointment?<br>

						No. Sorry we don’t take appointment. This is a walk-in clinic. First-come first-serve. As long as you come before our closing time, you will definitely be seen by the doctor.<br><br>

						3. What is your current waiting time?<br>

						On average 30-60 minutes or less. If you can, please avoid our daily opening and closing times as well as lunch times.<br><br>

						4. Can I see the doctor if I don’t have a valid Medicare card?<br>

						Yes. A fee is payable to see the doctor.<br><br>

						5. Are you an OSHC’s direct billing medical providers?<br>

						Yes we are. However a gap fee is applicable.<br><br>

						6. What kind of doctors are you?<br>

						We are general practitioners aka GP. We practice all aspects of family medicine including childhood immunisations. We also perform pre-employment medicals. However, we DO NOT provide the following services:

						prescribing of any drugs of addiction, narcotics or sleeping tablets,
						medical legal report (car accident, domestic violence, work injury, insurance),
						report writing of any Centrelink form,
						report writing of any insurance form,
						endorsement of Immunisation Exemption Conscientious Objection form,
						back dating of a medical certificate
						referral for termination of pregnancy<br><br>
						7. Any female doctor?<br>

						No. Sorry only male doctors at present.<br><br>

						8. Any parking on site?<br>

						Yes. We have several parking bays on site.<br><br>

						9. Do you have any allied health provider onsite?<br>

						Yes. We have an onsite St John of God pathology collection centre.<br><br>

						10. Can you tell me my test results over the phone?<br>

						No. For security and privacy reasons, we can not disclose your test results over the phone. You will need to come back to see the doctor.<br><br>

						11. Can the doctor come and visit me at home?<br>

						Yes, we are a member of the Western Australian Deputising Medical Service <a href="www.wadems.org.au">(www.wadems.org.au)</a>, and as a patient of our clinic, you may entitle to use the services provided by WADEMS. Please ring 9321 9133 to arrange for a doctor to visit you at home.<br><br>

						12. Do I need to pay if the doctor refers me for blood tests or Xrays?<br>

						Yes. A fee may be payable for those third party medical services (eg. pathology test, imaging, or specialist consultation).</p>
				<br><br><br>	
				</div>
	

﻿			</div>
		</div>
<?php
include("includes/footer.php");
?>