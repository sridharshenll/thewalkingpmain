<html>
<head>
   <style>
        #sortable { list-style-type: none; margin: 0; padding: 0; width: 60%; }
        #sortable li { margin: 0 3px 3px 3px; padding: 0.4em; padding-left: 1.5em; font-size: 1.4em; height: 18px; }
        #sortable li span { position: absolute; margin-left: -1.3em; }
        </style>

<script src="http://code.jquery.com/jquery-1.7.2.min.js"></script>
<script src="http://code.jquery.com/ui/1.8.21/jquery-ui.min.js"></script>
<script src="js/jquery.ui.touch-punch.min.js" language="javascript"></script>
</head>
<body>
 <script>
        $(function() {
          $( "#sortable" ).sortable();
          $( "#sortable" ).disableSelection();
        });
        </script>
 <div class="container">
<ul id="sortable">
  <li>First</li>
  <li>Second</li>
  <li>Third</li>
</ul>
</div>
</body>
</html>