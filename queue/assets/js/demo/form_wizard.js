/*
 * form_wizard.js
 *
 * Demo JavaScript used on Form Wizard-page.
 */

"use strict";

$(document).ready(function(){

	//===== Form Wizard =====//

	// Config
	var form    = $('#queue_system');
	var wizard  = $('#form_wizard');
	var error   = $('.alert-danger', form);
	var success = $('.alert-success', form);
	var medicar = $('#patient_medicareno').val();
	form.validate({
		doNotHideMessage: true, // To display error/ success message on tab-switch
		focusInvalid: false, // Do not focus the last invalid input
		invalidHandler: function (event, validator) {
			// Display error message on form submit

			success.hide();
			error.show();
		},
		submitHandler: function (form) {
			form.submit();
			
		}
	});

	// Functions
	var displayConfirm = function() {
		$('#tab4 .form-control-static', form).each(function(){
			var input = $('[name="'+$(this).attr("data-display")+'"]', form);

			if (input.is(":text") || input.is("textarea")) {
				$(this).html(input.val());
			} else if (input.is("select")) {
				$(this).html(input.find('option:selected').text());
			} else if (input.is(":radio") && input.is(":checked")) {
				$(this).html(input.attr("data-title"));
			}
		});
	}

	var handleTitle = function(tab, navigation, index) {
		var total = navigation.find('li').length;
		var current = index + 1;
		if(current==4){
			if($("#queue_sytem_set").val()==1){
				alert("Patient registration is not available at this location. Please try again later");
				javascript_abort();
			}
		}
		// Set widget title
		$('.step-title', wizard).text('Step ' + (index + 1) + ' of ' + total);

		// Set done steps
		$('li', wizard).removeClass("done");

		var li_list = navigation.find('li');
		for (var i = 0; i < index; i++) {
			$(li_list[i]).addClass("done");
		}

		if (current == 1) {
			wizard.find('.button-previous').hide();
		} else {
			wizard.find('.button-previous').show();
		}

		if (current >= total) {
			wizard.find('.button-next').hide();
			wizard.find('.button-submit').show();
			displayConfirm();
		} else {
			wizard.find('.button-next').show();
			wizard.find('.button-submit').hide();
		}
	}

	// Form wizard example
	wizard.bootstrapWizard({
		'nextSelector': '.button-next',
		'previousSelector': '.button-previous',
		onTabClick: function (tab, navigation, index, clickedIndex) {
			//alert(index);
			success.hide();
			error.hide();

			if (form.valid() == false) {
				return false;
			}

			handleTitle(tab, navigation, clickedIndex);
		},
		onNext: function (tab, navigation, index) {
			//alert(index);
			success.hide();
			error.hide();

			if (form.valid() == false) {
				return false;
			}
			if (index == 3) {				let clinicLocation = $('#queue_sytem_loc').val();				if (clinicLocation == 'Telehealth') {					showAnnouncement(clinicLocation);				}			}
			handleTitle(tab, navigation, index);
		},
		onPrevious: function (tab, navigation, index) {
			success.hide();
			error.hide();

			handleTitle(tab, navigation, index);
		},
		onTabShow: function (tab, navigation, index) {
			// To set progressbar width
			var total = navigation.find('li').length;
			var current = index + 1;
			var $percent = (current / total) * 100;
			wizard.find('.progress-bar').css({
				width: $percent + '%'
			});
		}

	});	

	wizard.find('.button-previous').hide();
	$('#form_wizard .button-submit').click(function () {
		form.submit();
	}).hide();

});