<?php
include('includes/configure.php');
$view_doctor_id=$_GET['doctor_id'];
$getdoctorQry="select * from  tbl_doctor where doctor_id='".$view_doctor_id."'";
$getdoctorRes=$DBCONN->query($getdoctorQry);
$getdoctoRow=$getdoctorRes->fetch(PDO::FETCH_ASSOC);
$doctor_name=stripslashes($getdoctoRow["doctor_name"]);
$doctor_qualification=stripslashes($getdoctoRow["doctor_qualification"]);
include('includes/header.php');
?>
<div id="content">
			<div class="container">
				<!--=== Page Header ===-->
				<div class="page-header">
					<div class="page-title">
						<b>Doctor:</b> <?php echo $doctor_name;?><br><b>Qulification:</b> <?php echo $doctor_qualification;?>
						
					</div>					
				</div>
				<!-- /Page Header -->
				
				<!--=== Responsive DataTable ===-->
				<div class="row">
					<div class="col-md-12">
						<div class="widget box">
							<div class="widget-header">
								<h4><i class="icon-reorder"></i>Patients List</h4>
								 <input type="button" class="btn btn-primary pull-right" onclick="document.location='viewstatus.php'" value="Back">
						       </div>
						<div class="widget-content">
								<table class="table table-hover table-striped table-bordered table-highlight-head">
									<thead>
										<tr>
											<th>#</th>
											<th>Patients Name</th>
										    <th>Token Number</th>
											
										</tr>
									</thead>
									<tbody>
										<?php
										$getQry="select * from tbl_patient where doctor_id='".$view_doctor_id."' and register_date='".date('Y-m-d')."' order by created_date asc";
										$getRes=$DBCONN->query($getQry);
										$getCnt=$getRes->rowCount();
										if($getCnt>0){
										$sno=1;
										foreach($getRes->fetchAll(PDO::FETCH_ASSOC) as $getRow){
											if($sno%2==0)
											  $class="odd gradeX";
											else
											  $class="even gradeC";
											  $doctor_id=stripslashes($getRow["doctor_id"]);
											  $getdoctorQry="select * from  tbl_doctor where doctor_id='".$doctor_id."'";
											  $getdoctorRes=$DBCONN->query($getdoctorQry);
											  $getdoctoRow=$getdoctorRes->fetch(PDO::FETCH_ASSOC);
											  $doctor_name=stripslashes($getdoctoRow["doctor_name"]);
										?>
										<tr class="<?php echo $class;?>">
											<td><?php echo $sno;?></td>
											<td><?php echo stripslashes($getRow["patient_name"]);?></td>								<td><?php echo stripslashes($getRow["token_number"]);?></td>
											
										</tr>		
										<?php
										$sno++;
									}
								}
									else{
										echo "<tr class=\"odd gradeX\"><td colspan=\"3\" style=\"text-align:center;\">No  patients found.</td></tr>";
									}
								?>
									</tbody>
								</table>
								<!--<div class="row">
									<div class="table-footer">										
										<div class="col-md-12">
											<ul class="pagination">
												<li class="disabled"><a href="javascript:void(0);">&larr; Prev</a></li>
												<li class="active"><a href="javascript:void(0);">1</a></li>
												<li><a href="javascript:void(0);">2</a></li>
												<li><a href="javascript:void(0);">3</a></li>
												<li><a href="javascript:void(0);">4</a></li>
												<li><a href="javascript:void(0);">Next &rarr;</a></li>
											</ul>
										</div>
									</div>
								</div> -->
							</div>
						</div>
					</div>
					<!-- /Table with Footer -->
							
						</div>
					</div>
				</div>
				<!-- /Responsive DataTable -->
				<!-- /Page Content -->
			</div>
			<!-- /.container -->

		</div>

		<!-- /Center Main page Content -->
<?php
include("includes/footer.php");
?>