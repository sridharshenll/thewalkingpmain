<?php
include('../includes/configure.php');
include("includes/session_check.php");
$Msg = $_GET['msg'] ?? '';
if($Msg == 1) {
	$Message = "Patient Information added successfully.";
} else if($Msg == 2){
	$Message = "Patient Information updated successfully.";
} else if($Msg == 3){
	$Message = "Patient Information deleted successfully.";
}
$check_location = $DBCONN->query("select * from tbl_settings where location='".$_SESSION["location"]."' and add_queue='0'");
$loca_num = $check_location->rowCount();
if (isset($_GET["patient_id"])) {
    $patientid = $_GET["patient_id"];
	$getPatientQry = "select * from tbl_patient where patient_id='".$patientid."'";
	$getPatientRes = $DBCONN->query($getPatientQry);
	$getPatientRow = $getPatientRes->fetch(PDO::FETCH_ASSOC);
    
    $preffered_doctor = $getPatientRow["doctor_id"];
	$given_names = addslashes($getPatientRow["patient_name"]);
	$clinic_location = addslashes($getPatientRow["location"]);
	$clinic_confirmation = addslashes($getPatientRow["location_option"]);
    
    $patient_title = addslashes($getPatientRow["title"]);

    $family_name = addslashes($getPatientRow["family_name"]);
    $patient_dob = $getPatientRow["dob"];
    $patient_medicareno = $getPatientRow["medicare_no"];
    $patient_referno = $getPatientRow["reference_no"];

    $patient_ref_expiry = $getPatientRow["expiry_date"];
    $patient_address = addslashes($getPatientRow["address"]);
    $patient_suburb = addslashes($getPatientRow["suburb"]);

	$patient_postcode = $getPatientRow["post_code"];
	$patient_homephone = $getPatientRow["home_phone"];
	$patient_mobile = $getPatientRow["mobile_phone"];
	$patient_hcc    = $getPatientRow["pension_card_no"];
	$hcc_ref_expiry = $getPatientRow["pension_card_expiry_date"];
	$buildversion = ($getPatientRow["buildversion"] != "") ? $getPatientRow["buildversion"] : "";
	$device_type = ($getPatientRow["device_type"] != "") ? $getPatientRow["device_type"] : "Website";
	$is_keep = ($getPatientRow["is_keep"] != "") ? $getPatientRow["is_keep"] : "";

	$getTknQry = "select count(*) as token_cnt from tbl_patient where register_date='".date('Y-m-d')."' and location='".$clinic_location."'";
	$getTknRes = $DBCONN->query($getTknQry);
	$getTknRow = $getTknRes->fetch(PDO::FETCH_ASSOC);
	$patient_token_number = $getTknRow["token_cnt"] + 1;

	$getPosition = "select max(display_order) as displayrow from tbl_patient where location='".$clinic_location."'";
	$getPosRes = $DBCONN->query($getPosition);
	$getDisplayRow = $getPosRes->fetch(PDO::FETCH_ASSOC);
	$DisplayRow = $getDisplayRow["displayrow"]+1;

	$insertQry = "insert into tbl_patient(doctor_id,patient_name,location,location_option,title,family_name,dob,medicare_no,reference_no,expiry_date,address,suburb,post_code,home_phone,mobile_phone,pension_card_no,pension_card_expiry_date,reg_time,token_number,register_date,patient_status,created_date,modified_date,display_order, buildversion, is_keep,device_type) values('".$preffered_doctor."','".ucwords($given_names)."','".$clinic_location."','".$clinic_confirmation."','".$patient_title."','".ucwords($family_name)."','".$patient_dob."','".$patient_medicareno."','".$patient_referno."','".$patient_ref_expiry."','".ucwords($patient_address)."','".ucwords($patient_suburb)."','".$patient_postcode."','".$patient_homephone."','".$patient_mobile."','".$patient_hcc."','".$hcc_ref_expiry."','".date('Y-m-d H:i:s')."','".$patient_token_number."','".date('Y-m-d')."','Appointment fixed','".date('Y-m-d H:i:s')."','".date('Y-m-d H:i:s')."','".$DisplayRow."','".$buildversion."','".$is_keep."','".$device_type."')";
				
	$insertRes = $DBCONN->query($insertQry);

	if($insertRes) {
		$updateQry = "update tbl_patient set patient_status='Duplicate' where patient_id='".$patientid."'";
		$updateRes = $DBCONN->query($updateQry);
		header("Location:manage_patients.php?msg=1");
		exit;
	}
}
include('includes/header.php');

function fun_getMaxValue($Query,$key) {
	global $DBCONN;
	$Result = $DBCONN->query($Query);
	$retresult = "";
	if($Result && $Result->rowCount()) {
		$Row = $Result->fetch(PDO::FETCH_ASSOC);
		$retresult = $Row[$key];
	}
	else
	{
		//echo mysql_error();
	}
	return $retresult;
}


?>
<style>
	#mngptrecds tbody tr .applyellow{
		background-color:yellow;
	}
</style>
<!-- Pickers -->
<script type="text/javascript" src="../plugins/pickadate/picker.js"></script>
<script type="text/javascript" src="../plugins/pickadate/picker.date.js"></script>
<script type="text/javascript" src="../plugins/pickadate/picker.time.js"></script>
<script type="text/javascript" src="../plugins/bootstrap-colorpicker/bootstrap-colorpicker.min.js"></script>

<script type="text/javascript" src="../plugins/fileinput/fileinput.js"></script>
<script type="text/javascript" src="../plugins/bootstrap-wysihtml5/wysihtml5.min.js"></script>
<script type="text/javascript" src="../plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.min.js"></script>

<!-- Form Validation -->
<script type="text/javascript" src="../plugins/validation/jquery.validate.min.js"></script>
<script type="text/javascript" src="../plugins/validation/additional-methods.min.js"></script>
<!-- Noty -->
<script type="text/javascript" src="../plugins/noty/jquery.noty.js"></script>
<script type="text/javascript" src="../plugins/noty/layouts/top.js"></script>
<script type="text/javascript" src="../plugins/noty/themes/default.js"></script>

<!-- Demo JS -->
<script type="text/javascript" src="../assets/js/custom.js"></script>
<script type="text/javascript" src="../assets/js/demo/form_validation.js"></script>
<script type="text/javascript" src="../assets/js/demo/ui_general.js"></script>

<!-- Center Main page Content -->
	<div id="content">
		<div class="container">
			<!--=== Page Header ===-->
			<div class="page-header">
				<div class="page-title">
					<!-- <h3>Patients List</h3> -->
				</div>
			</div>
			<!-- /Page Header -->
			<?php if($Msg ?? FALSE){ ?>
				<div class="alert fade in alert-success">
					<i class="icon-remove close" data-dismiss="alert"></i>
					<?php echo $Message; ?>
				</div>					
			<?php } ?>
			<div class="row">
				<div class="col-md-12">
					<div class="row">
						<div class="col-md-12">
							<div style="padding-top:5px;padding-bottom:5px;float:right;">
								<h5> Date:<?php echo date('d-m-Y');?></h5>
							</div>
						</div>
					</div>
					<div class="widget box">
						<div class="widget-header">
							<h4><i class="icon-reorder"></i>Manage Patients</h4>
							<input type="button" class="btn btn-primary pull-right" onclick="funexport();" value="Export"> 
						</div>
						<div class="widget-content">
							<form name="patient_list" method="post" id='patient_listform'>
								<table class="table table-bordered table-hover table-checkable" id='mngptrecds'>
									<thead>
										<tr>
											<th><input type='checkbox' value='test' onclick='funccheckall()' id='checkallpatients'></th>
											<th>No</th>
											<th>Doctor</th>	
											<th>Patient</th>
											<th>Date of Birth</th>
											<!-- <th class="hidden-xs">Medicare Number</th>
											<th class="hidden-xs">Phone Number</th> -->
											
											<!--  <th>Token Number</th>
												<th>Status</th>
												<th>Status</th>   -->
												<th class="align-center">Action</th>
										</tr>
									</thead>
									<tbody>
										<?php
										// $getQry="select * from tbl_patient where location='".$_SESSION["location"]."' and export!='yes' order by FIELD(patient_status,'Apointment fixed','Visited','Cancelled') asc, register_date desc,patient_id asc";

											$getQry = "(select * from tbl_patient where patient_status='Appointment fixed' and location='".$_SESSION["location"]."' and patient_status !='Duplicate' and export!='yes' order by register_date asc, display_order asc) ";

											$VisitedCount = fun_getMaxValue("select count(*) as visitedcount from tbl_patient where patient_status='Visited'","visitedcount");
										
											if($VisitedCount > 0)
												$getQry.=" union (select * from tbl_patient where (patient_status='Visited' OR 	patient_status='Arrived') and location='".$_SESSION["location"]."' and export!='yes' order by register_date asc, display_order asc) ";
										
											$CancelledCount = fun_getMaxValue("select count(*) as CancelledCount from tbl_patient where patient_status='Cancelled'","CancelledCount");
										
											if($CancelledCount > 0)
											$getQry.=" union (select * from tbl_patient where patient_status='Cancelled' and location='".$_SESSION["location"]."' and export!='yes' order by register_date asc, display_order asc) ";
										
											$getQry.=" order by FIELD(patient_status,'Appointment fixed','Visited','Cancelled') asc, display_order ASC";



											$getRes = $DBCONN->query($getQry);
											$getCnt = $getRes->rowCount();
											if($getCnt > 0) {
												$sno = 1;
												foreach($getRes->fetchAll(PDO::FETCH_ASSOC) as $getRow) {
													if($sno % 2 == 0)
														$class="odd gradeX";
													else
														$class="even gradeC";
													$doctor_id=stripslashes($getRow["doctor_id"]);
													$location_option=stripslashes($getRow["location_option"]);
													if($doctor_id > 0 && $doctor_id != "") {
														$getdoctorQry = "select * from tbl_staff where staff_id='".$doctor_id."'";
														$getdoctorRes = $DBCONN->query($getdoctorQry);
														$getdoctoRow = $getdoctorRes->fetch(PDO::FETCH_ASSOC);
														$doctor_name = stripslashes($getdoctoRow["staff_name"] ?? '');
													} else {
														$doctor_name = "First Available Doctor";
													}

													$bck_clr = '';
													if(strtolower($getRow["patient_status"]) == 'visited') {
														$bck_clr = 'style="background-color:yellow"';
													} else if (strtolower($getRow["patient_status"]) == 'cancelled') {
														$bck_clr = 'style="background-color:#FF9933"';
													}
													$colorstatus = $getRow["color_status"];
													if ($colorstatus == "#ffff1d") {
														$bck_status = "applyellow";
													} else {
														$bck_status = "";
													}
													//echo "patient_status---".$getRow["patient_status"]."--".$bck_clr."===".$getRow["token_number"]."<br>";
												?>

													<tr class="<?php echo $class;?>" <?php echo $bck_clr?>>
														<td class="<?php echo $bck_status;?>"><input type='checkbox' name='exportpatient[]' class='exportpatient' value="<?php echo $getRow["patient_id"];?>"></td>
														<td class="<?php echo $bck_status;?>" ><?php echo $sno;?></td>
														<td class="<?php echo $bck_status;?>"><?php echo ucwords($doctor_name);?></td>
														<td class="<?php echo $bck_status;?>">
															<?php if($getRow["patient_status"] == 'Arrived') { ?>
																<img src="../assets/img/tic.png">
															<?php } ?>
															<?php echo ucwords(stripslashes($getRow["patient_name"]).' '.stripslashes($getRow["family_name"])); ?>
														</td>
														<td class="<?php echo $bck_status;?>"><?php echo date('d/m/Y', strtotime($getRow["dob"]));?></td>
														<!-- <td class="hidden-xs"><?php echo stripslashes($getRow["medicare_no"]);?></td>
														<td class="hidden-xs"><?php echo stripslashes($getRow["mobile_phone"]);?></td> -->
														<td class="align-center <?php echo $bck_status;?>">
															<?php if($loca_num == 1 ) { ?>
																<span class="btn-group">
																	<a onclick="return confirm('Are you sure you want to add this patient back to the queue?');" href="manage_patients.php?patient_id=<?php echo stripslashes($getRow["patient_id"]);?>" class="btn btn-xs bs-tooltip" title="Add Queue"><i class="icon icon-list-ul" aria-hidden="true"></i></a>
																</span>
															<?php } ?>
															<span class="btn-group">
																<a href="edit_patient.php?staff_id=<?php echo stripslashes($getRow["patient_id"]);?>"
																class="btn btn-xs bs-tooltip" title="Edit"><i class="icon-pencil"></i></a>
															</span>
														</td>
														<!--  <td><?php echo strtolower($getRow["patient_status"])?></td>
														<td><?php echo strtolower($getRow["token_number"])?></td>
														<td><?php echo strtolower($getRow["register_date"])?></td>  -->
													</tr>
												<?php $sno++; }
											} else{
												echo "<tr class=\"odd gradeX\"><td colspan=\"6\" style=\"text-align:center;\">No patients found.</td></tr>";
											} ?>
									</tbody>
								</table>
							</form>
						</div>
					</div>
				</div>
				<!-- /Table with Footer -->
					</div>
				</div>
			</div>
			<!-- /Responsive DataTable -->
			<!-- /Page Content -->
		</div>
		<!-- /.container -->

	</div>

<!-- /Center Main page Content -->
<?php include("includes/footer.php");?>
<SCRIPT LANGUAGE="JavaScript">
	$(document).ready( function() {
	//  $('#mngptrecds').dataTable( {
		//"iDisplayLength": 10000000,
		//"aLengthMenu": [[-1], ["All"]],
		//"aaSorting": [[ 2, "asc" ]],
	// } );
		$("#mngptrecds_length").hide();
		$('.dataTables_paginate').hide();
	} )
	function funccheckall(){
		if($('#checkallpatients').is(':checked')==true){
			$("#patient_listform").find('input[type=checkbox]:gt(0)').prop('checked',true);
		}
		else{
			$("#patient_listform").find('input[type=checkbox]:gt(0)').prop('checked',false);
		}
	}
	$(".exportpatient").click(function(){
		if($(".exportpatient:checked").length==$(".exportpatient").length){
			$("#checkallpatients").prop('checked',true);
		}else
			$("#checkallpatients").prop('checked',false);

	});
	function funexport() {
		try {
			with(document.patient_list) {
				var ex = '0';
				if($(".exportpatient:checked").length > 0) {
					action='patient_list-export1.php';
					submit();
					setTimeout(function(){
							location.reload();
						return true;
							action='';
						},3000);
				}
			}
		}
		catch(e){
			alert(e)
		}
	}
</SCRIPT>
