<?php
include('../includes/configure.php');
include("includes/session_check.php");
$doctor_id = $_GET['staff_id'] ?? "";
if($doctor_id != ""){
	$password = "";
	$getdoctorQry = "select * from tbl_staff where staff_id='".$doctor_id."'";
	$getdoctorRes = $DBCONN->query($getdoctorQry);
	$getdoctoRow = $getdoctorRes->fetch(PDO::FETCH_ASSOC);
	$doctor_name = stripslashes($getdoctoRow["staff_name"]);
	$doctor_qualification = stripslashes($getdoctoRow["staff_qualification"]);
	$doctor_working_hours = stripslashes($getdoctoRow["staff_working_hours"]);
	$status = $getdoctoRow["status"];
	$password = $getdoctoRow["password"];
	$email = stripslashes($getdoctoRow["email"]);
	$username = stripslashes($getdoctoRow["username"]);
	 
	$Mode = "Edit";
	$Value = "Save";
}
else{
	$Mode = "Add";
	$Value = "Add";
}
if(isset($_POST["doctor_name"])){
	$Doctor_name = addslashes(trim($_POST["doctor_name"]));
    $Qualification = addslashes(trim($_POST["qualification"]));
	$Working_hours = addslashes(trim($_POST["doctor_working_hours"]));
	$Status = $_POST["status"];
	$Role = "doctor";
	$Location = $_SESSION["location"];
    $Email = addslashes(trim($_POST["email"]));

	$username = addslashes(stripslashes(trim($_POST["username"])));

	if(trim($_POST["password"]) != ""){
		$Password = md5(trim($_POST["password"]));
	}
	if($Mode == "Edit"){
		if(trim($_POST["password"]) == ""){
			$Password = $password;
		}

		$GetExistingusername = "select username from tbl_staff where username='".$username."' and staff_id!='".$doctor_id."'";
		$res = $DBCONN->query($GetExistingusername);
		if($res && $res->rowCount() > 0) {
			$row = $res->fetchAll(PDO::FETCH_ASSOC);
			$userexists = 1;
		} else {
			$userexists = 0;
		}
		
		if($userexists == 0)
		{
			$updateQry = "update tbl_staff set role='".$Role."',username='".$username."',email='".$Email."',password='".$Password."',staff_name='".$Doctor_name."',staff_qualification='".$Qualification."',staff_working_hours='".$Working_hours."',status='".$Status."',location='". $Location."',modified_date='$dbdatetime' where staff_id='".$doctor_id."'";
			$updateRes = $DBCONN->query($updateQry);
			if($updateRes) {
				header("Location:manage_doctors.php?msg=2");
				exit;
			}
		} else {
			$errorMessage = "Username already exists";
		}
	} else {
		$insertQry = "insert into tbl_staff(role,username,email,password,staff_name,staff_qualification,staff_working_hours,status,location,created_date,modified_date) values('".$Role."','".addslashes(stripslashes(trim($username)))."','".$Email."','".$Password."','".$Doctor_name."','".$Qualification."','".$Working_hours."','".$Status."','".$Location."','$dbdatetime','$dbdatetime')";

		$insertRes=$DBCONN->query($insertQry);
		if($insertRes) {
			header("Location:manage_doctors.php?msg=1");
			exit;
		}
	}
}
include('includes/header.php');
?>
<!-- Pickers -->
<script type="text/javascript" src="../plugins/pickadate/picker.js"></script>
<script type="text/javascript" src="../plugins/pickadate/picker.date.js"></script>
<script type="text/javascript" src="../plugins/pickadate/picker.time.js"></script>
<script type="text/javascript" src="../plugins/bootstrap-colorpicker/bootstrap-colorpicker.min.js"></script>
<script type="text/javascript" src="../plugins/fileinput/fileinput.js"></script>
<script type="text/javascript" src="../plugins/bootstrap-wysihtml5/wysihtml5.min.js"></script>
<script type="text/javascript" src="../plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.min.js"></script>
<!-- Form Validation -->
<script type="text/javascript" src="../plugins/validation/jquery.validate.min.js"></script>
<script type="text/javascript" src="../plugins/validation/additional-methods.min.js"></script>
<!-- Noty -->
<script type="text/javascript" src="../plugins/noty/jquery.noty.js"></script>
<script type="text/javascript" src="../plugins/noty/layouts/top.js"></script>
<script type="text/javascript" src="../plugins/noty/themes/default.js"></script>
<!-- Demo JS -->
<script type="text/javascript" src="../assets/js/custom.js"></script>
<script type="text/javascript" src="../assets/js/demo/form_validation.js"></script>
<script type="text/javascript" src="../assets/js/demo/ui_general.js"></script>
<div id="content">
			<div class="container">				
				<!--=== Page Header ===-->
				<div class="page-header">
					<div class="page-title">
						<h3>Manage Doctors</h3>
						<!-- <span>Add/Edit Doctor</span> -->
					</div>					
				</div>
				<!-- /Page Header -->

				

				<!--=== Page Content ===-->
				<!--=== Full Size Inputs ===-->
				<div class="row">
					<div class="col-md-12">
						<div class="widget box">
							<div class="widget-header">
								<h4><i class="icon-reorder"></i><?php echo $Mode;?> Doctor Information</h4>
								 <!-- <div class="toolbar no-padding">
									<div class="btn-group">
										<span class="btn btn-xs widget-collapse"><i class="icon-angle-down"></i></span>
									</div>
								</div>  -->
							</div>
							<div class="widget-content">
								
						<form class="form-horizontal row-border" method='POST' id="validate-1" action="">		
							<input type='hidden' name="hdn_mode" value="add">
								 <input type="hidden" name="hdn_doctor_id" id="hdn_doctor_id" value="<?php echo $doctor_id ?? '';?>">
							<div class="form-group">
								<label class="col-md-3 control-label">Doctor Name:<span class="required">*</span></label>
								<div class="col-md-9"><input type="text" name="doctor_name" class="form-control required" value="<?php echo ucwords($doctor_name ?? '') ;?>" data-msg-required="Please enter doctor name."></div>
							</div>

							<div class="form-group">
								<label class="col-md-3 control-label">Doctor Qualification:<span class="required">*</span></label>
								<div class="col-md-9"><input type="text" name="qualification" class="form-control required" value="<?php echo $doctor_qualification ?? '';?>" data-msg-required="Please enter doctor qualification."></div>
							</div>
							<div class="form-group">
								<label class="col-md-3 control-label">Doctor working hours:</label>
								<div class="col-md-9"><textarea rows="5"  class="form-control wysiwyg wysihtml5-editor" name="doctor_working_hours" data-msg-required="Please enter doctor working hours." ><?php echo $doctor_working_hours ?? '';?></textarea></div>
							</div>
							
							<div class="form-group">
										<label class="col-md-3 control-label">Status<span class="required">*</span></label>
										<div class="col-md-9">
											<label class="radio"><input type="radio" name="status" class="required" value="Active" <?php echo ($status ?? '') == 'Active' ? ' checked' : ''; ?> data-msg-required="Please select status.">Active</label>
											<label class="radio"><input type="radio" name="status" value="In-Active" <?php echo ($status ?? "") == 'In-Active' ? "checked" : "";?>>In-Active</label>
											<label class="radio"><input type="radio" name="status" value="Leave" <?php echo ($status ?? "") == 'Leave' ? "checked" : "";?>>Leave</label>
											<label for="status" class="has-error help-block" generated="true" style="display:none;"></label>
										</div>
									</div>
							<!-- <div class="form-group">
										<label class="col-md-3 control-label">Location:<span class="required">*</span></label>			<div class="col-md-9">
											<select class="form-control required" id="culture" name="location">
												<option value="">Select Location</option>
												<option value="Burswood" <?php if($location=="Burswood") { echo " selected";}?>>Burswood</option>
												<option value="Claremont" <?php if($location=="Claremont") { echo " selected";}?>>Claremont</option>
											</select>
										</div>
									</div> -->

									

							 <div class="form-group">
								<label class="col-md-3 control-label">Username:<span class="required">*</span></label>
								<div class="col-md-9"><input type="text" name="username" class="form-control required username" value="<?php echo $username ?? '';?>" data-msg-required="Please enter username."></div>
							</div>

                            <div class="form-group">
								<label class="col-md-3 control-label">Email:<span class="required">*</span></label>
								<div class="col-md-9"><input type="text" name="email" class="form-control required email" value="<?php echo $email ?? '';?>" data-msg-required="Please enter email."></div>
							</div>
							<div class="form-group">
								<label class="col-md-3 control-label">Password:<span class="required"><?php echo $Mode == "Edit" ? '' : '*'; ?></span></label>
								<div class="col-md-9"><input type="password" name="password" class="form-control <?php echo $Mode == "Edit" ? '' : 'required' ;?>" data-msg-required="Please enter password." ></div>
							</div>
							<div class="form-actions">
								
								 <input type="button" class="btn btn-primary pull-right" onclick="document.location='manage_doctors.php'" value="Cancel">
								 <input type="submit" value="<?php echo $Value;?> Doctor" class="btn btn-primary pull-right">
							</div>
							
						</form>
					</div>
						</div>
					</div>
				</div>				
				<!-- /Page Content -->
			</div>
			<!-- /.container -->

		</div>
		
<?php
include("includes/footer.php");
?>