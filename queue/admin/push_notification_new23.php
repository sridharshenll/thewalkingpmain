<?php
require '../third_party/vendor/autoload.php';
include("../includes/configure.php");

use Kreait\Firebase\Factory;
use Kreait\Firebase\Messaging\CloudMessage;

$firebase = (new Factory)
    ->withServiceAccount('walkingp-6eaf3-firebase-adminsdk-9qfy7-5d7379d2e2.json');
$messaging = $firebase->createMessaging();
$registrationTokens = [
    'fQ5Bu26OI0BivOG-wIjMJO:APA91bFCyZ9GbSvLabg54fK7Gm614TUcyz0ZVu9pDE3nOdLzP774KAgfZDKiBrKo3TEZQgtU93o32qaC2m5Tzb543YRYFaoe5UMrluCVvKlfR0YXsjO8oEf9KTY3W7RHB_fTjrLF5hnK'
];
foreach ($registrationTokens as $token) {
    $message = CloudMessage::withTarget('token', $token);

    try {
        $messaging->send($message, true); // 'true' for dry run
        echo "Token {$token} is valid." . PHP_EOL;
    } catch (\Kreait\Firebase\Exception\Messaging\NotFound $e) {
        echo "Token {$token} is invalid: " . $e->getMessage() . PHP_EOL;
    } catch (\Kreait\Firebase\Exception\Messaging\InvalidMessage $e) {
        echo "Token {$token} is invalid: " . $e->getMessage() . PHP_EOL;
    } catch (\Kreait\Firebase\Exception\Messaging\MessagingError $e) {
        echo "Token {$token} is invalid: " . $e->getMessage() . PHP_EOL;
    } catch (\Throwable $e) {
        echo "An error occurred: " . $e->getMessage() . PHP_EOL;
    }
}
?>