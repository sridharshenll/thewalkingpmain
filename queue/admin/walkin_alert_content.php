<?php
include('../includes/configure.php');
include("includes/session_check.php");

$Msg = $_GET['msg'] ?? '';
$Message = "";
$alert = '';
if(isset($_GET['msg']) && trim($_GET['msg']) != "") {
	if($_GET['msg'] == 1) {
		$Message = "Content updated successfully";
		$alert = 'success';
	} else {
		$Message = "Somthing went wrong!. Please try again later.";
		$alert = 'danger';
	}
}

if($_SERVER["REQUEST_METHOD"] == "POST" && !empty($_POST["content"])) {
	$content = addslashes(trim($_POST["content"]));
	$updateQry = "update tbl_content set cnt_content='".$content."',cnt_moddate=now() where cnt_pagename='only_walkin_content' and location='".$_SESSION["location"]."'";
	$updateRes = $DBCONN->query($updateQry);
	if ($updateRes) {
		header("Location:walkin_alert_content.php?msg=1");
		exit;
	}
}

if(!empty($_SESSION["location"])) {
	$getQry = "select * from tbl_content where cnt_pagename='only_walkin_content' and location='".$_SESSION["location"]."'";
	$getRes = $DBCONN->query($getQry);
	$getData = $getRes->fetch(PDO::FETCH_ASSOC);
}
include('includes/header.php');
?>
<script type="text/javascript" src="tinymce/tinymce.min.js"></script>
<script>
	tinymce.init({
		selector: "textarea",
		plugins: [
		"advlist autolink autosave link image lists charmap print preview hr anchor pagebreak",
		"searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking",
		"table contextmenu directionality emoticons template textcolor paste fullpage textcolor colorpicker textpattern"
		],

		toolbar1: "undo redo  styleselect bold italic alignleft aligncenter alignright alignjustify  fontselect fontsizeselect bullist numlist ",
		toolbar2: "outdent indent image forecolor backcolor",
		menubar: true,
		toolbar_items_size: 'medium',
		style_formats: [
			{
				title: 'Bold text',
				inline: 'b'
			},
			{
				title: 'Red text',
				inline: 'span',
				styles: {
					color: '#ff0000'
				}
			},
			{
				title: 'Red header',
				block: 'h1',
				styles: {
					color: '#ff0000'
				}
			},
			{
				title: 'Example 1',
				inline: 'span',
				classes: 'example1'
			},
			{
				title: 'Example 2',
				inline: 'span',
				classes: 'example2'
			},
			{
				title: 'Table styles'
			},
			{
				title: 'Table row 1',
				selector: 'tr',
				classes: 'tablerow1'
			}
		],
		templates: [{
			title: 'Test template 1',
			content: 'Test 1'
			}, {
			title: 'Test template 2',
			content: 'Test 2'
			}],
		relative_urls: false,
		remove_script_host: false,
		images_upload_url: 'tinymce/postAcceptor.php',
		images_upload_base_path: 'tinymce/',
		images_upload_credentials: false,
	});
</script>
<!-- Pickers -->
<script type="text/javascript" src="../plugins/pickadate/picker.js"></script>
<script type="text/javascript" src="../plugins/pickadate/picker.date.js"></script>
<script type="text/javascript" src="../plugins/pickadate/picker.time.js"></script>
<script type="text/javascript" src="../plugins/bootstrap-colorpicker/bootstrap-colorpicker.min.js"></script>
<script type="text/javascript" src="../plugins/fileinput/fileinput.js"></script>
<script type="text/javascript" src="../plugins/bootstrap-wysihtml5/wysihtml5.min.js"></script>
<script type="text/javascript" src="../plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.min.js"></script>
<!-- Form Validation -->
<script type="text/javascript" src="../plugins/validation/jquery.validate.min.js"></script>
<script type="text/javascript" src="../plugins/validation/additional-methods.min.js"></script>
<!-- Noty -->
<script type="text/javascript" src="../plugins/noty/jquery.noty.js"></script>
<script type="text/javascript" src="../plugins/noty/layouts/top.js"></script>
<script type="text/javascript" src="../plugins/noty/themes/default.js"></script>
<!-- Demo JS -->
<script type="text/javascript" src="../assets/js/custom.js"></script>
<script type="text/javascript" src="../assets/js/demo/form_validation.js"></script>
<script type="text/javascript" src="../assets/js/demo/ui_general.js"></script>
<style>
	input.has-error {
		border-color: red !important;
	}
</style>
<div id="content">
	<div class="container">				
		<div class="page-header" style="margin-top:60px;">
			<div class="page-title"> </div>
		</div>
		<?php if (!empty($Message)) { ?>
			<div class="alert fade in alert-<?php echo $alert; ?>">
				<i class="icon-remove close" data-dismiss="alert"></i>
				<?php echo $Message; ?>
			</div>					
		<?php } ?>
		<div class="row">
			<div class="col-md-12">
				<div class="widget box">
					<div class="widget-header"> <h4><i class="icon-reorder"></i>Add Content</h4> </div>
						<div class="widget-content">
							<form class="form-horizontal row-border" method='POST'  name="edit_content" id="validate-5"  enctype="multipart/form-data">
								<div class="form-group">
									<label class="control-label col-md-2">Description <span class="required">*</span></label>
									<div class="col-md-8">
										<textarea name="content" id="cont_desc" class="required" rows="25" cols="25"><?php echo $getData['cnt_content'] ?? '';?></textarea>
									</div>
								</div>
								<div class="form-actions">
									<input type="button" value="Save" class="btn btn-primary pull-right" id="submit_btn">
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>				
		</div>
	</div>
<?php include("includes/footer.php"); ?>

<script>
$(document).ready(function(){
	$("#submit_btn").click(function() {
		var wordCount = tinymce.get('cont_desc').plugins.wordcount.getCount();
		if($("#validate-5").validate()) {
			$("#validate-5").submit();
		}
	});
});
</script>