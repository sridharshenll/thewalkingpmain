<?php
include("../includes/configure.php");
include("includes/session_check.php");
if(count($_POST['exportpatient']) > 1)
	$pt_ids = implode(',',$_POST['exportpatient']);
else
	$pt_ids = $_POST['exportpatient'][0];
$Arranswers = array();
$getDemoQry = "select * from tbl_patient where location='".$_SESSION["location"]."' and patient_id in($pt_ids) order by FIElD(patient_status, 'Apointment fixed', 'Canceled', 'Visited') desc";
// $update_export = $DBCONN->query("update tbl_patient set export='yes' where patient_id in($pt_ids)");
$getDemoRes = $DBCONN->query($getDemoQry);
$getDemoCnt = $getDemoRes->rowCount();
$getDemoCnt = 1;
if($getDemoCnt > 0) {
	foreach($getDemoRes->fetchAll(PDO::FETCH_ASSOC) as $getDemoRow)	{
		$Date_of_birth = "";
		$expiry_date = "";
		$pension_card_expiry_date = "";
		if($getDemoRow["dob"] != "") {
			if($getDemoRow["dob"] != "0000-00-00"){
				$Date_of_birth = date('d-m-Y',strtotime(stripslashes($getDemoRow["dob"])));
			} else {
				$Date_of_birth = "";
			}
		} else {
			$Date_of_birth = "";
		}
        if($getDemoRow["expiry_date"] != "") {
			if($getDemoRow["expiry_date"] != "0000-00-00") {
				$expiry_date = date('d-m-Y',strtotime(stripslashes($getDemoRow["expiry_date"])));
			}
			else {
				$expiry_date = "";
			}
		}
		else{
			$expiry_date="";
		}
		if($getDemoRow["pension_card_expiry_date"] != "") {
			if($getDemoRow["pension_card_expiry_date"] != "0000-00-00"){
				$pension_card_expiry_date = date('d-m-Y',strtotime(stripslashes($getDemoRow["pension_card_expiry_date"])));
			}
			else{
				$pension_card_expiry_date = "";
			}
		}
		else{
			$pension_card_expiry_date = "";
		}
		
		$doctor_id = stripslashes($getDemoRow["doctor_id"]);
		if($doctor_id != "0") {
			$getdoctorQry = "select * from tbl_staff where staff_id='".$doctor_id."'";
			$getdoctorRes = $DBCONN->query($getdoctorQry);
			$getdoctoRow = $getdoctorRes->fetch(PDO::FETCH_ASSOC);
			$doctor_name = stripslashes($getdoctoRow["staff_name"]);
		} else {
			$doctor_name = "First Available Doctor";
		}

		$Arranswers[] = stripslashes($getDemoRow["family_name"] ?? '')."\t".stripslashes($getDemoRow["patient_name"] ?? '')."\t".stripslashes($getDemoRow["location"] ?? '')."\t".stripslashes($getDemoRow["location_option"] ?? '')."\t".$doctor_name."\t".$Date_of_birth."\t".stripslashes($getDemoRow["medicare_no"] ?? '')."\t".stripslashes($getDemoRow["reference_no"] ?? '')."\t".$expiry_date."\t".stripslashes($getDemoRow["address"] ?? '')."\t".stripslashes($getDemoRow["suburb"] ?? '')."\t".stripslashes($getDemoRow["post_code"] ?? '')."\t".stripslashes($getDemoRow["home_phone"] ?? '')."\t".stripslashes($getDemoRow["mobile_phone"] ?? '')."\t".stripslashes($getDemoRow["pension_card_no"] ?? '')."\t".$pension_card_expiry_date."\t".stripslashes($getDemoRow["token_number"] ?? '')."\t".stripslashes($getDemoRow["patient_status"] ?? '');
	}
	$answers = implode("\n",$Arranswers);
}
$heading = "Family Name\tPatient_name\tlocation\tVisited\tDoctor\tDate Of Birth\tMedicare Number\tReference NO\tExpiry Date\tAddress\tSuburb\tPost_code\tHome Phone\tMobile Phone\tPension Card No\tPension Card Expiry Date\tToken Number\tPatient Status\n";
if ($heading != "") {
    $tdate = date('m_d_Y');
    $strFileName = "Patient-List." . time() . $tdate . ".xls";
    $strDirname = "others/export/";

    // Create directory if it doesn't exist
    if (!is_dir($strDirname)) {
        mkdir($strDirname, 0777, true);
    }
    $strXlFileNm = $strDirname . $strFileName;
    $readed_contents = $heading . $answers;
    file_put_contents($strXlFileNm, $readed_contents);
    if (is_file($strXlFileNm)) {
        $contents = file_get_contents($strXlFileNm);
        header("Content-Type: application/octet-stream");
        header("Content-Disposition: attachment; filename=$strFileName");
        header("Content-Length: " . filesize($strXlFileNm));
        echo $contents;
        unlink($strXlFileNm);
    }
}
// if($heading != "") {
// 	$tdate = date('m_d_Y');
// 	$strFileName = "Patient-List.".time().$tdate.".xls";
// 	$strDirname = "others/export/";
// 	$strXlFileNm = $strDirname.$strFileName;
// 	$rhandle= @fopen($strXlFileNm,"r");
// 	$readed_contvents = @fread($rhandle,filesize($strXlFileNm));
// 	@fclose($rhandle);

// 	$readed_contents = $heading.$answers;
// 	$strXlFileNametoDwnld = $strDirname.$strFileName;
// 	$handle = fopen($strXlFileNm,'w');
// 	fwrite($handle,$readed_contents);
// 	fclose($handle);

// 	if(is_file($strXlFileNm)) {
// 		$contents = '';
// 		$handle=@fopen($strXlFileNm,"r");
// 		if($handle) {
// 			$contents = fread($handle, filesize($strXlFileNm));
// 			fclose($handle);
// 		}
// 		header("Content-Type: application/octet-stream");
// 		header("Content-Disposition: attachment; filename=$strFileName");
// 		header("Content-Length:".filesize($strXlFileNm));
// 		print $contents;
// 		unlink($strXlFileNm);
// 	}
// }

?>