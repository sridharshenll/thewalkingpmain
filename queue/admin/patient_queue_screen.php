<?php
include("../includes/configure.php");
include("includes/session_check.php");

$Msg = $_GET['msg'] ?? '';
$Message = "";
$alert = '';
if(isset($_GET['msg']) && trim($_GET['msg']) != "") {
	if($_GET['msg'] == 1) {
		$Message = "Patient details added successfully";
		$alert = 'success';
	} elseif ($_GET['msg'] == 2) {
		$Message = "Updated successfully";
		$alert = 'success';
	} elseif ($_GET['msg'] == 3) {
		$Message = "Status updated successfully";
		$alert = 'success';
	} else {
		$Message = "Somthing went wrong!. Please try again later.";
		$alert = 'danger';
	}
}

if (isset($_POST["hidden_id"])) {
	$hidden_id = addslashes(trim($_POST["hidden_id"]));
	$hidden_prev_status = addslashes(trim($_POST["hidden_prev_status"]));
	$hidden_status = addslashes(trim($_POST["hidden_status"]));
	$hidden_token = addslashes(trim($_POST["hidden_token"]));
	if ($hidden_status != $hidden_prev_status && $hidden_status == "Apointment fixed"){
		$getTknQry = "select count(*) as token_cnt from tbl_patient where register_date='".date('Y-m-d')."' and location='".$_SESSION["location"]."'";
		$getTknRes = $DBCONN->query($getTknQry);
		$getTknRow = $getTknRes->fetch(PDO::FETCH_ASSOC);
		$patient_token_number = $getTknRow["token_cnt"] + 1;
	} else {
		$patient_token_number = $hidden_token;
	}

	if($hidden_status == 'Arrived') {
		$updateQry = "update tbl_patient set modified_date='$dbdatetime', arrived = 1 where patient_id='".$hidden_id."'";
	} else {
		$updateQry = "update tbl_patient set patient_status='".$hidden_status."',token_number='".$patient_token_number."',modified_date='$dbdatetime' where patient_id='".$hidden_id."'";
	}
	
	$updateRes = $DBCONN->query($updateQry);
	if($updateRes){
		header("Location:patient_queue_screen.php?msg=3");
		exit;
	}
}

if (isset($_POST["reg_patient_id"])) {
	$reg_patient_id = addslashes(trim($_POST["reg_patient_id"]));
	$regtime_value = trim($_POST["regtime_value"]);
	$reg_time = date('H:i:s', strtotime($regtime_value));
	$updateQry = "update tbl_patient set reg_time='".$reg_time."',modified_date='$dbdatetime' where patient_id='".$reg_patient_id."'";
	$updateRes = $DBCONN->query($updateQry);
	if($updateRes){
		header("Location:patient_queue_screen.php?msg=2");
		exit;
	}
}

include("includes/header.php");
?>

<script src="http://code.jquery.com/jquery-1.7.2.min.js"></script>
<script src="http://code.jquery.com/ui/1.8.21/jquery-ui.min.js"></script>
<script src="../js/jquery.ui.touch-punch.min.js" language="javascript"></script>

<?php if($_SESSION["role"]=="admin") { ?>
<script language="javascript">
	url = document.location.href;
	xend = url.lastIndexOf("/") + 1;
	var base_url = url.substring(0, xend);
	$(document).ready(function() {
		$( "#sortable" ).sortable({
			update: function (event, ui) {
			var data = $(this).sortable('serialize');
			// POST to server using $.post or $.ajax
			$.ajax({
				data: data,
				type: 'POST',
				url: '../includes/savequeue.php',
				success: function(strRplytxt) {
				}
			});
		}});
		$( "#draggable" ).draggable({
			connectToSortable: "#sortable",
			helper: "clone",
			revert: "invalid"
		});
	});
</script>
<?php } ?>
<style type="text/css">
	.widget.box .widget-content {
		padding:0px;
		padding-top: 10px !important;
		padding-bottom: 10px !important;
		position: relative;
		background-color: #fff;
	}
	label{
		white-space:nowrap;
	}
	@media (min-width: 980px) {
		.margn_top{
			margin-top:60px;
		}
	}
	.update-reg {
		display:none;
	}

	.dotyes {
		height: 8px;
		width: 8px;
		border-radius: 50%;
		display: inline-block;
	}
	#mngptrecds tbody tr .applyellow{
		background-color:yellow;
	}
	.tele-user-dot {
		background-color: #000000;
		border-radius: 50%;
		padding: 0px 9px;
		margin-left: 5px;
	}
</style>
		<div id="content">
			<div class="container">
				<div class="page-header">
					<div class="page-title"> </div>
				</div>
				<?php if (!empty($Message)) { ?>
					<div class="alert fade in alert-<?php echo $alert; ?>" style="margin-top:64px;margin-bottom:-51px">
						<i class="icon-remove close" data-dismiss="alert"></i>
						<?php echo $Message; ?>
					</div>
				<?php } ?>
				<div class="row" >
					<div class="col-md-12 margn_top">
						<div class="row">
							<div class="col-md-12">
								<div style="padding-top:5px;padding-bottom:5px;float:right;">
									<h5>Date:<?php echo date('d/m/Y');?></h5>
								</div>
							</div>
						</div>
						<div class="table-responsive">
							<div class="widget box">
								<div class="widget-header">
									<h4><i class="icon-reorder"></i>Patients Queue</h4>
									<input type="button" class="btn btn-primary pull-right" onclick="document.location='jumpqueue.php'" value="Add Patient">
								</div>
								<div class="widget-content">
								    <form name="regtime_form" id="regtime_form" method="post">
								        <INPUT TYPE="hidden" NAME="reg_patient_id" id="reg_patient_id" >
										<INPUT TYPE="hidden" NAME="regtime_value" id="regtime_value">
							        </form>
									<form name="patient_form" id="patient_form" method="post">
										<INPUT TYPE="hidden" NAME="hidden_id" id="hidden_id" >
										<INPUT TYPE="hidden" NAME="hidden_prev_status" id="hidden_prev_status">
										<INPUT TYPE="hidden" NAME="hidden_status" id="hidden_status">
										<INPUT TYPE="hidden" NAME="hidden_token" id="hidden_token">
										<table class="table table-striped table-bordered table-hover table-checkable">
											<thead>
												<tr>
												 	<th width="5%">No&nbsp;</th>
													<th width="15%">Doctor</th>
													<th width="15%">Patient</th>
													<th width="10%" >Date of Birth</th>
													<th width="10%" >Full Details</th>
													<th  width="15%">Time (waiting since)</th>
													<th width="15%" class="hidden-xs visible-md visible-sm visible-lg">Status</th>
													<th width="20%" class="visible-xs hidden-md hidden-sm hidden-lg">Status</th>
												</tr>
											</thead>
											<tbody>
												<tr>
													<td colspan='8' style="padding:0px">
											  			<ul id="sortable" style="margin:0px;border:0px solid red;text-indent:0px;padding-left:0px">
														<?php
														if($_SESSION["location"] == 'Telehealth') {
															$getPatientsQry = "select * from tbl_patient where location='".$_SESSION["location"]."' and register_date='".date('Y-m-d')."' and patient_status='Appointment fixed' ORDER BY reg_time asc";
														} else {
															$getPatientsQry = "select * from tbl_patient where location in('".$_SESSION["location"]."','Telehealth') and register_date='".date('Y-m-d')."' and patient_status='Appointment fixed' ORDER BY reg_time asc";
														}
														$getPatientsRes = $DBCONN->query($getPatientsQry);
														$getPatientsCnt = $getPatientsRes->rowCount();
														if ($getPatientsCnt > 0) {
											 				$i = 1;
												 			foreach($getPatientsRes->fetchAll(PDO::FETCH_ASSOC) as $getPatientsRow) {
																$doctor_id = $getPatientsRow["doctor_id"];
																$location_option = $getPatientsRow["location_option"];
																$color_status = "";
																$dotcolor = "dotyes";
																$bck_status = "";
																$colorstatus = $getPatientsRow["color_status"];
																if ($colorstatus == "#ffff1d") {
																	$bck_status = "applyellow";
																} else {
																	$color_status = $getPatientsRow["color_status"];
																	$bck_status = "";
																}
																if ($doctor_id > 0 && $doctor_id != "") {
																	$getDocQry = "select * from tbl_staff where staff_id='".$doctor_id."'";
																	$getDocRes = $DBCONN->query($getDocQry);
																	$getDocRow = $getDocRes->fetch(PDO::FETCH_ASSOC);
																	$doctor_name = ucwords(stripslashes($getDocRow["staff_name"]));
																} else {
																	$doctor_name = "First Available Doctor";
																}
																$Patient_status = stripslashes($getPatientsRow["patient_status"]);
																if ($getPatientsRow["dob"]!="") {
																	list($year,$month,$day) = explode("-",$getPatientsRow["dob"]);
																	$dob = $day."/".$month."/".$year;
																	if ($dob=="//") {
																		$dob='';
																	}
																}
																$teleCheck = $_SESSION["location"] !== 'Telehealth' ? $getPatientsRow["location"] == 'Telehealth' ? 'tele-user-dot' : '' : '';
												 			?>
															<li id='item-<?php echo $getPatientsRow["patient_id"];?>'  style="list-style-type:none;width:100%">
																<table class="table table-striped table-bordered table-hover table-checkable"  id='mngptrecds'>
																	<tr class="<?php echo $bck_status;?>">
							                                            <td width="5%" class="<?php echo $bck_status;?>"><?php echo $i;?><span class="<?php echo $teleCheck;?>"></span></td>
																		<td width="15%" class="<?php echo $bck_status;?>"><?php echo $doctor_name;?></td>
																		<td width="15%" class="<?php echo $bck_status;?>">
																		    <?php if($getPatientsRow["arrived"] == 1) { ?>
																		        <img src="../assets/img/tic.png">
    																		<?php } else { ?>
    																		    <span class="<?php echo $dotcolor ?>" style="background-color:<?php echo $color_status?>"></span>
    																	    <?php } ?>
																		    <?php echo ucwords(stripslashes($getPatientsRow["patient_name"]).' '.stripslashes($getPatientsRow["family_name"])); ?>
																		</td>
																		<td width="10%" class="<?php echo $bck_status;?>"><?php echo $dob ?></td>
																		<td width="10%" class="<?php echo $bck_status;?>"><a href="edit_patient.php?redirec=pq&staff_id=<?php echo $getPatientsRow["patient_id"];?>">View Details</a></td>
																		<td width="15%" class="<?php echo $bck_status;?>">
																		    <div id="regtime-<?php echo $getPatientsRow["patient_id"];?>">
																		        <?php echo date('g:i A',strtotime($getPatientsRow["reg_time"]));?>
																		        <span class="btn-group">
																		            <a type="button" class="btn btn-xs edit_regtime" data-id="<?php echo $getPatientsRow["patient_id"];?>" style="border: 0;background: transparent;margin-bottom: 6px;padding-left: 5px;">
																		                <i class="icon-pencil"></i></a>
																		        </span>
																		    </div>
																		    <div class="update-reg" id="update-reg-<?php echo $getPatientsRow["patient_id"];?>">
																		        <input type="time" id="reg-time-<?php echo $getPatientsRow["patient_id"];?>" style="padding: 0 6px !important;border: 1px solid;" value="<?php echo date('h:i',date(strtotime($getPatientsRow["reg_time"])));?>">
																		        <a type="button" data-id="<?php echo $getPatientsRow["patient_id"];?>" class="btn btn-rounded btn-sm btn-info submit-reg-btn" style="padding: 2px 13px;">Save</a>
																		    </div>
																	    </td>
																		<td class="hidden-xs visible-md visible-sm visible-lg <?php echo $bck_status;?>"  width="15%">
																			<select  id="patient_bg<?php echo $i;?>" class="full-width-fix" onchange="getvalue('<?php echo $getPatientsRow["patient_id"];?>','<?php echo $Patient_status;?>',this.value,'<?php echo $getPatientsRow["token_number"];?>')">
																				<option value="">Select</option>
																				<option value="Visited">Visited</option>
																				<option value="Cancelled">Cancelled</option>
																				<?php if($getPatientsRow["arrived"] != 1) { ?>
																				    <option value="Arrived">Arrived</option>
																				<?php } ?>
																			</select>
																			<a href="download.php?id=<?php echo $getPatientsRow["patient_id"] ?>"  class="btn  waves-effect waves-light btn-rounded btn-sm btn-info customeditbtn"><i class="fa fa-download"></i> Download</a>
																		</td>
																		<td class="visible-xs hidden-md hidden-sm hidden-lg"  width="15%">
																			<div >
																				<select  id="patient_<?php echo $i;?>" class="select full-width-fix" onchange="getvalue('<?php echo $getPatientsRow["patient_id"];?>','<?php echo $Patient_status;?>',this.value,'<?php echo $getPatientsRow["token_number"];?>')">
																					<option value="">Select</option>
																					<option value="Visited">Vis</option>
																					<option value="Cancelled">Can</option>
																					<option value="Arrived">Arr</option>
																				</select>
																			</div>
																		</td>
																	</tr>
																</table>
															</li>
															<?php $i++; } ?>
														</ul>
													</td>
												</tr>	
												<?php } else { ?>
												<tr>
													<td colspan="7"><center>No patients Found.</center></td>
												</tr>
												<?php } ?>
											</tbody>
										</table>
									</form>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<?php include("includes/footer.php"); ?>
<SCRIPT LANGUAGE="JavaScript">
$(document).on("click", ".edit_regtime", function() {
    let dataId = $(this).attr('data-id');
    let regtime = $(`#regtime-${dataId}`).val();
    console.log(regtime);
    $(`#regtime-${dataId}`).hide();
    $(`#update-reg-${dataId}`).show();
    $('#reg_patient_id').val(dataId);
});
$(document).on("click", ".submit-reg-btn", function() {
    let dataId = $(this).attr('data-id');
    let regtime = document.getElementById(`reg-time-${dataId}`).value;
    $('#regtime_value').val(regtime+":00");
    document.regtime_form.submit();
});
function getvalue(id,prev_value,status,token) {
	$("#hidden_id").val(id);
	$('#hidden_prev_status').val(prev_value);
	$('#hidden_status').val(status);
	$('#hidden_token').val(token);
	document.patient_form.submit();
}
function funexport() {
	try {
		with(document.patient_form) {
			action='patient_list-export.php';
			submit();
			return true;
			action = '';
		}
	} catch(e) {
		alert(e)
	}
}
</SCRIPT>