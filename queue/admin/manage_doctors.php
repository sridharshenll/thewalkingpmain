<?php
include('../includes/configure.php');
include("includes/session_check.php");
$Msg = $_GET['msg'] ?? '';
$doctor_id = $_GET['staff_id'] ?? '';
if($Msg == 1){
	$Message="Doctor Information added successfully.";
}
else if($Msg == 2){
	$Message = "Doctor Information updated successfully.";
}
else if($Msg == 3){
	$Message = "Doctor Information deleted successfully.";
}
if($doctor_id != ""){
    $DeleteQry = "delete from tbl_staff where staff_id='".$doctor_id."'";
	$DeleteRes = $DBCONN->query($DeleteQry);
	if($DeleteRes){
		header("Location:manage_doctors.php?msg=3");
		exit;
	}
}
include('includes/header.php');
?>
<style>
#mngptrecds1_info{
display:none;
}
#mngptrecds1_length{
display:none;
}
</style>
		<!-- Center Main page Content -->
		<div id="content">
			<div class="container">
				<!--=== Page Header ===-->
				<div class="page-header">
					<div class="page-title">
						<!-- <h3>Doctors List</h3> -->
					</div>					
				</div>
				<!-- /Page Header -->
				<?php if($Msg != '') { ?>
					<div class="alert fade in alert-success">
						<i class="icon-remove close" data-dismiss="alert"></i>
						<?php echo $Message; ?>
					</div>
				<?php } ?>
				<!--=== Responsive DataTable ===-->
				<div class="row">
					
					<div class="col-md-12">
						<div class="row">
							<div class="col-md-12">
								<div style="padding-top:5px;padding-bottom:5px;float:right;"><h5><!-- DATE:<?php echo date('d-m-Y');?> --></h5>
								</div>
							</div>
						</div>
						<div class="widget box">
							<div class="widget-header">
								<h4><i class="icon-reorder"></i>Doctors</h4>
								<input type="button" class="btn btn-primary pull-right" onclick="document.location='edit_doctor.php'" value="Add Doctor">					
							</div>
							<div class="widget-content">
								<table class="table table-striped table-bordered table-hover table-checkable" id='mngptrecds1'>
									<thead>
										<tr>
											<th>No</th>
											<th >Doctors Name</th>
											<th >Qualification Name</th>
											<th class="hidden-xs" >Status</th>											
											<th class="align-center" >Action</th>
										</tr>
									</thead>
									<tbody>
										<?php
											$getQry = "select * from tbl_staff where role='doctor' and location='".$_SESSION["location"]."' order by staff_id desc";
											$getRes = $DBCONN->query($getQry);
											$getCnt = $getRes->rowCount();
											if($getCnt > 0){
												$sno = 1;
												$data = $getRes->fetchAll(PDO::FETCH_ASSOC);
												foreach($data as $key => $getRow){
													if($sno%2 == 0) {
														$class = "odd gradeX";
													} else {
														$class = "even gradeC";
													}?>
													<tr class="<?php echo $class;?>">
														<td><?php echo $sno;?></td>
														<td><?php echo ucwords(stripslashes($getRow["staff_name"]));?></td>
														<td><?php echo stripslashes($getRow["staff_qualification"]);?></td>
														<td class="hidden-xs"><span class="label label-success"><?php echo stripslashes($getRow["status"]);?></span></td>
														<td class="align-center">
															<span class="btn-group">
																<a href="edit_doctor.php?staff_id=<?php echo stripslashes($getRow["staff_id"]);?>"
																class="btn btn-xs bs-tooltip" title="Edit"><i class="icon-pencil"></i></a>
																<a href="manage_doctors.php?staff_id=<?php echo stripslashes($getRow["staff_id"]);?>" class="btn btn-xs bs-tooltip" title="Delete" onclick="return confirm('Are you sure want to delete this doctor?')"><i class="icon-trash"></i></a>
															</span>
														</td>
													</tr>
												<?php $sno++; }
											} else {
												echo "<tr class=\"odd gradeX\"><td colspan=\"5\" style=\"text-align:center;\">No doctors found.</td></tr>";
											}
										?>
									</tbody>
								</table>
								<!--<div class="row">
									<div class="table-footer">										
										<div class="col-md-12">
											<ul class="pagination">
												<li class="disabled"><a href="javascript:void(0);">&larr; Prev</a></li>
												<li class="active"><a href="javascript:void(0);">1</a></li>
												<li><a href="javascript:void(0);">2</a></li>
												<li><a href="javascript:void(0);">3</a></li>
												<li><a href="javascript:void(0);">4</a></li>
												<li><a href="javascript:void(0);">Next &rarr;</a></li>
											</ul>
										</div>
									</div>
								</div> -->
							</div>
						</div>
					</div>
					<!-- /Table with Footer -->
							
				</div>
			</div>
		</div>
				<!-- /Responsive DataTable -->
				<!-- /Page Content -->
	</div>
			<!-- /.container -->
</div>
		<!-- /Center Main page Content -->
<?php if( ($_SESSION["waringpopup"] ?? 0) ? "waringpopup" : 0) { ?>
    <div id="myModal" class="modal fade" role="dialog" data-toggle="modal" data-backdrop="static" data-keyboard="false" >
	  	<div class="modal-dialog">
	    <!-- Modal content-->
			<div class="modal-content" style="margin-top: 150px;">
				<!-- <div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
						<h4 class="modal-title"></h4>
				</div> -->
				<div class="modal-body">
					<p style="text-align: center;font-size: 24px;margin-top: 15px;">Please confirm doctors on duty today!</p>
				</div>
				<div class="modal-footer" style="border-top: 0px solid #e5e5e5;">
					<button type="button" class="btn btn-success" id="okwarningalert" data-id="okbox" data-dismiss="modal">OK</button>
				</div>
			</div>
		</div>
	</div>
	<script>$("#myModal").modal('show');</script>
<?php } ?>
<?php include("includes/footer.php"); ?>
<SCRIPT LANGUAGE="JavaScript">
	$(document).ready( function() {
		$('#mngptrecds1').dataTable( {
			"oLanguage": {
				"sLengthMenu": "_MENU_ doctors",
			},
		"iDisplayLength": 10000000,
		"aLengthMenu": [[-1], ["All"]],
		"aaSorting": [[ 0, "asc" ]],
		"aoColumns": [
		{ "bSortable": false },
		null,
		{ "bSortable": false },
		{ "bSortable": false },
		{ "bSortable": false },
		]
		} );
		$('.dataTables_paginate').hide();
		

	} );

	$(document).on("click","#okwarningalert",function() {
		var popupbox = $("#okwarningalert").attr("data-id");
		var typepopup = "closepopup";
		if (popupbox == "okbox") {
			$.ajax({
				url:"ajax-popup.php",
				method:'GET',
				data:"post_type="+typepopup,
				success:function(data) {
					if (data == "Success") {
						$("#myModal").modal('hide');
					}
				}
			});

		} else {
			$("#myModal").modal('show');
		}
	})

</script>