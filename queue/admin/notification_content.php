<?php

include('../includes/configure.php');
include("includes/session_check.php");

if($_SERVER["REQUEST_METHOD"] == "POST" && !empty($_POST['title'])) {
	$title = stripslashes($_POST['title'] ?? '');
	$subject = stripslashes($_POST['subject'] ?? '');
	$description = stripslashes($_POST['content'] ?? '');
	$status = stripslashes($_POST['status'] ?? '0' == "on" ? '1' : '0');
	$location = stripslashes($_SESSION['location']);
	$type = 2;

	if(empty($_POST['id'])) {

		if (!empty($title) && !empty($subject)) {
			$insertContentQuery = $DBCONN->prepare("INSERT INTO tbl_notification_contents (title, subject, description, status, location) VALUES (:title, :subject, :description, :status, :location)");
			$result = $insertContentQuery->execute([
				':title' => $title,
				':subject' => $subject,
				':description' => $description,
				':status' => $status,
				':location' => $location,
			]);
		}

	} else {
		$getQry = "select * from tbl_notification_contents where id='".$_POST['id']."'";
		$getRes = $DBCONN->query($getQry);
		if($getRes->rowCount() > 0) {
			$updateQuery = $DBCONN->prepare("UPDATE tbl_notification_contents SET title = :title, subject = :subject, description = :description, status = :status, location = :location WHERE id = :id");
			$result = $updateQuery->execute([
				':title' => $title,
				':subject' => $subject,
				':description' => $description,
				':status' => $status,
				':location' => $location,
				':id' => $_POST['id'],
			]);
			$type = 3;
		}
	}

	if ($result) { 
		header("Location:notification_list.php?msg=".$type);
		exit;
	}
}

if($_SERVER["REQUEST_METHOD"] == "GET") { 
	$id = $_GET['notification_id'] ?? '';
	if(!empty($id)) {
		$getQry = "select * from tbl_notification_contents where id='".$id."'";
		$getRes = $DBCONN->query($getQry);
		$getData = $getRes->fetch(PDO::FETCH_ASSOC);
	}

}
include('includes/header.php');
?>

<script type="text/javascript" src="tinymce/tinymce.min.js"></script>
<script>
tinymce.init({
	selector: "textarea",
	plugins: [
	"advlist autolink autosave link image lists charmap print preview hr anchor pagebreak",
	"searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking",
	"table contextmenu directionality emoticons template textcolor paste fullpage textcolor colorpicker textpattern"
	],
	toolbar1: "undo redo  styleselect bold italic alignleft aligncenter alignright alignjustify  fontselect fontsizeselect bullist numlist ",
	toolbar2: "outdent indent image forecolor backcolor",
	menubar: true,
	toolbar_items_size: 'medium',
	style_formats: [
		{
			title: 'Bold text',
			inline: 'b'
		},
		{
			title: 'Red text',
			inline: 'span',
			styles: {
				color: '#ff0000'
			}
		},
		{
			title: 'Red header',
			block: 'h1',
			styles: {
				color: '#ff0000'
			}
		},
		{
			title: 'Example 1',
			inline: 'span',
			classes: 'example1'
		},
		{
			title: 'Example 2',
			inline: 'span',
			classes: 'example2'
		},
		{
			title: 'Table styles'
		},
		{
			title: 'Table row 1',
			selector: 'tr',
			classes: 'tablerow1'
		}
	],

	templates: [
		{
			title: 'Test template 1',
			content: 'Test 1'
		}, 
		{
			title: 'Test template 2',
			content: 'Test 2'
		}
	],
	relative_urls: false,
	remove_script_host: false,
	images_upload_url: 'tinymce/postAcceptor.php',
	images_upload_base_path: 'tinymce/',
	images_upload_credentials: false,
});
</script>

<!-- Pickers -->
<script type="text/javascript" src="../plugins/pickadate/picker.js"></script>
<script type="text/javascript" src="../plugins/pickadate/picker.date.js"></script>
<script type="text/javascript" src="../plugins/pickadate/picker.time.js"></script>
<script type="text/javascript" src="../plugins/bootstrap-colorpicker/bootstrap-colorpicker.min.js"></script>
<script type="text/javascript" src="../plugins/fileinput/fileinput.js"></script>
<script type="text/javascript" src="../plugins/bootstrap-wysihtml5/wysihtml5.min.js"></script>
<script type="text/javascript" src="../plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.min.js"></script>
<!-- Form Validation -->
<script type="text/javascript" src="../plugins/validation/jquery.validate.min.js"></script>
<script type="text/javascript" src="../plugins/validation/additional-methods.min.js"></script>
<!-- Noty -->
<script type="text/javascript" src="../plugins/noty/jquery.noty.js"></script>
<script type="text/javascript" src="../plugins/noty/layouts/top.js"></script>
<script type="text/javascript" src="../plugins/noty/themes/default.js"></script>
<!-- Demo JS -->
<script type="text/javascript" src="../assets/js/custom.js"></script>
<script type="text/javascript" src="../assets/js/demo/form_validation.js"></script>
<script type="text/javascript" src="../assets/js/demo/ui_general.js"></script>
<style>
	input.has-error {
		border-color: red !important;
	}
	input {
		border: 1px solid #ddd;
	}
</style>

<div id="content">
	<div class="container">				
		<div class="page-header" style="margin-top:60px;">
			<div class="page-title"> </div>
		</div>
		<?php if ($msg ?? FALSE) { ?>
			<div class="alert fade in alert-success">
				<i class="icon-remove close" data-dismiss="alert"></i>
				<?php echo $Message; ?>
			</div>					
		<?php } ?>
		<div class="row">
			<div class="col-md-12">
				<div class="widget box">
					<div class="widget-header"> <h4><i class="icon-reorder"></i>Add Content</h4> </div>
						<div class="widget-content">
							<form class="form-horizontal row-border" method='POST'  name="edit_content" id="validate-5"  enctype="multipart/form-data">
								<input type="hidden" name="id" value="<?php echo $getData['id'] ?? ''; ?>">
								<div class="form-group">
									<label class="control-label col-md-2">Title <span class="required">*</span></label>
									<div class="col-md-8">
										<input type="text" name="title" style="width: 100%;" value="<?php echo $getData['title'] ?? '';?>" class="notification_subject required">
									</div>
								</div>
								<div class="form-group">
									<label class="control-label col-md-2">Subject <span class="required">*</span></label>
									<div class="col-md-8">
										<input type="text" name="subject" style="width: 100%;" value="<?php echo $getData['subject'] ?? '';?>" class="notification_subject required">
									</div>
								</div>
								<div class="form-group">
									<label class="control-label col-md-2">Description <span class="required">*</span></label>
									<div class="col-md-8">
										<textarea name="content" id="cont_desc" class="required" rows="25" cols="25"><?php echo $getData['description'] ?? '';?></textarea>
									</div>
								</div>
								<div class="form-group">
									<label class="col-md-2" style="text-align: right;">Status</label>
									<div class="col-md-8">
										<input type="checkbox" id="status" name="status" <?php echo $getData['status'] ?? '' == 1 ? 'checked' : ''; ?> >
									</div>
								</div>
								<div class="form-actions">
									<input type="button" value="Save" class="btn btn-primary pull-right" id="submit_btn">
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>				
		</div>
	</div>
<?php include("includes/footer.php"); ?>
<script>

$(document).ready(function(){
	$("#submit_btn").click(function() {
		var wordCount = tinymce.get('cont_desc').plugins.wordcount.getCount();
		if($("#validate-5").validate()) {
			$("#validate-5").submit();
		}
	});
});
</script>