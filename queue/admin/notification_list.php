<?php

include("../includes/configure.php");
include("includes/session_check.php");

$Msg = $_GET['msg'] ?? '';
$Message = "";
$alert = '';
if(isset($_GET['msg']) && trim($_GET['msg']) != "") {
	if($_GET['msg'] == 3) {
		$Message = "Notification content updated successfully";
		$alert = 'success';
	} elseif ($_GET['msg'] == 1) {
		$Message = "Notification Sent successfully";
		$alert = 'success';
	} elseif ($_GET['msg'] == 2) {
		$Message = "Notification added successfully";
		$alert = 'success';
	} elseif ($_GET['msg'] == 4) {
		$Message = "Notification deleted successfully";
		$alert = 'success';
	} else {
		$Message = "Something went wrong!. Please try again later.";
		$alert = 'danger';
	}
}

if(isset($_GET['notification_id']) && trim($_GET['notification_id']) != "") { 
	$updateQry = "update tbl_notification_contents set is_deleted='1' where id='".$_GET['notification_id']."'";
	$updateRes = $DBCONN->query($updateQry);
	if ($updateRes) { 
		header("Location:notification_list.php?msg=4");
		exit;
	}
}

include("includes/header.php");
?>

<script src="http://code.jquery.com/jquery-1.7.2.min.js"></script>
<script src="http://code.jquery.com/ui/1.8.21/jquery-ui.min.js"></script>
<script src="../js/jquery.ui.touch-punch.min.js" language="javascript"></script>

<style type="text/css">
	.widget.box .widget-content {
		padding:0px;
		padding-top: 10px !important;
		padding-bottom: 10px !important;
		position: relative;
		background-color: #fff;
	}
	label{
		white-space:nowrap;
	}
	@media (min-width: 980px) {
		.margn_top{

			margin-top:60px;
		}
	}
	.update-reg {
		display:none;
	}
	.dotyes {
		height: 8px;
		width: 8px;
		border-radius: 50%;
		display: inline-block;
	}
	#mngptrecds tbody tr .applyellow{
		background-color:yellow;
	}
	.truncate {
		padding: 0px !important;
		display: -webkit-box;
		-webkit-box-orient: vertical;
		-webkit-line-clamp: 2; /* Limit to two lines */
		overflow: hidden;
		text-overflow: ellipsis;
	}
	.truncate * {
		margin-bottom: 10px;
	}
</style>
		<div id="content">
			<div class="container">
				<div class="page-header">
					<div class="page-title"> </div>
				</div>
				<?php if ($Message!='') { ?>
					<div class="alert fade in alert-<?php echo $alert; ?>" style="margin-top:64px;margin-bottom:-51px">
						<i class="icon-remove close" data-dismiss="alert"></i>
						<?php echo $Message; ?>
					</div>
				<?php } ?>
				<div class="row" >
					<div class="col-md-12 margn_top">
						<div class="row">
							<div class="col-md-12">
								<div style="padding-top:5px;padding-bottom:5px;float:right;">
									<h5>Date:<?php echo date('d/m/Y');?></h5>
								</div>
							</div>
						</div>
						<div class="table-responsive">
							<div class="widget box">
								<div class="widget-header">
									<h4><i class="icon-reorder"></i>Notification List</h4>
									<input type="button" class="btn btn-primary pull-right" onclick="document.location='notification_content.php'" value="Add Notification">
								</div>
								<div class="widget-content">
									<table class="table table-striped table-bordered table-hover table-checkable">
										<thead>
											<tr>
												<th width="5%">No&nbsp;</th>
												<th width="20%">Title</th>
												<th width="20%">Subject</th>
												<th width="30%">Description</th>
												<th width="8%">Status</th>
												<th width="10%" class="align-center">Action</th>
											</tr>
										</thead>
										<tbody>
											<?php
												$getQry = "select * from tbl_notification_contents where location='".stripslashes($_SESSION['location'])."' and is_deleted= '0'";
												$getRes = $DBCONN->query($getQry);
												$getCnt = $getRes->rowCount();
											if ($getCnt > 0) {
												$i = 1;
												foreach($getRes->fetchAll(PDO::FETCH_ASSOC) as $getNotificationRow) { ?>
												<tr>
													<td><?php echo $i; ?></td>
													<td><?php echo stripslashes($getNotificationRow['title']); ?></td>
													<td><?php echo stripslashes($getNotificationRow['subject']); ?></td>
													<td class="truncate"><?php echo stripslashes($getNotificationRow['description']); ?></td>
													<td class=""><span class="label label-success"><?php echo stripslashes($getNotificationRow["status"] == 1 ? 'Active' : 'Inactive');?></span></td>
														<td class="align-center">
															<span class="btn-group">
																<a href="notification_content.php?notification_id=<?php echo stripslashes($getNotificationRow["id"]);?>"
																class="btn btn-xs bs-tooltip" title="Edit"><i class="icon-pencil"></i></a>
																<a href="push_notification.php?notification_id=<?php echo stripslashes($getNotificationRow["id"]);?>" class="btn btn-xs bs-tooltip" <?php echo stripslashes($getNotificationRow["status"] == 1 ? '' : 'disabled');?>>Send </a>
																<a href="notification_list.php?notification_id=<?php echo stripslashes($getNotificationRow["id"]);?>" class="btn btn-xs bs-tooltip" title="Delete" onclick="return confirm('Are you sure want to delete this notification?')"><i class="icon-trash"></i></a>
															</span>
														</td>
												</tr>
												<?php $i++; } ?>
											<?php } else { ?>
											<tr>
												<td colspan="7"><center>No Data.</center></td>
											</tr>
											<?php } ?>
										</tbody>
									</table>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<?php include("includes/footer.php"); ?>
