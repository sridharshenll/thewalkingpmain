<?php
include('../includes/configure.php');
include("includes/session_check.php");
$msg = $_GET['msg'] ?? '';
if($msg != "") {
	if($msg == 1) {
		$Message = "Password changed successfully";
		$class = "success";
	} else if($msg == 2) {
		$Message = "New password and confirm password doesn't match ";
		$class = "danger";
	} else if($msg == 3) {
		$Message = "Current password and database password doesn't match";
		$class = "danger";
	} else if($msg == 4) {
		$Message = "Email already exists, please try another email.";
		$class = "danger";
	} else if($msg == 5) {
		$Message = "Email changed successfully";
		$class = "success";
	} else if($msg == 6) {
		$Message = "Username already exists, please try another username.";
		$class = "danger";
	} else if($msg == 7) {
		$Message = "Username changed successfully";
		$class = "success";
	}
	
}
$sql = "select * from  tbl_staff where staff_id='".$_SESSION["ses_admin_id"]."'";
$posts = $DBCONN->query($sql);
$row = $posts->fetch(PDO::FETCH_ASSOC);
$currentpassword = trim($row["password"]);
$currentemail = trim($row["email"]);
$currentUserName = trim($row["username"]);
if(isset($_POST['oldpassword'])) {
	    $oldpasword = md5(trim($_POST['oldpassword']));
		$newpassword = md5(trim($_POST['newpassword']));
		$confirmpwd = md5(trim($_POST['confirmpassword']));
		if($oldpasword == $currentpassword && $newpassword == $confirmpwd) {
			$qry = "update tbl_staff SET  password='$newpassword',modified_date='$dbdatetime' where staff_id='".$_SESSION["ses_admin_id"]."'";
			$result = $DBCONN->query($qry);
			if($result){
				header('Location:changepassword.php?msg=1');
				exit;
			}
		} else if($newpassword != $confirmpwd){
            header('Location:changepassword.php?msg=2');
			exit;

		} else if($currentpassword != $oldpasword){
			header('Location:changepassword.php?msg=3');
			exit;
		}
		
}

if(isset($_POST['new_username'])){
	$newusername = addslashes(trim($_POST['new_username']));
	$checkmailqry = "select * from  tbl_staff where staff_id!='".$_SESSION["ses_admin_id"]."' and username='".$newusername."'";
	$checkmailres = $DBCONN->query($checkmailqry);
	$checkmailcnt = $checkmailres->rowCount();
	if($checkmailcnt>0){
		header('Location:changepassword.php?msg=6');
		exit;
	}
	else{
		$qry = "update tbl_staff SET  username='".addslashes(stripslashes(trim($newusername)))."',modified_date='".$dbdatetime."' where staff_id='".$_SESSION["ses_admin_id"]."'";
		$result = $DBCONN->query($qry);
		if($result){
			header('Location:changepassword.php?msg=7');
			exit;
		}
	}
}
if(isset($_POST['new_email'])){
	$newemail = addslashes(trim($_POST['new_email']));
	$checkmailqry = "select * from  tbl_staff where staff_id!='".$_SESSION["ses_admin_id"]."' and email='".$newemail."'";
	$checkmailres = $DBCONN->query($checkmailqry);
	$checkmailcnt = $checkmailres->rowCount();
	if($checkmailcnt>0){
		header('Location:changepassword.php?msg=4');
		exit;
	}
	else{
		$qry = "update tbl_staff SET  email='$newemail',modified_date='$dbdatetime' where staff_id='".$_SESSION["ses_admin_id"]."'";
		$result = $DBCONN->query($qry);
		if($result){
			header('Location:changepassword.php?msg=5');
			exit;
		}
	}
}
include('includes/header.php');
?>
<!-- Pickers -->
<script type="text/javascript" src="../plugins/pickadate/picker.js"></script>
<script type="text/javascript" src="../plugins/pickadate/picker.date.js"></script>
<script type="text/javascript" src="../plugins/pickadate/picker.time.js"></script>
<script type="text/javascript" src="../plugins/bootstrap-colorpicker/bootstrap-colorpicker.min.js"></script>

<script type="text/javascript" src="../plugins/fileinput/fileinput.js"></script>
<script type="text/javascript" src="../plugins/bootstrap-wysihtml5/wysihtml5.min.js"></script>
<script type="text/javascript" src="../plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.min.js"></script>

<!-- Form Validation -->
<script type="text/javascript" src="../plugins/validation/jquery.validate.min.js"></script>
<script type="text/javascript" src="../plugins/validation/additional-methods.min.js"></script>

<!-- Noty -->
<script type="text/javascript" src="../plugins/noty/jquery.noty.js"></script>
<script type="text/javascript" src="../plugins/noty/layouts/top.js"></script>
<script type="text/javascript" src="../plugins/noty/themes/default.js"></script>

<!-- Demo JS -->
<script type="text/javascript" src="../assets/js/custom.js"></script>
<script type="text/javascript" src="../assets/js/demo/form_validation.js"></script>
<script type="text/javascript" src="../assets/js/demo/ui_general.js"></script>

<div id="content">
			<div class="container">				
				<!--=== Page Header ===-->
				<div class="page-header">
					<div class="page-title">
						<h3>Change Password</h3>
						<!-- <span>Add/Edit Doctor</span> -->
					</div>
				
				</div>
				<!-- /Page Header -->
				<?php if($msg ?? FALSE) { ?>
				<div class="alert fade in alert-<?php echo $class;?>">
					<i class="icon-remove close" data-dismiss="alert"></i>
					<?php echo $Message; ?>
				</div>					
				<?php }
				?>
				<!--=== Page Content ===-->
				<!--=== Full Size Inputs ===-->
				<div class="row">
					<div class="col-md-12">
						<div class="widget box">
							<div class="widget-header">
								<h4><i class="icon-reorder"></i>Change Username</h4>
								 <div class="toolbar no-padding">
									<!-- <div class="btn-group">
										<span class="btn btn-xs widget-collapse"><i class="icon-angle-down"></i></span>
									</div> -->
								</div> 
							</div>
							<div class="widget-content">
								
						<form class="form-horizontal row-border" method='POST' id="validate-2">		
							<input type='hidden' name = "hdn_mode" value="add">
							<div class="form-group">
								<label class="col-md-3 control-label">UserName:</label>
								<!-- <div class="col-md-9"><?php echo $currentemail;?></div> -->
								<label class="col-md-3 control-label" style="text-align:left;"><?php echo $currentUserName ?? '';?></label>
							</div>

							<div class="form-group">
								<label class="col-md-3 control-label">New UserName:<span class="required">*</span></label>
								<div class="col-md-3"><input type="text" name="new_username" class="form-control required" data-msg-required="Please enter new username."></div>
							</div>
													
							<div class="form-actions">
								<input type="submit" value="Change Username" class="btn btn-primary pull-right">
							</div>
							
						</form>
					</div>
						</div>
					</div>

					<!-- <div class="row">
					<div class="col-md-12">
						<div class="widget box">
							<div class="widget-header">
								<h4><i class="icon-reorder"></i>Change Email</h4>
								 <div class="toolbar no-padding">
									<!-- <div class="btn-group">
										<span class="btn btn-xs widget-collapse"><i class="icon-angle-down"></i></span>
									</div> --
								</div> 
							</div>
							<div class="widget-content">
								
						<form class="form-horizontal row-border" method='POST' id="validate-1">		
							<input type='hidden' name = "hdn_mode" value="add">
							<div class="form-group">
								<label class="col-md-3 control-label">Email:</label>
								<label class="col-md-3 control-label" style="text-align:left;"><?php echo $currentemail;?></label>
							</div>

							<div class="form-group">
								<label class="col-md-3 control-label">New Email:<span class="required">*</span></label>
								<div class="col-md-3"><input type="text" name="new_email" class="form-control required email" data-msg-required="Please enter new email."></div>
							</div>
													
							<div class="form-actions">
								<input type="submit" value="Change Email" class="btn btn-primary pull-right">
							</div>
							
						</form>
					</div>
						</div>
					</div> -->



					<div class="col-md-12">
						<div class="widget box">
							<div class="widget-header">
								<h4><i class="icon-reorder"></i>Change Password</h4>
								 <div class="toolbar no-padding">
									<!-- <div class="btn-group">
										<span class="btn btn-xs widget-collapse"><i class="icon-angle-down"></i></span>
									</div> -->
								</div> 
							</div>
							<div class="widget-content">
								
						<form class="form-horizontal row-border" method='POST' id="validate-1">		
							<input type='hidden' name = "hdn_mode" value="add">
							
							<div class="form-group">
								<label class="col-md-3 control-label">Current Password:<span class="required">*</span></label>
								<div class="col-md-3"><input type="password" name="oldpassword" class="form-control required" data-msg-required="Please enter current password."></div>
							</div>

							<div class="form-group">
								<label class="col-md-3 control-label">New Password:<span class="required">*</span></label>
								<div class="col-md-3"><input type="password" name="newpassword" class="form-control required" data-msg-required="Please enter new password."></div>
							</div>
							<div class="form-group">
								<label class="col-md-3 control-label">Confirm Password:<span class="required">*</span></label>
								<div class="col-md-3"><input type="password" name="confirmpassword" class="form-control required" data-msg-required="Please enter confirm password."></div>
							</div>
						
							<div class="form-actions">
								<input type="submit" value="Change Password" class="btn btn-primary pull-right">
							</div>
							
						</form>
					</div>
						</div>
					</div>
				</div>				
				<!-- /Page Content -->
			</div>
			<!-- /.container -->

		</div>
		
<?php
include("includes/footer.php");
?>