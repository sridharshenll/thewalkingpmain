<?php
include('../includes/configure.php');
include("includes/session_check.php");

$Msg = $_GET['msg'] ?? '';
$Message = "";
$alert = '';
if(isset($_GET['msg']) && trim($_GET['msg']) != "") {
	if($_GET['msg'] == 1) {
		$Message = "Settings updated successfully";
		$alert = 'success';
	} else {
		$Message = "Somthing went wrong!. Please try again later.";
		$alert = 'danger';
	}
}

$getQry = "select * from tbl_settings where location='".$_SESSION["location"]."'";
$getRes = $DBCONN->query($getQry);
$getRow = $getRes->fetch(PDO::FETCH_ASSOC);
$State = stripslashes($getRow["queue_system"]);
$addQueueState = stripslashes($getRow["add_queue"]);
$addOnlyWalkinState = stripslashes($getRow["only_walkin"]);
if(!empty($_POST)){
  $state = trim($_POST['hidden']);
  $addqueuestate = trim($_POST['hidden_new']);
  $addonlywalkin = trim($_POST['hidden_only_walkin']);
  $updateQry = "update tbl_settings set only_walkin='".$addonlywalkin."',add_queue='".$addqueuestate."',queue_system='".$state."',modified_date='$dbdatetime' where location='".$_SESSION["location"]."'";
  $updateRes = $DBCONN->query($updateQry);
	if($updateRes){
		header("Location:settings.php?msg=1");
		exit;
	} else {
		header("Location:settings.php?msg=2");
	}
}
include('includes/header.php');
?>
<!-- Form Validation -->
<script type="text/javascript" src="../plugins/validation/jquery.validate.min.js"></script>
<script type="text/javascript" src="../plugins/validation/additional-methods.min.js"></script>
<!-- Smartphone Touch Events -->
<script type="text/javascript" src="../plugins/touchpunch/jquery.ui.touch-punch.min.js"></script>
<script type="text/javascript" src="../plugins/event.swipe/jquery.event.move.js"></script>
<script type="text/javascript" src="../plugins/event.swipe/jquery.event.swipe.js"></script>

<!-- General -->
<script type="text/javascript" src="../assets/js/libs/breakpoints.js"></script>
<script type="text/javascript" src="../plugins/respond/respond.min.js"></script> <!-- Polyfill for min/max-width CSS3 Media Queries (only for IE8) -->
<script type="text/javascript" src="../plugins/cookie/jquery.cookie.min.js"></script>
<script type="text/javascript" src="../plugins/slimscroll/jquery.slimscroll.min.js"></script>
<script type="text/javascript" src="../plugins/slimscroll/jquery.slimscroll.horizontal.min.js"></script>

<!-- Page specific plugins -->
<!-- Charts -->
<script type="text/javascript" src="../plugins/sparkline/jquery.sparkline.min.js"></script>

<script type="text/javascript" src="../plugins/daterangepicker/moment.min.js"></script>
<script type="text/javascript" src="../plugins/daterangepicker/daterangepicker.js"></script>
<script type="text/javascript" src="../plugins/blockui/jquery.blockUI.min.js"></script>

<!-- Forms -->
<script type="text/javascript" src="../plugins/typeahead/typeahead.min.js"></script> <!-- AutoComplete -->
<script type="text/javascript" src="../plugins/autosize/jquery.autosize.min.js"></script>
<script type="text/javascript" src="../plugins/inputlimiter/jquery.inputlimiter.min.js"></script>
<script type="text/javascript" src="../plugins/uniform/jquery.uniform.min.js"></script> <!-- Styled radio and checkboxes -->
<script type="text/javascript" src="../plugins/tagsinput/jquery.tagsinput.min.js"></script>
<script type="text/javascript" src="../plugins/select2/select2.min.js"></script> <!-- Styled select boxes -->
<script type="text/javascript" src="../plugins/fileinput/fileinput.js"></script>
<script type="text/javascript" src="../plugins/duallistbox/jquery.duallistbox.min.js"></script>
<script type="text/javascript" src="../plugins/bootstrap-inputmask/jquery.inputmask.min.js"></script>
<script type="text/javascript" src="../plugins/bootstrap-wysihtml5/wysihtml5.min.js"></script>
<script type="text/javascript" src="../plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.min.js"></script>
<script type="text/javascript" src="../plugins/bootstrap-multiselect/bootstrap-multiselect.min.js"></script>
<script type="text/javascript" src="../plugins/bootstrap-switch/bootstrap-switch.js"></script>
<!-- Globalize -->
<script type="text/javascript" src="../plugins/globalize/globalize.js"></script>
<script type="text/javascript" src="../plugins/globalize/cultures/globalize.culture.de-DE.js"></script>
<script type="text/javascript" src="../plugins/globalize/cultures/globalize.culture.ja-JP.js"></script>
<div id="content">
			<div class="container">				
				<!--=== Page Header ===-->
				<div class="page-header">
					<div class="page-title">
						<h3>Change Settings</h3>
					</div>
				
				</div>
				<!-- /Page Header -->
				<?php if( !empty($Message)) { ?>
					<div class="alert fade in alert-<?php echo $alert; ?>">
						<i class="icon-remove close" data-dismiss="alert"></i>
						<?php echo $Message; ?>
					</div>					
				<?php } ?>
				<!--=== Page Content ===-->
				<!--=== Full Size Inputs ===-->
				<div class="row">
					<div class="col-md-12">
						<div class="widget box">
							<div class="widget-header">
								<h4><i class="icon-reorder"></i>Queue System Settings</h4>
								 <div class="toolbar no-padding">
									
								</div> 
							</div>
							<div class="widget-content">
							<form class="form-horizontal row-border" method="post" name="edit_form" id="edit_form">
								<div class="form-group">
									<label class="col-md-4 control-label">Queue System</label>
									<div class="col-md-8">
										<div>
											<div class="make-switch switch-large">
												<input type="checkbox" id="firstSwitch" <?php echo isset($State) && $State == 0 ? 'checked' : ''; ?>>
											</div>
											<input type="hidden" id="hidden" name="hidden" value="<?php echo $State?>">
										</div>
									</div>
								</div>
								<div class="form-group">
									<label class="col-md-4 control-label">Queue System Button(Show/hide)</label>
									<div class="col-md-8">
										<div>
											<div class="make-switch switch-large" >
												<input type="checkbox" id="secondSwitch" <?php echo isset($addQueueState) && $addQueueState == 0 ? 'checked' : ''; ?>>
											</div>
											<input type="hidden" id="hidden_new" name="hidden_new" value="<?php echo $addQueueState?>">
										</div>
									</div>
								</div>
								<div class="form-group">
									<label class="col-md-4 control-label">Only Walkin Queue System</label>
									<div class="col-md-8">
										<div>
											<div class="make-switch switch-large" >
												<input type="checkbox" id="onlyWalkinSwitch" <?php echo isset($addOnlyWalkinState) && $addOnlyWalkinState == 0 ? 'checked' : ''; ?>>
											</div>
											<input type="hidden" id="hidden_only_walkin" name="hidden_only_walkin" value="<?php echo $addOnlyWalkinState?>">
										</div>
									</div>
								</div>
							</form>
							
					</div>
						</div>
					</div>
				</div>				
				<!-- /Page Content -->
			</div>
			<!-- /.container -->

		</div>
		
<?php
include("includes/footer.php");
?>
