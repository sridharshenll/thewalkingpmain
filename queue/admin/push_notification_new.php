<?php
require '../third_party/vendor/autoload.php';
include("../includes/configure.php");

use Kreait\Firebase\Factory;
use Kreait\Firebase\Messaging\CloudMessage;
use Kreait\Firebase\Messaging\Notification;
use Kreait\Firebase\Exception\Messaging\InvalidMessage;
use Kreait\Firebase\Exception\MessagingException;
use Kreait\Firebase\Exception\Messaging\NotFound;

// Function to log notification status
function logNotificationStatus($patient, $status, $subject, $contents, $title, $notification_id, $response = '') {
    global $DBCONN;

    $data = array();
    $data['status'] = $status;
    $data['message'] = !empty($response) ? json_encode($response) : '';
    $data['patient_id'] = $patient['patient_id'] ?? '';
    $data['patient_name'] = $patient['patient_name'] ?? '';
    $data['patient_email_address'] = $patient['email_address'] ?? '';
    $data['patient_mobile_phone'] = $patient['mobile_phone'] ?? '';
    $data['fcm_token'] = $patient['fcm_token'] ?? '';
    $data['device_type'] = $patient['device_type'] ?? '';
    $data['notification_id'] = $notification_id ?? '';
    $data['title'] = $title ?? '';
    $data['subject'] = $subject;
    $data['contents'] = $contents;
    $data['date'] = date("d-m-Y");

    $responseArr = array();
    $notification_log = $DBCONN->prepare("INSERT INTO tbl_notification_log (patient_id, patient_name, family_name, email_address, mobile_phone, location, response, status, fcm_token, notification_content_id, notification_title, subject, description, created_date) VALUES (:patient_id, :patient_name, :family_name, :email_address, :mobile_phone, :location, :response, :status, :fcm_token, :notification_content_id, :notification_title, :subject, :description, :created_date)");
    date_default_timezone_set('Etc/GMT-8');
    $gmtdatetime = date("Y-m-d H:i:s");
    $notification_log->execute([
        ':patient_id' => $patient['patient_id'] ?? '',
        ':patient_name' => $patient['patient_name'] ?? '',
        ':family_name' => $patient['family_name'] ?? '',
        ':email_address' => $patient['email_address'] ?? '',
        ':mobile_phone' => $patient['mobile_phone'] ?? '',
        ':location' => $patient['location'] ?? '',
        ':response' => json_encode($response),
        ':status' => strtolower($status),
        ':fcm_token' => $patient['fcm_token'] ?? '',
        ':notification_content_id' => $notification_id ?? '',
        ':notification_title' => $title ?? '',
        ':subject' => $subject,
        ':description' => $contents,
        ':created_date' => $gmtdatetime,
    ]);

    if($DBCONN->lastInsertId()) {
        $data['log_id'] = $DBCONN->lastInsertId();
    }
    $strDirname = "notificationLog";

    // Create directory if it doesn't exist
    if (!is_dir($strDirname)) {
        mkdir($strDirname, 0777, true);
    }
    $fp = fopen($strDirname.'/walking_push_notification_'.date("d-m-Y").'.txt', 'a');
	fwrite($fp, json_encode($data). "\n");
	fclose($fp);
}

if(isset($_GET['notification_id'])) {
    // Get Notification Subject and Contents
    $getPushMessageQuery = $DBCONN->query("SELECT * FROM `tbl_notification_contents` where notification_id = '".$_GET['notification_id']."'");
    $getPushMessageResult = $getPushMessageQuery->fetch(PDO::FETCH_ASSOC);
    $getPushMessageTitle = $getPushMessageResult['title'];
    $getPushMessageSubject = $getPushMessageResult['subject'];
    $getPushMessageContent = $getPushMessageResult['description'];

    // Get Device List with FCM Token
    // $getPatientsList = $DBCONN->query("SELECT * FROM tbl_patient WHERE fcm_token IS NOT NULL AND fcm_token != ''");
    $getPatientsList = $DBCONN->query("SELECT DISTINCT (fcm_token) FROM tbl_patient WHERE fcm_token IS NOT NULL AND fcm_token != ''");
    $getPatientCnt = $getPatientsList->rowCount();

    if($getPatientCnt > 0) {
        // Initialize Firebase Admin SDK
        $factory = (new Factory)->withServiceAccount('walkingp-6eaf3-firebase-adminsdk-9qfy7-5d7379d2e2.json');
        $messaging  = $factory->createMessaging(NOTIFICATION_TOKEN);

        // Construct a notification payload
        $notification = Notification::create($getPushMessageTitle, $getPushMessageSubject);

        foreach($getPatientsList->fetchAll(PDO::FETCH_ASSOC) as $patientList) {
            try { 
                $deviceToken = $patientList['fcm_token'];
                $protocol = (!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on') ? "https://" : "http://";
                $host = $_SERVER['HTTP_HOST'];
                $plainText = strip_tags($getPushMessageContent);
                $getNotificationQry = "select * from tbl_notification_contents where notification_id = '".$_GET["notification_id"]."'";
                $getNotificationRes = $DBCONN->query($getNotificationQry);
                $notificationData = $getNotificationRes->fetch(PDO::FETCH_ASSOC);

                $dataArr = array(
                    'title' => $getPushMessageTitle,
                    'subject' => $getPushMessageSubject,
                    'body' => htmlspecialchars($plainText, ENT_QUOTES, 'UTF-8'),
                    'notification_url' => stripslashes($notificationData["description"]),
                );
                // Construct a message targeting a specific device or topic
                $message = CloudMessage::withTarget('token', $deviceToken)
                ->withNotification($notification)->withData($dataArr);

                // Send the message
                $response = $messaging->send($message);
                logNotificationStatus($patientList, 'SUCCESS', $getPushMessageSubject, $getPushMessageContent, $getPushMessageTitle, $_GET["notification_id"], $response);
            } catch (InvalidMessage | MessagingException $e) {
                logNotificationStatus($patientList, 'FAILED', $getPushMessageSubject, $getPushMessageContent, $getPushMessageTitle, $_GET["notification_id"], $e->errors());
            } catch (Throwable $e) {
                logNotificationStatus($patientList, 'FAILED', $getPushMessageSubject, $getPushMessageContent, $getPushMessageTitle, $_GET["notification_id"], $e->errors());
            }
        }
    } else {
        $error['error']['code'] = '404';
        $error['error']['message'] = 'No FCM Token found for '.$getPushMessageResult['location'];
        logNotificationStatus('', 'FAILED', $getPushMessageSubject, $getPushMessageContent, $getPushMessageTitle, $_GET["notification_id"], $error);
    }
    header("Location:notification_list.php?msg=1");
	exit;
}
?>