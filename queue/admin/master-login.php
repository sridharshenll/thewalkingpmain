<?php
$ErrorMessage = "";
require_once('../includes/configure.php');
if(isset($_POST['username']) && $_POST['username'] != ''){
	$UserName=trim($_POST['username']);
	$Location=trim($_POST['location']);
	$_SESSION["location"]=$Location;

	$GetAdminQry="select * from tbl_staff where masterkey='".addslashes(stripslashes(trim($UserName)))."' and location='".$_SESSION["location"]."' ";
	$GetAdminRes=$DBCONN->query($GetAdminQry);
	$GetAdminCnt=$GetAdminRes->rowCount();
	if($GetAdminCnt>0){
		$GetAdminRow=$GetAdminRes->fetch(PDO::FETCH_ASSOC);
		$AdminId=$GetAdminRow['staff_id'];
		$_SESSION["ses_admin_id"]=$AdminId;
	    $_SESSION["ses_usr_name"]=stripslashes($GetAdminRow['username']);
		 $_SESSION["ses_usr_email"]=stripslashes($GetAdminRow['email']);
		$_SESSION["role"]=stripslashes($GetAdminRow['role']);
		$_SESSION["location"]=stripslashes($GetAdminRow['location']);
		if($_SESSION["role"]=="admin"){
			header("Location:patient_queue_screen.php");
			exit;
		}
		else if($_SESSION["role"]=="doctor"){
            header("Location:patient_queue_screen.php");
			exit;
		}
	}
	else
	{
		$ErrorMessage = "Invalid master key for the selected location";	
    }
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0" />
	<link rel="icon" type="image/ico" href="http://thewalkingp.com.au/favicon.png">
	<title></title>
    <!--=== CSS ===-->

	<!-- Bootstrap -->
	<link href="../bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />

	<!-- Theme -->
	<link href="../assets/css/main.css" rel="stylesheet" type="text/css" />
	<link href="../assets/css/plugins.css" rel="stylesheet" type="text/css" />
	<link href="../assets/css/responsive.css" rel="stylesheet" type="text/css" />
	<link href="../assets/css/icons.css" rel="stylesheet" type="text/css" />

	<!-- Login -->
	<link href="../assets/css/login.css" rel="stylesheet" type="text/css" />

	<link rel="stylesheet" href="../assets/css/fontawesome/font-awesome.min.css">
	<!--[if IE 7]>
		<link rel="stylesheet" href="assets/css/fontawesome/font-awesome-ie7.min.css">
	<![endif]-->

	<!--[if IE 8]>
		<link href="assets/css/ie8.css" rel="stylesheet" type="text/css" />
	<![endif]-->
	<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,600,700' rel='stylesheet' type='text/css'>

	<!--=== JavaScript ===-->

	<script type="text/javascript" src="../assets/js/libs/jquery-1.10.2.min.js"></script>

	<script type="text/javascript" src="../bootstrap/js/bootstrap.min.js"></script>
	<script type="text/javascript" src="../assets/js/libs/lodash.compat.min.js"></script>

	<!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
	<!--[if lt IE 9]>
		<script src="assets/js/libs/html5shiv.js"></script>
	<![endif]-->

	

	<!-- Form Validation -->
	<script type="text/javascript" src="../plugins/validation/jquery.validate.min.js"></script>
	<!-- Noty -->
<script type="text/javascript" src="../plugins/noty/jquery.noty.js"></script>
<script type="text/javascript" src="../plugins/noty/layouts/top.js"></script>
<script type="text/javascript" src="../plugins/noty/themes/default.js"></script>
	<!-- Slim Progress Bars -->
	<script type="text/javascript" src="../plugins/nprogress/nprogress.js"></script>
	<script type="text/javascript" src="../assets/js/demo/form_validation.js"></script>
	<!-- App -->
	<!-- <script type="text/javascript" src="../assets/js/login.js"></script> -->
	<!-- <script>
	$(document).ready(function(){
		"use strict";

		Login.init(); // Init login JavaScript
	});
	</script> -->
	<style type="text/css">
		/*select.form-control,.form-control{
		padding-left:12px !important;
		padding-right:0px !important;
		padding-top:5px;
		padding-bottom:5px;

	}*/
	select.form-control, textarea.form-control {
		padding:5px 4px !important;
	}
	</style>
</head>

<body class="login">
	<!-- Logo -->
	<div class="logo">
		<img src="../assets/img/logo.png" alt="logo" />		
	</div>
	<!-- /Logo -->

	<!-- Login Box -->
	<div class="box">
		<div class="content">
			<!-- Login Formular -->
			<form class="form-vertical login-form" action="" method="post" id="login-form">
				<!-- Title -->
				<h3 class="form-title">Sign In to your Account</h3>

				<!-- Error Message -->
				<div class="alert fade in alert-danger" style="display: none;">
					<i class="icon-remove close" data-dismiss="alert"></i>
					Enter username and password.
				</div>
				<?php
				if($ErrorMessage!='')
				{
				?>
				<div class="alert fade in alert-danger">
					<i class="icon-remove close" data-dismiss="alert"></i>
					<?php echo $ErrorMessage; ?>
				</div>					
				<?php
				}
				?>

					
				<div class="form-group">
					
					<div class="input-icon">
						
						<select class="form-control required" id="culture" name="location" class="form-control" placeholder="Select  Location" autofocus="autofocus" data-rule-required="true" data-msg-required="Please Select your location." />
												<option value="">Select Location</option>
												<option value="Burswood">Burswood</option>
												<option value="Claremont">Claremont</option>
												<option value="Telehealth">Telehealth</option>
											</select> 
					</div>
				</div>
				<!-- Input Fields -->
				<div class="form-group">
					<!--<label for="username">Username:</label>-->
					<div class="input-icon">
						<i class="icon-user"></i>
						<input type="text" name="username" class="form-control" placeholder="Master Key" autofocus="autofocus" data-rule-required="true" data-msg-required="Please enter your master key." autocomplete="off"/>
					</div>
				</div>				
				<!-- /Input Fields -->

				<!-- Form Actions -->
				<div class="form-actions">
					    <button type="submit" class="submit btn btn-primary pull-right">
						Sign In <i class="icon-angle-right"></i>
					</button>
				</div>
			</form>
			<!-- /Login Formular -->

		 </div> <!-- /.content -->

	</div>
	<!-- /Login Box -->

	
</body>
</html>