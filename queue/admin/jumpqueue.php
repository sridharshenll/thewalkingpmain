<?php

include('../includes/configure.php');
include("includes/session_check.php");

$check_location = $DBCONN->query("select * from tbl_settings where location='".$_SESSION["location"]."' and queue_system='1'");
$loca_num = $check_location->rowCount();
if(isset($_POST["preffered_doctor"])) {

	$family_name = addslashes(trim($_POST["family_name"]));
	$given_names = addslashes(trim($_POST["given_names"]));
	$email_address = addslashes(trim($_POST["email_address"]));
	$next_of_kin = addslashes(trim($_POST["next_of_kin"]));
	$next_of_kin_contact_number = addslashes(trim($_POST["next_of_kin_contact_number"]));
	$preffered_doctor =	$_POST["preffered_doctor"];
	$patient_dob = "";
	if($_POST["birth_month"] != "" && $_POST["birth_date"] != "" && $_POST["birth_year"] != "") {
		$patient_dob = $_POST["birth_year"]."-".$_POST["birth_month"]."-".$_POST["birth_date"];
	}

	$getPosition = "select max(display_order) as displayrow from tbl_patient where location='".$_SESSION["location"]."'";
	$getPosRes = $DBCONN->query($getPosition);
	$getDisplayRow = $getPosRes->fetch(PDO::FETCH_ASSOC);
	$DisplayRow = $getDisplayRow["displayrow"] + 1;

	$insertQry = "insert into tbl_patient(doctor_id,patient_name,family_name,dob,location,patient_status,register_date,reg_time,created_date,modified_date,display_order,email_address,next_of_kin,next_of_kin_contact_number) values('".$preffered_doctor."','".ucwords($given_names)."','".ucwords($family_name)."','".$patient_dob."','".$_SESSION["location"]."','Appointment fixed','".date('Y-m-d')."','".date('Y-m-d H:i:s')."','".date('Y-m-d H:i:s')."','".date('Y-m-d H:i:s')."','".$DisplayRow."','".$email_address."','".$next_of_kin."','".$next_of_kin_contact_number."')";
	$insertRes = $DBCONN->query($insertQry);
	if($insertRes){
		header("Location:patient_queue_screen.php?msg=1");
		exit;
	}
}
include('includes/header.php');
?>

<!-- Pickers -->

<script type="text/javascript" src="../plugins/pickadate/picker.js"></script>
<script type="text/javascript" src="../plugins/pickadate/picker.date.js"></script>
<script type="text/javascript" src="../plugins/pickadate/picker.time.js"></script>
<script type="text/javascript" src="../plugins/bootstrap-colorpicker/bootstrap-colorpicker.min.js"></script>
<script type="text/javascript" src="../plugins/fileinput/fileinput.js"></script>
<script type="text/javascript" src="../plugins/bootstrap-wysihtml5/wysihtml5.min.js"></script>
<script type="text/javascript" src="../plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.min.js"></script>
<!-- Form Validation -->
<script type="text/javascript" src="../plugins/validation/jquery.validate.min.js"></script>
<script type="text/javascript" src="../plugins/validation/additional-methods.min.js"></script>
<!-- Noty -->
<script type="text/javascript" src="../plugins/noty/jquery.noty.js"></script>
<script type="text/javascript" src="../plugins/noty/layouts/top.js"></script>
<script type="text/javascript" src="../plugins/noty/themes/default.js"></script>
<!-- Demo JS -->
<script type="text/javascript" src="../assets/js/custom.js"></script>
<script type="text/javascript" src="../assets/js/demo/form_validation.js"></script>
<script type="text/javascript" src="../assets/js/demo/ui_general.js"></script>
<div id="content">
	<div class="container">				
		<!--=== Page Header ===-->
		<div class="page-header" style="margin-top:40px;">
			<div class="page-title">
			</div>					
		</div>
		<!-- /Page Header -->
		<?php if($msg ?? FALSE) { ?>
			<div class="alert fade in alert-success">
				<i class="icon-remove close" data-dismiss="alert"></i>
				<?php echo $Message; ?>
			</div>
		<?php } ?>
		<div class="row">
			<div class="col-md-12">
				<div class="widget box">
					<div class="widget-header">
						<h4><i class="icon-reorder"></i>Patient Registration Form</h4>
					</div>
					<div class="widget-content">
						<form class="form-horizontal row-border" method='POST' id="validate-1" action="">									
							<div class="form-group">
								<label class="control-label col-md-3">Doctor: <span class="required">*</span></label>
								<div class="col-md-4">
									<select name="preffered_doctor" id="preffered_doctor" class="select2 full-width-fix required" data-msg-required="Please select prefered doctor." >
										<option value="0">First available doctor</option>
										<?php
											$doctQuery = "select * from tbl_staff where location='".$_SESSION["location"]."' and staff_name!=''";
										$doctResult = $DBCONN->query($doctQuery);
										foreach($doctResult->fetchAll(PDO::FETCH_ASSOC) as $docRows) {
											$doctorId = stripslashes($docRows['staff_id']);
											$doctorName = stripslashes($docRows['staff_name']); ?>
											<option value="<?php echo $doctorId ?? ''; ?>"><?php echo $doctorName ?? '';?></option>
										<?php } ?>
									</select>
								</div>
							</div>
							<div class="form-group">
								<label class="control-label col-md-3">Family Name: <span class="required">*</span></label>
								<div class="col-md-4">
									<input type="text" class="form-control required letterswithbasicpunc" name="family_name" id="family_name" value="" data-msg-required="Please enter family name.">
									<span class="help-block">(as printed on your medicare card).</span>
								</div>
							</div>
							<div class="form-group">
								<label class="control-label col-md-3">Given Name: <span class="required">*</span></label>
								<div class="col-md-4">
									<input type="text" class="form-control required letterswithbasicpunc" name="given_names" id="given_names" value="" data-msg-required="Please enter given name.">
								<span class="help-block">(as printed on your medicare card).</span>
								</div>
							</div>
							<div class="form-group" style="display:none;">
								<label class="control-label col-md-3">Email Address: <span class="required">*</span></label>
								<div class="col-md-4">
									<input type="email" class="form-control" name="email_address" id="email_address" value="" data-msg-required="Please enter an email address.">
								</div>
							</div>
							<div class="form-group">
								<label class="control-label col-md-3">Date of Birth: <span class="required">*</span></label>
								<div class="col-md-4" style='padding-left:0px'>
										<div class="col-md-4" style='padding-bottom:5px'>
											<select name="birth_date" class="form-control required full-width-fix" id="birth_date" class="" data-msg-required="Please select date.">
												<option value="">Date</option>
												<?php
													for($d = 1; $d <= 31; $d++) {
														echo "<option value='".$d."'>".$d."</option>";
													}
												?>
											</select>
										</div>
											<div class="col-md-4"  style='padding-bottom:5px'>
											<select name="birth_month" id="birth_month" class="form-control required full-width-fix " data-msg-required="Please select month.">	<option value="">Month</option>
												<?php
													for($i = 1; $i <= 12; $i++) {
														echo "<option value='".$i."'>".$i."</option>";
													}
												?>
											</select>
										</div>
										<div class="col-md-4"  style='padding-bottom:5px'>
											<select name="birth_year" id="birth_year" class="form-control required full-width-fix" data-msg-required="Please select year.">
												<option value="">Year</option>
												<?php
													for($y = 1900; $y <= 2030; $y++) {
														echo "<option value='".$y."'>".$y."</option>";
													}
												?>
											</select>
										</div>
								</div>
							</div>
							<div class="form-group" style="display:none;">
								<label class="control-label col-md-3">Next of Kin: </label>
								<div class="col-md-4">
									<input type="text" class="form-control" name="next_of_kin" id="next_of_kin" value="">
								</div>
							</div>
							<div class="form-group" style="display:none;">
								<label class="control-label col-md-3">Next of Kin Contact Number: <span class="required">*</span></label>
								<div class="col-md-4">
									<input type="text" class="form-control" name="next_of_kin_contact_number" id="next_of_kin_contact_number" value="" data-msg-required="Please enter valid next of kin contact number." style="text-decoration:none">
								</div>
							</div>
							<div class="form-actions">
							  <input type="button" class="btn btn-primary pull-right" onclick="document.location='patient_queue_screen.php'" value="Cancel">
								<input type="submit" value="Submit" class="btn btn-primary pull-right">
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>				
		<!-- /Page Content -->
	</div>
</div>
<?php include("includes/footer.php"); ?>