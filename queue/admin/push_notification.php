<?php

require '../third_party/vendor/autoload.php';
include("../includes/configure.php");

use Kreait\Firebase\Factory;
use Kreait\Firebase\Messaging\CloudMessage;
use Kreait\Firebase\Messaging\Notification;
use Kreait\Firebase\Exception\Messaging\InvalidMessage;
use Kreait\Firebase\Exception\MessagingException;
use Kreait\Firebase\Exception\Messaging\NotFound;
use Kreait\Firebase\Messaging\ApnsConfig;

// Function to log notification status
function logNotificationStatus($result) {
    global $DBCONN;

    if (!empty($result) && $result->status != 'No Data') {
        $responseArr = array();

        $patient_id = "";
        $getPatient = $DBCONN->query("SELECT * FROM `tbl_patient` where device_tbl_id = ".$result->device['id']." Order by patient_id desc");
        $patientDetail = $getPatient->fetch(PDO::FETCH_ASSOC);
        if (!empty($patientDetail)) {
           $patient_id =  $patientDetail["id"];
           $result->patient = json_encode($patientDetail);
        }

        $notification_log = $DBCONN->prepare("INSERT INTO tbl_notification_log (patient_id, response, status, fcm_token, notification_id, notification_title, subject, description) VALUES (:patient_id, :response, :status, :fcm_token, :notification_id, :notification_title, :subject, :description)");
        $notification_log->execute([
            ':patient_id' => $patient_id ?? '',
            ':response' => $result->response,
            ':status' => strtolower($result->status),
            ':fcm_token' => $result->device['fcm_token'] ?? '',
            ':notification_id' => $result->notification['id'] ?? '',
            ':notification_title' => $result->notification['title'] ?? '',
            ':subject' => $result->notification['subject'],
            ':description' => $result->notification['description'],
        ]);

        if($DBCONN->lastInsertId()) {
            $result->log_id = $DBCONN->lastInsertId();
        }
        $strDirname = "./logs";
        if (!is_dir($strDirname)) {
            mkdir($strDirname, 0777, true);
        }
        $fp = fopen($strDirname.'/walking-gp-'.date("Y-m-d").'.log', 'a');
    	fwrite($fp, "[".date('Y-m-d H:m:s')."] === Patient id : ". $patient_id . ", FCM Token : === ".$result->device['fcm_token']."=== \n".json_encode($result). "\n\n");
    	fclose($fp);
    }
}

if(isset($_GET['notification_id'])) {

    $getPushMessageQuery = $DBCONN->query("SELECT * FROM `tbl_notification_contents` where id = '".$_GET['notification_id']."'");
    $notificationResult = $getPushMessageQuery->fetch(PDO::FETCH_ASSOC);

    // Get Device List with FCM Token
    $getDeviceList = $DBCONN->query("SELECT * FROM tbl_device_details WHERE fcm_token IS NOT NULL AND fcm_token != '' GROUP BY fcm_token order BY id desc");
    $getDeviceCnt = $getDeviceList->rowCount();

    $result = new stdClass();
    if($getDeviceCnt > 0) {

        $factory = (new Factory)->withServiceAccount('./includes/walkingp-6eaf3-firebase-adminsdk-9qfy7-5d7379d2e2.json');
        $messaging  = $factory->createMessaging(NOTIFICATION_TOKEN);

        // Construct a notification payload
        $notification = Notification::create($notificationResult['title'], $notificationResult['subject']);
        foreach($getDeviceList->fetchAll(PDO::FETCH_ASSOC) as $deviceinfo) {
            if (validateFCMToken($deviceinfo['fcm_token'])) {
                try { 
                    $plainText = strip_tags($notificationResult['description']);
                    $dataArr = array(
                        'title' => $notificationResult['title'],
                        'subject' => $notificationResult['subject'],
                        'body' => htmlspecialchars($plainText, ENT_QUOTES, 'UTF-8'),
                        'notification_url' => stripslashes($notificationResult["description"]),
                    );

                    $androidConfig = [
                        'priority' => 'high',
                        'notification' => [
                            'sound' => 'default',
                        ]
                    ];

                    if ($deviceinfo['device_type'] == 'iOS') {
                       $message = CloudMessage::withTarget('token', $deviceinfo['fcm_token'])
                        ->withNotification($notification)->withData($dataArr)
                        ->withApnsConfig(
                            ApnsConfig::new()
                            ->withImmediatePriority()
                            ->withSound('default')
                            ->withBadge(1)
                        ); 
                    } else {
                        $message = CloudMessage::withTarget('token', $deviceinfo['fcm_token'])
                        ->withNotification($notification)->withData($dataArr)
                        ->withAndroidConfig($androidConfig);
                    }

                    $result->device = $deviceinfo;
                    $result->notification = $notificationResult;
                    $result->status = 'Success';

                    // Send the message
                    $response = $messaging->send($message);
                    $result->response = json_encode($response);
                } catch (InvalidMessage | MessagingException $e) {
                    $result->response = json_encode($e->errors());
                    $result->status = 'Failed';
                } catch (Throwable $e) {
                    $result->response = json_encode($e->errors());
                    $result->status = 'Failed';
                }
                logNotificationStatus($result);
            }
        }
    } else {
        $result->response = "No FCM Token found for". $notificationResult['location'];
        $result->status = "No Data";
        logNotificationStatus($result);
    }
    header("Location:notification_list.php?msg=1");
	exit;
}

function validateFCMToken($token) {
    $valid = false;
    $firebase = (new Factory)
    ->withServiceAccount('walkingp-6eaf3-firebase-adminsdk-9qfy7-5d7379d2e2.json');
    $messaging = $firebase->createMessaging();
    $message = CloudMessage::withTarget('token', $token);
    try {
        $messaging->send($message, true); // 'true' for dry run
        $valid = true;
    } catch (\Kreait\Firebase\Exception\Messaging\NotFound $e) {
        logFile($token, json_encode($e->getMessage()));
    } catch (\Kreait\Firebase\Exception\Messaging\InvalidMessage $e) {
        logFile($token, json_encode($e->getMessage()));
    } catch (\Kreait\Firebase\Exception\Messaging\MessagingError $e) {
        logFile($token, json_encode($e->getMessage()));
    } catch (\Throwable $e) {
        logFile($token, json_encode($e->getMessage()));
    }
    return $valid;
}

function logFile($token, $message) {
    $strDirname = "./logs";
    if (!is_dir($strDirname)) {
        mkdir($strDirname, 0777, true);
    }
    $fp = fopen($strDirname.'/walking-gp-'.date("Y-m-d").'.log', 'a');
    fwrite($fp, "[".date('Y-m-d H:m:s')."] FCM Token : === ".$token."=== \n".$message. "\n\n");
    fclose($fp);
}
?>