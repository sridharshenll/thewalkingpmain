<?php
$file_name=basename($_SERVER["PHP_SELF"]);
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0" />
	<?php
		if($file_name=="patient_queue_screen.php" || $file_name=="manage_patients.php"){
	?>
	<meta http-equiv="refresh" content="60"/>
	<?php
	}
	?>
	<title>Clinic Management System</title>

	<!--=== CSS ===-->
	<link rel="icon" type="image/ico" href="http://thewalkingp.com.au/favicon.png">
	<!-- Bootstrap -->
	<link href="../bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />

	<!-- jQuery UI -->
	<!--<link href="../plugins/jquery-ui/jquery-ui-1.10.2.custom.css" rel="stylesheet" type="text/css" />-->
	<!--[if lt IE 9]>
		<link rel="stylesheet" type="text/css" href="../plugins/jquery-ui/jquery.ui.1.10.2.ie.css"/>
	<![endif]-->

	<!-- Theme -->
	<link href="../assets/css/main.css" rel="stylesheet" type="text/css" />
	<link href="../assets/css/plugins.css" rel="stylesheet" type="text/css" />
	<link href="../assets/css/responsive.css" rel="stylesheet" type="text/css" />
	<link href="../assets/css/icons.css" rel="stylesheet" type="text/css" />

	<link rel="stylesheet" href="../assets/css/fontawesome/font-awesome.min.css">
	<!--[if IE 7]>
		<link rel="stylesheet" href="../assets/css/fontawesome/font-awesome-ie7.min.css">
	<![endif]-->

	<!--[if IE 8]>
		<link href="../assets/css/ie8.css" rel="stylesheet" type="text/css" />
	<![endif]-->
	<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,600,700' rel='stylesheet' type='text/css'>

	<!--=== JavaScript ===-->

	<script type="text/javascript" src="../assets/js/libs/jquery-1.10.2.min.js"></script>
	<script type="text/javascript" src="../plugins/jquery-ui/jquery-ui-1.10.2.custom.min.js"></script>

	<script type="text/javascript" src="../bootstrap/js/bootstrap.min.js"></script>
	<script type="text/javascript" src="../assets/js/libs/lodash.compat.min.js"></script>
		<script type="text/javascript" src="../plugins/bootstrap-inputmask/jquery.inputmask.js"></script>

	<!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
	<!--[if lt IE 9]>
		<script src="assets/js/libs/html5shiv.js"></script>
	<![endif]-->

	<!-- Smartphone Touch Events -->
	<script type="text/javascript" src="../plugins/touchpunch/jquery.ui.touch-punch.min.js"></script>
	<script type="text/javascript" src="../plugins/event.swipe/jquery.event.move.js"></script>
	<script type="text/javascript" src="../plugins/event.swipe/jquery.event.swipe.js"></script>

	<!-- General -->
	<script type="text/javascript" src="../assets/js/libs/breakpoints.js"></script>
	<script type="text/javascript" src="../plugins/respond/respond.min.js"></script> <!-- Polyfill for min/max-width CSS3 Media Queries (only for IE8) -->
	<script type="text/javascript" src="../plugins/cookie/jquery.cookie.min.js"></script>
	<script type="text/javascript" src="../plugins/slimscroll/jquery.slimscroll.min.js"></script>
	<script type="text/javascript" src="../plugins/slimscroll/jquery.slimscroll.horizontal.min.js"></script>

	<!-- Page specific plugins -->
	<!-- Charts -->
	<script type="text/javascript" src="../plugins/sparkline/jquery.sparkline.min.js"></script>

	<script type="text/javascript" src="../plugins/daterangepicker/moment.min.js"></script>
	<script type="text/javascript" src="../plugins/daterangepicker/daterangepicker.js"></script>
	<script type="text/javascript" src="../plugins/blockui/jquery.blockUI.min.js"></script>

	<!-- Forms -->
	<script type="text/javascript" src="../plugins/uniform/jquery.uniform.min.js"></script> <!-- Styled radio and checkboxes -->
	<script type="text/javascript" src="../plugins/select2/select2.min.js"></script> <!-- Styled select boxes -->


	<!-- App -->
	<script type="text/javascript" src="js/app.js"></script>
	<script type="text/javascript" src="../assets/js/plugins.js"></script>
	<script type="text/javascript" src="../assets/js/plugins.form-components.js"></script>
	<script type="text/javascript" src="../plugins/datatables/jquery.dataTables.js"></script>
	<script type="text/javascript" src="../plugins/datatables/DT_bootstrap.js"></script>
	<script>
	$(document).ready(function(){
		"use strict";

		App.init(); // Init layout and core plugins
		Plugins.init(); // Init all plugins
		FormComponents.init(); // Init all form-specific plugins
	});
	</script>	
<style>
select{
  border:1px solid gray;
 }
 .navbar .container .navbar-brand {
	display:block;
}


@media (min-width: 992px){
	#col-md-4_loaction {
		padding-left:0px !important;
		padding-right:0px !important;
		width: 130px;
		margin-top: 70px;
		text-align:right;
		float:right;
	}
}
@media (min-width: 768px){
	#col-md-4_loaction {
		padding-left:0px !important;
		padding-right:0px !important;
		width: 130px;
		margin-top: 60px;
		float:right;
		text-align:right;
	}
}
@media (min-width: 368px){
	#col-md-4_loaction{
		padding-left:0px !important;
		padding-right:0px !important;
		width:130px;
		margin-top: 65px;
		float:right;
		text-align:right;
	}
}

@media (max-width: 367px){
	#col-md-4_loaction {
		padding-left:0px !important;
		padding-right:0px !important;
		width:130px;
		margin-top: 0px;
		text-align:right;
		float:right;
		
		
	}
}

@media (max-width: 979px)
	.navbar .toggle-sidebar {
		display: block;
	}
}
</style>
</head>

<body style="font-size: 13px;">

	<!-- Header -->
	<header class="header navbar navbar-fixed-top" role="banner">
		<!-- Top Navigation Bar -->
		<div class="container">
			<div class="col-md-12">
			<!-- Only visible on smartphones, menu toggle -->
			<ul class="nav navbar-nav">
				<li class="nav-toggle"><a href="javascript:void(0);" title=""><i class="icon-reorder"></i></a></li>
			</ul>

			<!-- Logo -->
			<a class="navbar-brand" href=<?php if($_SESSION["role"]=="admin"){echo "manage_doctors.php";}
			  else if($_SESSION["role"]=="doctor"){
					echo "patient_queue_screen.php";
		   } ?>><img src="../assets/img/logo.png" alt="logo" />    
		   </a>
			<!-- /logo -->

			<!-- Sidebar Toggler -->
			<a href="#" class="toggle-sidebar bs-tooltip" data-placement="bottom" data-original-title="Toggle navigation" style="color: black;">
				<i class="icon-reorder"></i>
			</a>
			<!-- /Sidebar Toggler -->			
		</div>
		
		<div class="col-md-4" id="col-md-4_loaction" style="width:200px">
		<?php
		 if($_SESSION["role"]=="doctor"){?>
			<span style="white-space:nowrap"><b>Welcome <?php echo $_SESSION["ses_staff_name"];?>!</b></span><br>
		<?php }?>
		 
			<b>Location:<?php echo $_SESSION["location"];?></b>
		 </div>
			
		</div>
		<!-- /top navigation bar -->		
	</header> <!-- /.header -->

	<div id="container"> <!-- Main Container -->
		<div id="sidebar" style="background: #02A5F2;color: #FFFFFF;" class="sidebar-fixed">
			<div id="sidebar-content" style="">				

				<?php
				include_once("includes/left_menu.php");
				?>

			</div>
			<div id="divider" class="resizeable"></div>
		</div>
		<!-- /Sidebar -->	