<!--=== Navigation ===-->
<?php
$style="";
if($_SESSION["role"]=="admin"){
$style="display:none;";

}
?>
<ul id="nav">
<?php
if($_SESSION["role"]=="admin"){
?>



 <li>
		<a href="patient_queue_screen.php">
			<i class="icon-edit"></i>
			Patients Queue
		</a>
	</li>


     <li>
		<a href="manage_doctors.php">
			<i class="icon-plus"></i>
			Manage Doctors
		</a>
	</li>	
	<li>
		<a href="manage_patients.php">
			<i class="icon-user"></i>
			Manage Patients
		</a>
	</li>
	 <li>
		<a href="upload_content.php">
			<i class="icon-edit"></i>
			Manage Content
		</a>
	</li>
	<li>
		<a href="changepassword.php">
			<i class="icon-cog"></i>
			Change Login
		</a>
	</li>	
	<li>
		<a href="settings.php">
			<i class="icon-wrench"></i>
			Control panel
		</a>
	</li>
	<li>
		<a href="walkin_alert_content.php">
			<i class="icon-file"></i>
			Walkin Alert Content
		</a>
	</li>	
	<li>
		<a href="notification_list.php">
			<i class="icon-comments"></i>
			Global Notification
		</a>
	</li>	
	<li>
		<a href="logout.php">
			<i class="icon-off"></i>
			Logout
		</a>
	</li>	
	<?php
}
if($_SESSION["role"]=="doctor")
{
?>
 


	<li>
		<a href="patient_queue_screen.php">
			<i class="icon-edit"></i>
			Patients Queue
		</a>
	</li>	
	<!-- <li>
		<a href="manage_patients.php">
			<i class="icon-user"></i>
			Manage Patients
		</a>
	</li>	 -->
	
	<!-- <li>
		<a href="changepassword.php">
			<i class="icon-cog"></i>
			Change Password
		</a>
	</li>	 -->
	<li>
		<a href="logout.php">
			<i class="icon-off"></i>
			Logout
		</a>
	</li>	


<?php
}	
?>	
</ul>
<!--=== /Navigation ===-->