<?php
include("includes/configure.php");
$location=$_GET["loc"];
$FromLocation=$_GET["frm"];
include("includes/header.php");
?>
<script src="http://code.jquery.com/jquery-1.7.2.min.js"></script>
<script src="http://code.jquery.com/ui/1.8.21/jquery-ui.min.js"></script>
<script src="js/jquery.ui.touch-punch.min.js" language="javascript"></script>
<style>
.widget.box {
    border: 0px solid #d9d9d9;
}
.page-title > h3{
	margin: 0;
    margin-bottom: 10px;
    color: #0089CB;
    font-weight: 400;
    font-size: 20px;
}
#content{
	background: #fff;
	margin-left: 0px;
	overflow: visible;
	padding-bottom: 30px;
	min-height: 100%;
}
.navbar .container .navbar-brand {
	display:block;
}
@media screen and (min-width: 1000px) and (max-width: 1920px) {
	.page-title {
    	float: left;
        padding-top: 40px;
        padding-bottom: 0px;
    }
	.dotyes {
    	height: 15px;
    	width: 15px;
    	background-color: #08559C;
    	border-radius: 50%;
    	display: inline-block;
	}
	.dotno {
	    height: 15px;
	    width: 15px;
	    background-color:#FF4500;
	    border-radius: 50%;
	    display: inline-block;
	}
   .page-header {
    	margin-top:70px;
    }
    .page-title > h3 {
	    font-size: 55px;
	    font-weight: 400;
    }
	.widget,.box{
		font-size: 45px;
		white-space: nowrap;
	}
	.icon-reorder{
		font-size: 40px !important;
	}
	.patientfon{
		font-size: 40px !important;
	}
	#queuelisttbl thead tr th{
  	  height: 90px ;
  	  vertical-align: middle;
  	  background-color: #055095;
  	  color:white;
  	  border-right: 2px solid #FFFFFF;
  	  font-weight: 400;
    }
    #queuelisttbl tbody tr td{
  	  height: 80px ;
  	  vertical-align: middle;
  	  color:black;
  	  border-right: 2px solid #FFFFFF;
  	  font-weight: 400;
    }
    #queuelisttbl tbody tr td:last-child {
       border-right: 1px solid #ddd;
    }
    #queuelisttbl thead tr th:last-child {
       border-right: 1px solid #ddd;
    }
    .servercaltime{
    	color: #0089CB;
    	font-weight: 500;
    	font-size: 30px;
    	margin-top:25px;
    }
    .widget.box .widget-content{
    	padding: 0px;
    }
}
@media screen and (max-width: 1000px) {
  .page-header {
     margin-top:50px;
   }
    
}

@media (min-width: 1921px) {
	.page-title {
    	float: left;
        padding-top: 40px;
        padding-bottom: 0px;
    }
	.page-title > h3 {
		font-size: 65px;
		font-weight: 400;
	}
	.widget,.box{
		font-size: 55px;
	}
	.icon-reorder{
	   font-size: 50px !important;
	}
	.patientfon{
	   font-size: 50px !important;
	}
    .dotyes {
    	height: 15px;
    	width: 15px;
    	background-color: #08559C;
    	border-radius: 50%;
    	display: inline-block;
	}
	.dotno {
	    height: 15px;
	    width: 15px;
	    background-color:#FF4500;
	    border-radius: 50%;
	    display: inline-block;
	}
	#queuelisttbl thead tr th{
  	  height: 90px ;
  	  vertical-align: middle;
  	  background-color: #055095;
  	  color:white;
  	  border-right: 2px solid #FFFFFF;
  	  font-weight: 400;
    }
    #queuelisttbl tbody tr td{
  	  height: 80px ;
  	  vertical-align: middle;
  	  color:black;
  	  border-right: 2px solid #FFFFFF;
  	  font-weight: 400;
    }
    #queuelisttbl tbody tr td:last-child {
       border-right: 1px solid #ddd;
    }
    #queuelisttbl thead tr th:last-child {
       border-right: 1px solid #ddd;
    }
    .servercaltime{
    	color: #0089CB;
    	font-weight: 500;
    	font-size: 30px;
    	margin-top:25px;
    }
    .widget.box .widget-content{
    	padding: 0px;
    }
}

@media screen and (min-width: 0px) and (max-width: 999px) {
	.dotyes {
	    height: 8px;
	    width: 8px;
	    background-color: #08559C;
	    border-radius: 50%;
	    display: inline-block;
	}
	.dotno {
	    height: 8px;
	    width: 8px;
	    background-color:#FF4500;
	    border-radius: 50%;
	    display: inline-block;
	}
	#queuelisttbl tbody tr td{
  	  vertical-align: middle;
  	  color:black;
  	  border-right: 1px solid #FFFFFF;
  	  font-weight: bold;
    }
    #queuelisttbl thead tr th{
  	  vertical-align: middle;
  	  background-color: #055095;
  	  color:white;
  	  border-right: 1px solid #FFFFFF;
  	  font-weight: bold;
    }
    #queuelisttbl tbody tr td:last-child {
       border-right: 1px solid #ddd;
    }
    #queuelisttbl thead tr th:last-child {
       border-right: 1px solid #ddd;
    }
    .servercaltime{
    	color: #0089CB;
    	font-weight: 400;
    	font-size: 20px;
    	margin-top:27px;
    }
    .widget.box .widget-content{
		padding: 0px;
    }
}
@media only screen and (min-device-width : 1000px) and (max-device-width : 1920px) and (orientation : portrait) 
and (-webkit-min-device-pixel-ratio: 1) { 
	.page-title {
    	float: left;
        padding-top: 40px;
        padding-bottom: 0px;
    }
	.dotyes {
    	height: 15px;
    	width: 15px;
    	background-color: #08559C;
    	border-radius: 50%;
    	display: inline-block;
	}
	.dotno {
	    height: 15px;
	    width: 15px;
	    background-color:#FF4500;
	    border-radius: 50%;
	    display: inline-block;
	}
   .page-header {
    	margin-top:70px;
    }
    .page-title > h3 {
	    font-size: 55px;
	    font-weight: 400;
    }
	.widget,.box{
		font-size: 45px;
		white-space: nowrap;
	}
	.icon-reorder{
		font-size: 40px !important;
	}
	.patientfon{
		font-size: 40px !important;
	}
	#queuelisttbl thead tr th{
  	  height: 90px ;
  	  vertical-align: middle;
  	  background-color: #055095;
  	  color:white;
  	  border-right: 2px solid #FFFFFF;
  	  font-weight: 400;
  	  text-align: center;
    }
    #queuelisttbl tbody tr td{
  	  height: 80px ;
  	  vertical-align: middle;
  	  color:black;
  	  border-right: 2px solid #FFFFFF;
  	  font-weight: 400;
    }
    #queuelisttbl tbody tr td:last-child {
       border-right: 1px solid #ddd;
    }
    #queuelisttbl thead tr th:last-child {
       border-right: 1px solid #ddd;
    }
    .servercaltime{
    	color: #0089CB;
    	font-weight: 500;
    	font-size: 30px;
    	margin-top:25px;
    }
    .widget.box .widget-content{
    	padding: 0px;
    } 
    .dispheading {
    	display: block;
    }
}

@media only screen and (min-device-width : 1921px) and (orientation : portrait) 
and (-webkit-min-device-pixel-ratio: 1) {
	.page-title {
    	float: left;
        padding-top: 40px;
        padding-bottom: 0px;
    }
	.page-title > h3 {
		font-size: 65px;
		font-weight: 400;
	}
	.widget,.box{
		font-size: 55px;
	}
	.icon-reorder{
	   font-size: 50px !important;
	}
	.patientfon{
	   font-size: 50px !important;
	}
    .dotyes {
    	height: 15px;
    	width: 15px;
    	background-color: #08559C;
    	border-radius: 50%;
    	display: inline-block;
	}
	.dotno {
	    height: 15px;
	    width: 15px;
	    background-color:#FF4500;
	    border-radius: 50%;
	    display: inline-block;
	}
	#queuelisttbl thead tr th{
  	  height: 90px ;
  	  vertical-align: middle;
  	  background-color: #055095;
  	  color:white;
  	  border-right: 2px solid #FFFFFF;
  	  font-weight: 400;
    }
    #queuelisttbl tbody tr td{
  	  height: 80px ;
  	  vertical-align: middle;
  	  color:black;
  	  border-right: 2px solid #FFFFFF;
  	  font-weight: 400;
    }
    #queuelisttbl tbody tr td:last-child {
       border-right: 1px solid #ddd;
    }
    #queuelisttbl thead tr th:last-child {
       border-right: 1px solid #ddd;
    }
    .servercaltime{
    	color: #0089CB;
    	font-weight: 500;
    	font-size: 30px;
    	margin-top:25px;
    }
    .widget.box .widget-content{
    	padding: 0px;
    }
}

@media only screen and (min-device-width : 768px) and (max-device-width : 999px) and (orientation : portrait) 
and (-webkit-min-device-pixel-ratio: 1) { 
	.page-title {
    	float: left;
        padding-top: 40px;
        padding-bottom: 0px;
    }
	.dotyes {
    	height: 15px;
    	width: 15px;
    	background-color: #08559C;
    	border-radius: 50%;
    	display: inline-block;
	}
	.dotno {
	    height: 15px;
	    width: 15px;
	    background-color:#FF4500;
	    border-radius: 50%;
	    display: inline-block;
	}
   .page-header {
    	margin-top:70px;
    }
    .page-title > h3 {
	    font-size: 35px;
	    font-weight: 400;
    }
	.widget,.box{
		font-size: 30px;
		white-space: nowrap;
	}
	.icon-reorder{
		font-size: 30px !important;
	}
	.patientfon{
		font-size: 30px !important;
	}
	#queuelisttbl thead tr th{
  	  height: 90px ;
  	  vertical-align: middle;
  	  background-color: #055095;
  	  color:white;
  	  border-right: 2px solid #FFFFFF;
  	  font-weight: 400;
  	  text-align: center;
    }
    #queuelisttbl tbody tr td{
  	  height: 80px ;
  	  vertical-align: middle;
  	  color:black;
  	  border-right: 2px solid #FFFFFF;
  	  font-weight: 400;
    }
    #queuelisttbl tbody tr td:last-child {
       border-right: 1px solid #ddd;
    }
    #queuelisttbl thead tr th:last-child {
       border-right: 1px solid #ddd;
    }
    .servercaltime{
    	color: #0089CB;
    	font-weight: 500;
    	font-size: 25px;
    	margin-top:25px;
    }
    .widget.box .widget-content{
    	padding: 0px;
    } 
    .dispheading{
    	display: block;
    }
}
@media screen and (min-width: 424px) and (max-width: 633px) {
  .page-title {
    float: left;
    padding-top: 20px;
    padding-bottom: 0px;
  }
  .page-header {
    margin-top:40px;
  }
  .page-title > h3 {
    font-size: 24px !important;
    font-weight: 400;
  }
  .widget,.box{
    font-size: 17px !important;
    white-space: nowrap;
  }
  .icon-reorder{
    font-size: 17px !important;
  }
  .patientfon{
    font-size: 17px !important;
  }
  #queuelisttbl thead tr th{
    height: 90px ;
    vertical-align: middle;
    background-color: #055095;
    color:white;
    border-right: 2px solid #FFFFFF;
    font-weight: 400;
    text-align: center;
  }
  #queuelisttbl tbody tr td{
    height: 36px ;
    vertical-align: middle;
    color:black;
    border-right: 2px solid #FFFFFF;
    font-weight: 400;
  }
  #queuelisttbl tbody tr td:last-child {
    border-right: 1px solid #ddd;
  }
  #queuelisttbl thead tr th:last-child {
    border-right: 1px solid #ddd;
  }
  .servercaltime{
    color: #0089CB;
    font-weight: 500;
    font-size: 22px !important;
    margin-top:25px;
  }
  .widget.box .widget-content{
    padding: 0px;
  } 
  #container {
    position: relative;
    left: 0px;
    padding-left: 10px;
    padding-right: 10px;
  }
}
@media screen and (min-width: 0px) and (max-width:425px) {
  .page-title {
    float: left;
    padding-top: 20px;
    padding-bottom: 0px;
  }
  .page-header {
    margin-top:40px;
  }
  .page-title > h3 {
    font-size: 18px !important;
    font-weight: 400;
  }
  .widget,.box{
    font-size: 19px !important;
    white-space: nowrap;
  }
  .icon-reorder{
    font-size: 15px !important;
  }
  .patientfon{
    font-size: 15px !important;
  }
  #queuelisttbl thead tr th{
    height: 90px ;
    vertical-align: middle;
    background-color: #055095;
    color:white;
    border-right: 2px solid #FFFFFF;
    font-weight: 400;
    text-align: center;
  }
  #queuelisttbl tbody tr td{
    height: 36px ;
    vertical-align: middle;
    color:black;
    border-right: 2px solid #FFFFFF;
    font-weight: 400;
  }
  #queuelisttbl tbody tr td:last-child {
    border-right: 1px solid #ddd;
  }
  #queuelisttbl thead tr th:last-child {
    border-right: 1px solid #ddd;
  }
  .servercaltime{
    color: #0089CB;
    font-weight: 500;
    font-size: 19px !important;
    margin-top:25px;
    margin-right: 40px;
  }
  .widget.box .widget-content{
    padding: 0px;
  } 
  #container {
    position: relative;
    left: 0px;
    padding-left: 10px;
    padding-right: 10px;
  }
  .widget-content{
    width: 100%;
    overflow-x: scroll;
  } 
}
.dispheading{
  display: block;
}
#queuelisttbl thead tr th{
  text-align: center;
}
</style>

<script>
url = document.location.href;
xend = url.lastIndexOf("/") + 1;
var base_url = url.substring(0, xend);

$(function() {
$( "#sortable" ).sortable({
	revert: true,
 axis: 'y',
    update: function (event, ui) {
        var data = $(this).sortable('serialize');
        // POST to server using $.post or $.ajax
        $.ajax({
            data: data,
            type: 'POST',
            url: base_url + 'includes/savequeue.php',
			success: function(strRplytxt){
		   }
        });
    }});
$( "#draggable" ).draggable({
connectToSortable: "#sortable",
helper: "clone",
revert: "invalid"
});
$( "ul, li" ).disableSelection();
});

var currenttime = '<?php print date("F d, Y H:i:s", time())?>' //PHP method of getting server date

var montharray=new Array("January","February","March","April","May","June","July","August","September","October","November","December")
var serverdate=new Date(currenttime)

function padlength(what){
var output=(what.toString().length==1)? "0"+what : what
return output
}

function displaytime(){
serverdate.setSeconds(serverdate.getSeconds()+1)
var datestring=montharray[serverdate.getMonth()]+" "+padlength(serverdate.getDate())+", "+serverdate.getFullYear()
//var timestring=padlength(serverdate.getHours())+":"+padlength(serverdate.getMinutes())+":"+padlength(serverdate.getSeconds())

 var hours = serverdate.getHours();
 var minutes = serverdate.getMinutes();

var ampm = hours >= 12 ? 'pm' : 'am';

    if (hours > 12) {
        hours -= 12;
    } else if (hours === 0) {
        hours = 12;
    }
var timestring= padlength(hours) +":"+padlength(serverdate.getMinutes())+":"+padlength(serverdate.getSeconds());


document.getElementById("servertime").innerHTML=datestring+" "+timestring +" "+ampm;
}

window.onload=function(){
setInterval("displaytime()", 1000)
}
</script>

		<!-- Center Main page Content -->
		<div id="content"  style="margin-left:0px;">
			<div class="container">
				<!--=== Page Header ===-->

			<div class="page-header">
					<div class="page-title">
						<h3>Current Queue in  <?php echo $location;?></h3>
					</div>		
					
				</div>
				<!-- /Page Header -->

				 
				<!--=== Responsive DataTable ===-->
				<div class="row">
					<div class="col-md-12">
						<div class="widget box">
							<!-- <div class="widget-header" style="line-height:inherit;">
								<h4 class="patientfon"><i class="icon-reorder"></i>Patients</h4>
								<div style="float:right;"><strong>Current Time: <span id="servertime"></span></strong></div>
							</div> -->
												
							<div class="widget-content">
								<table class="table table-hover table-striped table-bordered table-highlight-head" id="queuelisttbl">
									<thead>
										<tr>
											<th width="30%">Patient <span class="dispheading">(initials)</span></th>
											<th width="40%">Doctor</th>
											<th width="30%">Time <span class="dispheading">(waiting since)<span></th>
										</tr>
									</thead>
									 <tbody>
									 <?php
										$getPatientsQry="select * from tbl_patient where location='".$location."' and patient_status='Appointment fixed' and register_date='".date('Y-m-d')."' order by display_order,reg_time asc";
									 //exit;
									 $getPatientsRes=$DBCONN->query($getPatientsQry);
									 $getPatientsCnt=$getPatientsRes->rowCount();
									 if($getPatientsCnt>0){
									 	$sno=1;
										 foreach($getPatientsRes->fetchAll(PDO::FETCH_ASSOC) as $getPatientsRow) {
											 $doctor_id=$getPatientsRow["doctor_id"];
											 $location_option=$getPatientsRow["location_option"];
											 // if(strtolower($location_option) == "yes") {
            //                                     $dotcolor="dotyes";
											 // } 
											 // if(strtolower($location_option) == "no"){
            //                                       $dotcolor="dotno";
											 // }
											 if($doctor_id>0&&$doctor_id!=""){
												 $getDocQry="select * from tbl_staff where staff_id='".$doctor_id."'";
												$getDocRes=$DBCONN->query($getDocQry);
												$getDocRow=$getDocRes->fetch(PDO::FETCH_ASSOC);
												$doctor_name=stripslashes($getDocRow["staff_name"]);
											 }
											 else{
                            $doctor_name="First Available Doctor";
											 }
											 $reg_time=stripslashes($getPatientsRow["reg_time"]);
											 if($reg_time!="" && $reg_time!="00:00:00"){
											  $reg_time=date('g:i A',strtotime(stripslashes($getPatientsRow["reg_time"])));
											}
                      //$patient_name=preg_replace('/[^\p{L}\p{N}\s]/u', '', $getPatientsRow["patient_name"]);
                      //$family_name=preg_replace('/[^\p{L}\p{N}\s]/u', '', $getPatientsRow["family_name"]);
											$bgcolor=($sno%2==0)?'#E5F1FC':'#FFFFFF';

										 ?>
												
											    
													<tr style="background-color:<?php echo $bgcolor;?>">
														<td width="25%"><?php echo strtoupper(substr(stripslashes($getPatientsRow["patient_name"]),0,1).substr(stripslashes($getPatientsRow["family_name"]),0,1))?>  </td>
														<td width="50%"><?php echo $doctor_name;?></td>
														<td width="25%" nowrap><?php echo  $reg_time;?></td>											
													</tr>
											
											
										<?php
										   $sno++;
										 }
										?>
										<?php
									    }
									
										else{

										?>
										<tr>
											<td colspan="3"><center>No patient(s) found.</center></td>
										</tr>
										<?php
										}
										?>
										
									</tbody>
								</table>
								<!-- <div class="row">
									<div class="table-footer">										
										<div class="col-md-12">
											<ul class="pagination">
												<li class="disabled"><a href="javascript:void(0);">&larr; Prev</a></li>
												<li class="active"><a href="javascript:void(0);">1</a></li>
												<li><a href="javascript:void(0);">2</a></li>
												<li><a href="javascript:void(0);">3</a></li>
												<li><a href="javascript:void(0);">4</a></li>
												<li><a href="javascript:void(0);">Next &rarr;</a></li>
											</ul>
										</div>
									</div>
								</div> -->
							</div>
						</div>
						<?php
						//if(!isset($_GET["frm"]))
						//{
						?>
						<!-- <h5>Please <a href="index.php">click here </a>to go in the queue</h5> -->
						<?php
						//}
						?>
					</div>

					<!-- /Table with Footer -->
							
						</div>
					</div>
				</div>
				<!-- /Responsive DataTable -->
				<!-- /Page Content -->
			</div>
			<!-- /.container -->

		</div>

		<!-- /Center Main page Content -->
<?php
include("includes/footer.php");
?>

