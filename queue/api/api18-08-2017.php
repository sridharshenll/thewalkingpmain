<?php
include("../includes/configure.php");
header('Content-Type: application/json');
error_reporting(0);

if(isset($_REQUEST['type']) && trim($_REQUEST['type'])!="")
{
       $type                 =   stripslashes($_REQUEST["type"]);
	   $jsonParams           =   stripslashes(urldecode($_REQUEST["jsonParams"]));
	   if((empty($jsonParams)) || (empty($type))) {

			  $dbdatetime=date('Y-m-d H:i:s');
			  $jsonParams="Empty Json Parameter";
			  $insertapi=array(":api_type"=>$type,":api_request"=>$jsonParams,":created_date"=>$dbdatetime);
			  $insertapiqry="insert into tbl_api_request_log(api_type,api_request,created_date) values (:api_type,:api_request,:created_date)";
			  $apiinsertqry=$DBCONN->prepare($insertapiqry);
			  $insertRes=$apiinsertqry->execute($insertapi); 
			  
		} else {

			  $dbdatetime=date('Y-m-d H:i:s');
			  $insertapi=array(":api_type"=>$type,":api_request"=>$jsonParams,":created_date"=>$dbdatetime);
			  $insertapiqry="insert into tbl_api_request_log(api_type,api_request,created_date) values (:api_type,:api_request,:created_date)";
			  $apiinsertqry=$DBCONN->prepare($insertapiqry);
			  $insertRes=$apiinsertqry->execute($insertapi); 
		 }


   if(strtolower($type)=="getlocations")
   {    
        $type                        =   stripslashes($_REQUEST["type"]);
		funLocatoinInfo($type); 
   }
   if(strtolower($type)=="getstaff")
	{   
	    $type                        =   stripslashes($_REQUEST["type"]);
		fungetdoctorInfo($type); 
	}
	if(strtolower($type)=="getpatientslist")
	{   
		$Json_patient_data           =   stripslashes($_REQUEST["jsonParams"]);
	    $patient_response            =   json_decode($Json_patient_data,true);
		$location                    =   $patient_response['location'];
		fungetpatientslistInfo($location); 
	}

	if(strtolower($type)=="addpatients"){

		$Json_addpatient_data            =   stripslashes(urldecode($_REQUEST["jsonParams"]));
	    $Add_patient_response            =   json_decode($Json_addpatient_data,true);
		funAddPatientsInfo($Add_patient_response);
	}
	if(strtolower($type)=="getpersonaldetails"){
		$Json_personal_data           =   stripslashes($_REQUEST["jsonParams"]);
	    $personal_response            =   json_decode($Json_personal_data,true);
		$medicare                     =    $personal_response['medicare'];
		fungetpersonaldetailsInfo($medicare); 
	}
}


function funLocatoinInfo($type)
{
	global $DBCONN;
	
	// Get location tbl_settings
	$getLocationQry="select * from tbl_settings";	
    $prepgetLocationQry=$DBCONN->prepare($getLocationQry);
    $prepgetLocationQry->execute();
    $getResCnt	=$prepgetLocationQry->rowCount();
    if($getResCnt>0)
	{
		$getResRows	=$prepgetLocationQry->fetchAll();
		$Arrgetdetails["responseCode"]  = 	1;
		$Arrgetdetails["message"]       =	"Success";

        $ArrLocation=Array();
		foreach($getResRows as $getResRow)
		{
			$setting_id				=	stripslashes($getResRow['setting_id']);
			$location_name			=	stripslashes($getResRow['location']);	
			$queue_system			=	stripslashes($getResRow['queue_system']);
			
			$ArrLocation["name"]	=	$location_name;			
			$ArrLocation["status"]	=	$queue_system;

			$Arrgetdetails["locations"][]	=	$ArrLocation;
		}
	 }

	 // Get apps name information for tbl_apps
		$getAppQry="select * from tbl_app_settings";	
		$prepgetAppQry=$DBCONN->prepare($getAppQry);
		$prepgetAppQry->execute();
		$getResCnt	=$prepgetAppQry->rowCount();
		if($getResCnt>0)
		{
			$getResRows	=$prepgetAppQry->fetchAll();
			$ArrgetAppDetails["responseCode"]  = 	1;
			$ArrgetAppDetails["message"]       =	"Success";

			$ArrApps=Array();
			foreach($getResRows as $getResRow)
			{
				$app_name	                      = stripslashes($getResRow['app_name']);	
				$app_status	                      = stripslashes($getResRow['app_status']);

				$ArrApps["name"]	              =	$app_name;			
				$ArrApps["status"]	              =	$app_status;
				$Arrgetdetails["apps"][]	      =	$ArrApps;
			}
	} 
	else
	{
	  $Arrgetdetails["responseCode"]=1;
	  $Arrgetdetails["message"]="No records found.";
	}

	$ArrgetLocationDetails=$Arrgetdetails;

 echo json_encode($ArrgetLocationDetails);
}


function fungetdoctorInfo($type){

	global $DBCONN;
	
	// Get staff information for tbl_staff
	$getStaffQry="select staff_id, staff_name, staff_qualification from tbl_staff where role='doctor'";	
    $prepgetStaffQry=$DBCONN->prepare($getStaffQry);
    $prepgetStaffQry->execute();
    $getResCnt	=$prepgetStaffQry->rowCount();
    if($getResCnt>0)
	{
		$getResRows	=$prepgetStaffQry->fetchAll();
		$ArrgetStaffDetails["responseCode"]  = 	1;
		$ArrgetStaffDetails["message"]       =	"Success";
        $ArrStaff=Array();
		foreach($getResRows as $getResRow)
		{
			$staff_id				          =	stripslashes($getResRow['staff_id']);
			$staff_name	                      = stripslashes($getResRow['staff_name']);	
			$staff_qualification	          = stripslashes($getResRow['staff_qualification']);
            
			$ArrStaff["staff_id"]	          =	$staff_id;	
			$ArrStaff["staff_name"]	          =	$staff_name;	
			$ArrStaff["staff_name"]	          =	$staff_name;			
			$ArrStaff["staff_qualification"]  =	$staff_qualification;
			$ArrgetStaffDetails["staffs"][]	  =	$ArrStaff;
		}
	 } 
	 else
	{
	  $ArrgetStaffDetails["responseCode"]=0;
	  $ArrgetStaffDetails["message"]="No records found.";
	}
  echo json_encode($ArrgetStaffDetails);
}

function fungetpatientslistInfo($location){
	 global $DBCONN;

	 $isvalid = validateInput('getPatientsList');
 	 if($isvalid === true){ 
	    // Get patients information for tbl_patient
		 $getPatientsQry="select * from tbl_patient where location='".$location."' and patient_status='Appointment fixed' and register_date='".date('Y-m-d')."' order by display_order,reg_time asc";
		 $prepgetPatientsQry=$DBCONN->prepare($getPatientsQry);
		 $prepgetPatientsQry->execute();
		 $getResCnt	=$prepgetPatientsQry->rowCount();
		  if($getResCnt>0)
	      {
				$getResRows	=$prepgetPatientsQry->fetchAll();
				$ArrgetPatientDetails["responseCode"]   = 	1;
				$ArrgetPatientDetails["message"]        =   "Success";
				$Currenttime                            =   date('Y-m-d H:i:s');
				$ArrgetPatientDetails["Currenttime"]    =   $Currenttime;

                $ArrPatient=Array();
				foreach($getResRows as $getResRow)
				{
					
                    $doctor_id	                      = stripslashes($getResRow['doctor_id']);
					if($doctor_id>0&&$doctor_id!=""){
						 $getDocQry="select * from tbl_staff where staff_id='".$doctor_id."'";
						 $prepgetDocQry=$DBCONN->prepare($getDocQry);
		                 $prepgetDocQry->execute();
						 $getDocRow	=$prepgetDocQry->fetch();
						 $doctor_name=stripslashes($getDocRow["staff_name"]);
				   }
				    else{
					   $doctor_name="First Available Doctor";
					}
					$patient_name	                  = stripslashes($getResRow['patient_name']);	
					$family_name	                  = stripslashes($getResRow['family_name']);
					$token_number	                  = stripslashes($getResRow['token_number']);
					$reg_time                         = stripslashes($getResRow["reg_time"]);
					if($reg_time!="" && $reg_time!="00:00:00"){
					     $reg_time=date('g:i A',strtotime(stripslashes($getResRow["reg_time"])));
					}

					$ArrPatient["first_name"]	          =	$patient_name;			
					$ArrPatient["given_name"]	          =	$family_name;
					$ArrPatient["doctor_name"]	          =	$doctor_name;
					$ArrPatient["appointment_time"]	      =	$reg_time;
					$ArrPatient["token_number"]	          =	$token_number;
					$ArrgetPatientDetails["patients"][]	  =	$ArrPatient;
				}
		  }
		  else
		  {
			  $ArrgetPatientDetails["responseCode"]=0;
			  $ArrgetPatientDetails["message"]="No records found.";
			  $Currenttime                            =   date('Y-m-d H:i:s');
			  $ArrgetPatientDetails["Currenttime"]    =   $Currenttime;
		  }
	 } 
	 else
	 {
	  $ArrgetPatientDetails["responseCode"]=0;
	  $ArrgetPatientDetails["message"]=$isvalid;
	  $Currenttime                            =   date('Y-m-d H:i:s');
	  $ArrgetPatientDetails["Currenttime"]    =   $Currenttime;
	 }

  echo json_encode($ArrgetPatientDetails);
}

function funAddPatientsInfo($Add_patient_response){
	
	    global $DBCONN;
     $isvalid = validateInput('addPatients');
 	 if($isvalid === true){ 
	    //Insert data form patient details
        $clinic_location				 =	 $Add_patient_response["clinic_location"];
		$clinic_confirmation             =	 "Yes";
		$patient_title					 =	 $Add_patient_response["patient_title"];
		$family_name					 =	 $Add_patient_response["family_name"];
		$given_names		             =	 $Add_patient_response["given_names"];
		$patient_dob                     =   $Add_patient_response["birth_date"];
		$buildversion                    =   $Add_patient_response["build_version"];
		
	    if($patient_dob=="")
		{
		  $patient_dob		= "";
		}
		$patient_medicareno				=	$Add_patient_response["patient_medicareno"];
		$patient_referno				=	$Add_patient_response["patient_referno"];
	    $patient_ref_expiry				=	$Add_patient_response["expiry_month_medi"];
		$preffered_doctor				=	$Add_patient_response["preffered_doctor"];
		if($preffered_doctor==""){
			$preffered_doctor="0";
		}
		$patient_address				=	$Add_patient_response["patient_address"];
		$patient_suburb					=	$Add_patient_response["patient_suburb"];
		$patient_postcode				=	$Add_patient_response["patient_postcode"];
		$patient_homephone				=	$Add_patient_response["patient_homephone"];
		if($patient_homephone=="")
		{
			$patient_homephone		="";
		}
		$patient_mobile					=	$Add_patient_response["patient_mobile"];
		$patient_hcc					=	$Add_patient_response["patient_hcc"];
		if($patient_hcc=="")
		{
			$patient_hcc		="";
		}
		$hcc_ref_expiry		            =   $Add_patient_response["hcc_ref_expiry"];
		if($hcc_ref_expiry=="")
		{
			$hcc_ref_expiry		="";
		}
	    $is_keep_check                  =   $Add_patient_response["is_keep"];
        if($is_keep_check!=""){
		  $is_keep=$is_keep_check;
		} else {
		 $is_keep="1";
		}

	$getTknQry="select count(*) as token_cnt from tbl_patient where register_date='".date('Y-m-d')."' and location='".$clinic_location."'";
	$prepgetTknQry=$DBCONN->prepare($getTknQry);
	$prepgetTknQry->execute();
	$getTknRow	=$prepgetTknQry->fetch();
	
	$patient_token_number=$getTknRow["token_cnt"]+1; 

	$getPosition = "select max(display_order) as displayrow from tbl_patient where location='".$clinic_location."'";
	$prepgetPositionQry=$DBCONN->prepare($getPosition);
	$prepgetPositionQry->execute();
	$getDisplayRow	=$prepgetPositionQry->fetch();

	$DisplayRow = $getDisplayRow["displayrow"]+1;

    $reg_time=date('Y-m-d H:i:s');
	$register_date=date('Y-m-d');
	$created_date=date('Y-m-d H:i:s');
	$modified_date=date('Y-m-d H:i:s');
	
	$insertQry="insert into tbl_patient(doctor_id, patient_name, location, location_option, title, family_name, dob, medicare_no, reference_no, expiry_date, address, suburb, post_code, home_phone, mobile_phone, pension_card_no, pension_card_expiry_date ,reg_time, token_number, register_date ,patient_status, created_date, modified_date, display_order, buildversion, is_keep) values(:doctor_id, :given_names, :clinic_location, :clinic_confirmation, :title, :family_name, :dob,:medicareno, :patient_referno, :patient_ref_expiry, :patient_address, :patient_suburb, :patient_postcode, :homephone, :patient_mobile, :patient_hcc, :hcc_ref_expiry, :reg_time, :token_number, :register_date, :patient_status, :created_date, :modified_date, :displayorder, :buildversion, :is_keep)";
	
	$sql_prepare			= $DBCONN->prepare($insertQry);

	$response_array = array(":doctor_id"=>$preffered_doctor, ":given_names"=>ucwords($given_names), ":clinic_location"=>$clinic_location, ":clinic_confirmation"=>$clinic_confirmation, ":title"=>$patient_title, ":family_name"=>ucwords($family_name), ":dob"=>$patient_dob, ":medicareno"=>$patient_medicareno, ":patient_referno"=>$patient_referno, ":patient_ref_expiry"=>$patient_ref_expiry, ":patient_address"=>ucwords($patient_address), ":patient_suburb"=>ucwords($patient_suburb), ":patient_postcode"=>$patient_postcode, ":homephone"=>$patient_homephone, ":patient_mobile"=>$patient_mobile, ":patient_hcc"=>$patient_hcc, ":hcc_ref_expiry"=>$hcc_ref_expiry, ":reg_time"=>$reg_time, ":token_number"=>$patient_token_number, ":register_date"=>$register_date, ":patient_status"=>"Appointment fixed",":created_date"=>$created_date, ":modified_date"=>$modified_date,":displayorder"=>$DisplayRow,":buildversion"=>$buildversion, ":is_keep"=>$is_keep); 
    
	//print_r($response_array);exit;
	 $insert_res=$sql_prepare->execute($response_array);

	 if($insert_res){
		      $ArrgetPatientDetails["responseCode"]=1;
			  $ArrgetPatientDetails["message"]="Patient details save successfully";

	 } else {
		      $ArrgetPatientDetails["responseCode"]=0;
			  $ArrgetPatientDetails["message"]="Patient details not inserted.";
	 }
  } 
	else {
	  $ArrgetPatientDetails["responseCode"]=0;
	  $ArrgetPatientDetails["message"]=$isvalid;
  }
	 
   echo json_encode($ArrgetPatientDetails);
}
function fungetpersonaldetailsInfo($medicare){
	 global $DBCONN;
	 $isvalid = validateInput('getPersonalDetails');
 	 if($isvalid === true){ 
			$getMedicareQry="select * from tbl_patient where medicare_no='".$medicare."' order by patient_id DESC";
			$prepgetMedicareQry=$DBCONN->prepare($getMedicareQry);
			$prepgetMedicareQry->execute();
			$getResCnt	=$prepgetMedicareQry->rowCount();
			if($getResCnt>0)
			{
			    $getResRows	                    =   $prepgetMedicareQry->fetch();
				$address					    =	stripslashes(trim($getResRows["address"]));
				$suburb							=	stripslashes($getResRows["suburb"]);
				$post_code						=	stripslashes($getResRows["post_code"]);
				$home_phone						=	stripslashes($getResRows["home_phone"]); 	
				$mobile_phone					=	stripslashes($getResRows["mobile_phone"]);
				$pension_card_no				=	stripslashes($getResRows["pension_card_no"]);
				$pension_card_expiry_date		=	stripslashes($getResRows["pension_card_expiry_date"]);
				$is_keep					    =	stripslashes(trim($getResRows["is_keep"]));
                if($is_keep=="0"){
					$ArrgetMedicareDetails["responseCode"]        = 	1;
					$ArrgetMedicareDetails["message"]             =	"Success";
					$ArrMedicareDetails["patient_address"]	      =	$address;			
					$ArrMedicareDetails["patient_suburb"]	      =	$suburb;
					$ArrMedicareDetails["patient_postcode"]	      =	$post_code;
					$ArrMedicareDetails["patient_homephone"]	  =	$home_phone;
					$ArrMedicareDetails["patient_mobile"]	      =	$mobile_phone;
					$ArrMedicareDetails["patient_hcc"]	          =	$pension_card_no;
					$ArrMedicareDetails["hcc_ref_expiry"]	      =	$pension_card_expiry_date;
					$ArrgetMedicareDetails["patient_details"][]   = $ArrMedicareDetails;
				} else {

					$ArrgetMedicareDetails["responseCode"]=0;
				    $ArrgetMedicareDetails["message"]="No records found.";
				}
			} else {
				$ArrgetMedicareDetails["responseCode"]=0;
				$ArrgetMedicareDetails["message"]="No records found.";
		   }
	 } else{
		 
	    $ArrgetMedicareDetails["responseCode"]=0;
	    $ArrgetMedicareDetails["message"]=$isvalid;
	 }
		  
  //$json = preg_replace("#(/\*([^*]|[\r\n]|(\*+([^*/]|[\r\n])))*\*+/)|([\s\t]//.*)|(^//.*)#", '', $ArrgetMedicareDetails);
   echo json_encode($ArrgetMedicareDetails);
 
  
}


function validateInput($action){
			
			//****************
			/* @parameter: $action is string
			/* @return : true if validation is success or error message
			****************/
		    
            $jsonParams = json_decode($_REQUEST['jsonParams'],true);
			$check_keys = array_keys($jsonParams);
			//print_r($check_keys);
			
			switch($action){
				
				case 'getPatientsList':
 					$check_for = array('location');
					if(!empty($jsonParams['location'])){
						$rtnval = true;
					}else{
						
						$rtnval = "Invalid parameters supplied. Provide valid input ". implode(', ', array_diff($check_for, $check_keys));
					}
				break;
				case 'getPersonalDetails':
 					$check_for = array('medicare');
					if(!empty($jsonParams['medicare'])){
						$rtnval = true;
					}else{
						$rtnval = "Invalid parameters supplied. Provide valid input ". implode(', ', array_diff($check_for, $check_keys));
					}
				break;
				case 'addPatients':
					
 					$check_for = array('clinic_location','patient_title','family_name','given_names','birth_date','patient_medicareno','patient_referno','expiry_month_medi','preffered_doctor');
				   
					if(!empty($jsonParams['clinic_location']) && !empty($jsonParams['patient_title']) && !empty($jsonParams['family_name']) && !empty($jsonParams['given_names']) && !empty($jsonParams['birth_date']) && !empty($jsonParams['patient_medicareno']) && !empty($jsonParams['patient_referno']) && !empty($jsonParams['expiry_month_medi'])&& ($jsonParams['preffered_doctor']!="")){
						
						$rtnval = true;
					}else{
						
						$rtnval = "Invalid parameters supplied. Provide valid input ". implode(', ', array_diff($check_for, $check_keys));
					}
				break;
				default:
					$rtnval = 'Invalid request';

			}

			return $rtnval;
	
		}

		

?>
