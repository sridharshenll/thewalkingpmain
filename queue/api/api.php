<?php
include("../includes/configure.php");
header('Content-Type: application/json');
error_reporting(0);

if (isset($_REQUEST['type']) && trim($_REQUEST['type'])!= "") {
	$type = stripslashes($_REQUEST["type"]);
	$jsonParams = stripslashes(urldecode($_REQUEST["jsonParams"]));
	if ((empty($jsonParams)) || (empty($type))) {
		$dbdatetime=date('Y-m-d H:i:s');
		$jsonParams="Empty Json Parameter";
		$insertapi=array(":api_type"=>$type,":api_request"=>$jsonParams,":created_date"=>$dbdatetime);
		$insertapiqry="insert into tbl_api_request_log(api_type,api_request,created_date) values (:api_type,:api_request,:created_date)";
		$apiinsertqry=$DBCONN->prepare($insertapiqry);
		$insertRes=$apiinsertqry->execute($insertapi);
	} else {
		$dbdatetime=date('Y-m-d H:i:s');
		$insertapi=array(":api_type"=>$type,":api_request"=>$jsonParams,":created_date"=>$dbdatetime);
		$insertapiqry="insert into tbl_api_request_log(api_type,api_request,created_date) values (:api_type,:api_request,:created_date)";
		$apiinsertqry=$DBCONN->prepare($insertapiqry);
		$insertRes=$apiinsertqry->execute($insertapi); 
	}
	if (strtolower($type)=="getlocations") {    
		$type = stripslashes($_REQUEST["type"]);
		funLocatoinInfo($type); 
	}
	if (strtolower($type)=="getstaff") {   
		$type = stripslashes($_REQUEST["type"]);
		$location = stripslashes($_REQUEST["location"]);
		fungetdoctorInfo($type,$location); 
	}
	if (strtolower($type)=="getpatientslist") {   
		$Json_patient_data = stripslashes($_REQUEST["jsonParams"]);
		$patient_response = json_decode($Json_patient_data,true);
		$location = $patient_response['location'];
		fungetpatientslistInfo($location); 
	}
	if (strtolower($type)=="addpatients") {
		$Json_addpatient_data = stripslashes(urldecode($_REQUEST["jsonParams"]));
		$Add_patient_response = json_decode($Json_addpatient_data,true);
		funAddPatientsInfo($Add_patient_response);
	}
	if (strtolower($type)=="getpersonaldetails") {
		$Json_personal_data = stripslashes($_REQUEST["jsonParams"]);
		$personal_response = json_decode($Json_personal_data,true);
		$medicare = $personal_response['medicare'];
		fungetpersonaldetailsInfo($medicare); 
	}
	if(strtolower($type)=="adddevicedetails") {
		$device_details_response = stripslashes($_REQUEST["jsonParams"]);
		$device_details = json_decode($device_details_response,true);
		funadddevicedetails($device_details);
	}

	if (strtolower($type)=="getconfig") {
		fungetconfig(); 
	}

	if (strtolower($type)=="getlang") {
		fungetlang();
	}

	if (strtolower($type)=="getnotificationlist") {
		$json_fcm_token_data = stripslashes($_REQUEST["jsonParams"]);
		$fcm_token_response = json_decode($json_fcm_token_data,true);
		$fcm_token = $fcm_token_response['fcm_token'];
		fungetnotificationlist($fcm_token);
	}

	if (strtolower($type)=="removenotificationlist") {
		$remove_data = stripslashes($_REQUEST["jsonParams"]);
		$fcm_token_response = json_decode($remove_data,true);
		$fcm_token = $fcm_token_response['fcm_token'];
		$notification_id = $fcm_token_response['notification_id'];
		funremovenotificationlist($fcm_token, $notification_id);
	}
}

function fungetconfig()
{
	global $DBCONN;

	$Arrgetdetails["responseCode"] = 1;
	$Arrgetdetails["message"] = "Success";
	$Arrgetdetails["lang"] = array(
			array( 'key' => 'en', 'value' => 'English'),
			array( 'key' => 'ch', 'value' => '中文'),
		);
	
	$getContentQry = "select * from tbl_content where cnt_pagename='announcement'";	
	$prepgetContentQry=$DBCONN->prepare($getContentQry);
	$prepgetContentQry->execute();
	$getResRows	= $prepgetContentQry->fetch();

	$Arrgetdetails["announcement_content"] = array(
			"announcement_title" => isset($getResRows["page_title"]) ? $getResRows["page_title"] : "",
			"message" => isset($getResRows["cnt_content"]) ? $getResRows["cnt_content"] : "",
			"status" => isset($getResRows["status"]) ? $getResRows["status"] : "",
		);

	echo json_encode($Arrgetdetails);
}

function fungetlang()
{
	$lang_type = $_REQUEST['lang_type'];
	// Read the JSON file
	$json = file_get_contents($_SERVER['DOCUMENT_ROOT'] . '/queue/api/lang/' . $lang_type . '.json');

	$data["responseCode"] = 1;
	$data["message"] = "Success";
	$data["lang"] = json_decode($json,true);

	echo json_encode($data);exit;
}

function funLocatoinInfo($type)
{
	global $DBCONN;

	// Get location tbl_settings
	$getLocationQry="select * from tbl_settings";	
	$prepgetLocationQry=$DBCONN->prepare($getLocationQry);
	$prepgetLocationQry->execute();
	$getResCnt =$prepgetLocationQry->rowCount();
	if ($getResCnt>0) {
		$getResRows	=$prepgetLocationQry->fetchAll();
		$Arrgetdetails["responseCode"]  = 	1;
		$Arrgetdetails["message"]       =	"Success";

          $ArrLocation=Array();
		foreach($getResRows as $getResRow)
		{
			$setting_id = stripslashes($getResRow['setting_id']);
			$location_name = stripslashes($getResRow['location']);	
			$queue_system = stripslashes($getResRow['queue_system']);

			$ArrLocation["name"]	=	$location_name;			
			$ArrLocation["status"]	=	$queue_system;

			$Arrgetdetails["locations"][]	=	$ArrLocation;
		}
	 }

	 // Get apps name information for tbl_apps
	// 	$getAppQry="select * from tbl_app_settings";
	// 	$prepgetAppQry=$DBCONN->prepare($getAppQry);
	// 	$prepgetAppQry->execute();
	// 	$getResCnt	=$prepgetAppQry->rowCount();
	// 	if($getResCnt>0)
	// 	{
	// 		$getResRows	=$prepgetAppQry->fetchAll();
	// 		$ArrgetAppDetails["responseCode"]  = 	1;
	// 		$ArrgetAppDetails["message"]       =	"Success";

	// 		$ArrApps=Array();
	// 		foreach($getResRows as $getResRow)
	// 		{
	// 			$app_name	                      = stripslashes($getResRow['app_name']);
	// 			$app_status	                      = stripslashes($getResRow['app_status']);

	// 			$ArrApps["name"]	              =	$app_name;
	// 			$ArrApps["status"]	              =	$app_status;
	// 			$Arrgetdetails["apps"][]	      =	$ArrApps;
	// 		}
	// }
	// else
	// {
	//   $Arrgetdetails["responseCode"]=1;
	//   $Arrgetdetails["message"]="No records found.";
	// }

	$ArrgetLocationDetails=$Arrgetdetails;

	echo json_encode($ArrgetLocationDetails);
}


function fungetdoctorInfo($type,$location){

	global $DBCONN;
	if($location!=""){
		// Get staff information for tbl_staff
		$getStaffQry="select staff_id, staff_name, staff_qualification from tbl_staff where role='doctor' and location=:location and status=:status";	
	    $prepgetStaffQry=$DBCONN->prepare($getStaffQry);
	    $prepgetStaffQry->execute(array(":location"=>$location,":status"=>"Active"));
	    $getResCnt	=$prepgetStaffQry->rowCount();
	    if($getResCnt>0)
		{
			$getResRows	=$prepgetStaffQry->fetchAll();
			$ArrgetStaffDetails["responseCode"]  = 	1;
			$ArrgetStaffDetails["message"]       =	"Success";
	        $ArrStaff=Array();
			foreach($getResRows as $getResRow)
			{
				$staff_id				          =	stripslashes($getResRow['staff_id']);
				$staff_name	                      = stripslashes($getResRow['staff_name']);	
				$staff_qualification	          = stripslashes($getResRow['staff_qualification']);
	            
				$ArrStaff["staff_id"]	          =	$staff_id;	
				$ArrStaff["staff_name"]	          =	$staff_name;	
				$ArrStaff["staff_name"]	          =	$staff_name;			
				$ArrStaff["staff_qualification"]  =	$staff_qualification;
				$ArrgetStaffDetails["staffs"][]	  =	$ArrStaff;
			}
		 } 
		 else
		{
		  $ArrgetStaffDetails["responseCode"]=0;
		  $ArrgetStaffDetails["message"]="No records found.";
		}
    } else {
    	$ArrgetStaffDetails["responseCode"]=0;
		$ArrgetStaffDetails["message"]="Invalid location send.";
    }
  echo json_encode($ArrgetStaffDetails);
}

function fungetpatientslistInfo($location){
	 global $DBCONN;

	 $isvalid = validateInput('getPatientsList');
 	 if($isvalid === true){ 
	    // Get patients information for tbl_patient
		if($location == 'Telehealth') {
			$getPatientsQry = "select * from tbl_patient where location='".$location."' and register_date='".date('Y-m-d')."' and patient_status='Appointment fixed' ORDER BY reg_time asc";
		} else {
			$getPatientsQry = "select * from tbl_patient where location in('".$location."','Telehealth') and register_date='".date('Y-m-d')."' and patient_status='Appointment fixed' ORDER BY reg_time asc";															
		}
		//  $getPatientsQry="select * from tbl_patient where location='".$location."' and patient_status='Appointment fixed' and register_date='".date('Y-m-d')."' order by display_order,reg_time asc";
		 $prepgetPatientsQry=$DBCONN->prepare($getPatientsQry);
		 $prepgetPatientsQry->execute();
		 $getResCnt	=$prepgetPatientsQry->rowCount();
		  if($getResCnt>0)
	      {
				$getResRows	=$prepgetPatientsQry->fetchAll();
				$ArrgetPatientDetails["responseCode"]   = 	1;
				$ArrgetPatientDetails["message"]        =   "Success";
				$Currenttime                            =   date('Y-m-d H:i:s');
				$ArrgetPatientDetails["Currenttime"]    =   $Currenttime;

                $ArrPatient=Array();
				foreach($getResRows as $getResRow)
				{
					
                    $doctor_id	                      = stripslashes($getResRow['doctor_id']);
					if($doctor_id>0&&$doctor_id!=""){
						 $getDocQry="select * from tbl_staff where staff_id='".$doctor_id."'";
						 $prepgetDocQry=$DBCONN->prepare($getDocQry);
		                 $prepgetDocQry->execute();
						 $getDocRow	=$prepgetDocQry->fetch();
						 $doctor_name=stripslashes($getDocRow["staff_name"]);
				   }
				    else{
					   $doctor_name="First Available Doctor";
					}
					$patient_name	                  = stripslashes($getResRow['patient_name']);	
					$family_name	                  = stripslashes($getResRow['family_name']);
					$token_number	                  = stripslashes($getResRow['token_number']);
					$reg_time                         = stripslashes($getResRow["reg_time"]);
					$patient_location                         = stripslashes($getResRow["location"]);
					if($reg_time!="" && $reg_time!="00:00:00"){
					     $reg_time=date('g:i A',strtotime(stripslashes($getResRow["reg_time"])));
					}

					$ArrPatient["first_name"]	          =	$patient_name;			
					$ArrPatient["given_name"]	          =	$family_name;
					$ArrPatient["doctor_name"]	          =	$doctor_name;
					$ArrPatient["appointment_time"]	      =	$reg_time;
					$ArrPatient["token_number"]	          =	$token_number;
					$ArrPatient["location"]	          =	$patient_location;
					$ArrgetPatientDetails["patients"][]	  =	$ArrPatient;
				}
		  }
		  else
		  {
			  $ArrgetPatientDetails["responseCode"]=0;
			  $ArrgetPatientDetails["message"]="No records found.";
			  $Currenttime                            =   date('Y-m-d H:i:s');
			  $ArrgetPatientDetails["Currenttime"]    =   $Currenttime;
		  }
		  $getLocationQry="select * from tbl_settings where location='".$location."'";	
		  $prepgetLocationQry=$DBCONN->prepare($getLocationQry);
		  $prepgetLocationQry->execute();
		  $getResLocation =$prepgetLocationQry->fetch();
		  $ArrgetPatientDetails["only_walkin_status"] = stripslashes($getResLocation['only_walkin']);

		  $getOnlyWalkinContentQry="select * from tbl_content where cnt_pagename='only_walkin_content' and location='".$location."'";
		  $prepgetOnlyWalkinContentQry=$DBCONN->prepare($getOnlyWalkinContentQry);
		  $prepgetOnlyWalkinContentQry->execute();
		  $getResContent =$prepgetOnlyWalkinContentQry->fetch();
		  $ArrgetPatientDetails["only_walkin_content"] = stripslashes($getResContent['cnt_content']);
	 } 
	 else
	 {
	  $ArrgetPatientDetails["responseCode"]=0;
	  $ArrgetPatientDetails["message"]=$isvalid;
	  $Currenttime                            =   date('Y-m-d H:i:s');
	  $ArrgetPatientDetails["Currenttime"]    =   $Currenttime;
	 }

  echo json_encode($ArrgetPatientDetails);
}

function funAddPatientsInfo($Add_patient_response){

	funlog(json_encode($Add_patient_response));
	if (empty($Add_patient_response["fcm_token"]) || empty($Add_patient_response["device_id"])) {
		$result = array();
		$result["responseCode"] = 0;
	  	$result["message"] = "Device id or FCM token is empty!";
	  	echo json_encode($ArrgetPatientDetails);
	  	exit;
	}

	global $DBCONN;
    $isvalid = validateInput('addPatients');
 	if($isvalid === true) {
        $clinic_location = $Add_patient_response["clinic_location"];
		$email_address = isset($Add_patient_response["patient_email_address"]) ? $Add_patient_response["patient_email_address"] : null;
		$next_of_kin = isset($Add_patient_response["patient_next_of_kin"]) ? $Add_patient_response["patient_next_of_kin"] : null;
		$next_of_kin_contact_number = isset($Add_patient_response["patient_next_of_kin_contact_number"]) ? $Add_patient_response["patient_next_of_kin_contact_number"] : null;
		$clinic_confirmation             =	 $Add_patient_response["clinic_confirmation"];
		$patient_title					 =	 $Add_patient_response["patient_title"];
		$family_name					 =	 $Add_patient_response["family_name"];
		$given_names		             =	 $Add_patient_response["given_names"];
		$patientdob                      =   $Add_patient_response["birth_date"];
		$buildversion                    =   $Add_patient_response["build_version"];
		$patient_dob                     =   !empty($patientdob) ? date("Y-m-d",strtotime($patientdob)) : "";
		funlog($patient_dob."\n");
		$patient_medicareno	=	$Add_patient_response["patient_medicareno"];
		$patient_referno =	$Add_patient_response["patient_referno"];
	    $patient_ref_expiry	=	$Add_patient_response["expiry_month_medi"];
		$preffered_doctor =	!empty($Add_patient_response["preffered_doctor"]) ? $Add_patient_response["preffered_doctor"] : 0;
		$patient_address				=	$Add_patient_response["patient_address"];
		$patient_suburb					=	$Add_patient_response["patient_suburb"];
		$patient_postcode				=	$Add_patient_response["patient_postcode"];
		$patient_homephone				=	$Add_patient_response["patient_homephone"] ?? "";
		$patient_mobile					=	$Add_patient_response["patient_mobile"];
		$patient_hcc					=	$Add_patient_response["patient_hcc"] ?? "";
		$hccrefexpiry		            =   $Add_patient_response["hcc_ref_expiry"];
	    $is_keep_check = $Add_patient_response["is_keep"];
		$is_keep = !empty($is_keep_check) ? $is_keep_check : "1";
		$hcc_ref_expiry = !empty($hccrefexpiry) ? date("Y-m-d",strtotime($hccrefexpiry)) : "";
	    $color_status = ($Add_patient_response["color_status"]!="")?"#".$Add_patient_response["color_status"]:"";
		$device_type = $Add_patient_response["device_type"];
		$fcm_token = $Add_patient_response["fcm_token"];
		$device_id = $Add_patient_response["device_id"];

		$getTknQry = "select count(*) as token_cnt from tbl_patient where register_date='".date('Y-m-d')."' and location='".$clinic_location."'";
		$prepgetTknQry = $DBCONN->prepare($getTknQry);
		$prepgetTknQry->execute();
		$getTknRow	= $prepgetTknQry->fetch();
		$patient_token_number = $getTknRow["token_cnt"]+1; 
		$getPosition = "select max(display_order) as displayrow from tbl_patient where location='".$clinic_location."'";
		$prepgetPositionQry = $DBCONN->prepare($getPosition);
		$prepgetPositionQry->execute();
		$getDisplayRow	= $prepgetPositionQry->fetch();
		$DisplayRow = $getDisplayRow["displayrow"]+1;
	    $reg_time = date('Y-m-d H:i:s');
		$register_date = date('Y-m-d');
		$created_date = date('Y-m-d H:i:s');
		$modified_date = date('Y-m-d H:i:s');

		$device_tbl_id = '';
		$getDevice = "select * from tbl_device_details where device_id='".$device_id."'";
		$prepgetDeviceQry = $DBCONN->prepare($getDevice);
		$prepgetDeviceQry->execute();
		$getDeviceResult	= $prepgetDeviceQry->fetch();
		if (!empty($getDeviceResult)) {
			$device_tbl_id = $getDeviceResult['id'];
		}

		$insertQry="insert into tbl_patient(doctor_id, patient_name, email_address, location, location_option, title, family_name, dob, medicare_no, reference_no, expiry_date, address, suburb, post_code, home_phone, mobile_phone, pension_card_no, pension_card_expiry_date ,reg_time, token_number, register_date ,patient_status, created_date, modified_date, display_order, buildversion, is_keep,color_status,device_type, next_of_kin, next_of_kin_contact_number, fcm_token, device_id, device_tbl_id) values(:doctor_id, :given_names, :email_address, :clinic_location, :clinic_confirmation, :title, :family_name, :dob,:medicareno, :patient_referno, :patient_ref_expiry, :patient_address, :patient_suburb, :patient_postcode, :homephone, :patient_mobile, :patient_hcc, :hcc_ref_expiry, :reg_time, :token_number, :register_date, :patient_status, :created_date, :modified_date, :displayorder, :buildversion, :is_keep,:color_status,:device_type, :next_of_kin, :next_of_kin_contact_number, :fcm_token, :device_id, :device_tbl_id)";
		$sql_prepare			= $DBCONN->prepare($insertQry);
		$response_array = array(":doctor_id"=>$preffered_doctor, ":given_names"=>ucwords($given_names), ":email_address" => $email_address, ":clinic_location"=>$clinic_location, ":clinic_confirmation"=>$clinic_confirmation, ":title"=>$patient_title, ":family_name"=>ucwords($family_name), ":dob"=>$patient_dob, ":medicareno"=>$patient_medicareno, ":patient_referno"=>$patient_referno, ":patient_ref_expiry"=>$patient_ref_expiry, ":patient_address"=>ucwords($patient_address), ":patient_suburb"=>ucwords($patient_suburb), ":patient_postcode"=>$patient_postcode, ":homephone"=>$patient_homephone, ":patient_mobile"=>$patient_mobile, ":patient_hcc"=>$patient_hcc, ":hcc_ref_expiry"=>$hcc_ref_expiry, ":reg_time"=>$reg_time, ":token_number"=>$patient_token_number, ":register_date"=>$register_date, ":patient_status"=>"Appointment fixed",":created_date"=>$created_date, ":modified_date"=>$modified_date,":displayorder"=>$DisplayRow,":buildversion"=>$buildversion, ":is_keep"=>$is_keep, ":color_status"=>$color_status,":device_type"=>$device_type, ":next_of_kin" => $next_of_kin, ":next_of_kin_contact_number" => $next_of_kin_contact_number, ":fcm_token" =>$fcm_token, ":device_id" => $device_id, ":device_tbl_id" => $device_tbl_id); 
    
		$insert_res=$sql_prepare->execute($response_array);
		if($insert_res){
			$ArrgetPatientDetails["responseCode"]=1;
			$ArrgetPatientDetails["message"]="Patient details save successfully";
		} else {
			$ArrgetPatientDetails["responseCode"]=0;
			$ArrgetPatientDetails["message"]="Patient details not inserted.";
		}
  	} else {
		$ArrgetPatientDetails["responseCode"] = 0;
		$ArrgetPatientDetails["message"] = $isvalid;
	}
   echo json_encode($ArrgetPatientDetails);
}

function fungetpersonaldetailsInfo($medicare){
	 global $DBCONN;
	 $isvalid = validateInput('getPersonalDetails');
 	 if($isvalid === true){ 
			$getMedicareQry="select * from tbl_patient where medicare_no='".$medicare."' order by patient_id DESC";
			$prepgetMedicareQry=$DBCONN->prepare($getMedicareQry);
			$prepgetMedicareQry->execute();
			$getResCnt	=$prepgetMedicareQry->rowCount();
			if($getResCnt>0)
			{
			    $getResRows	                    =   $prepgetMedicareQry->fetch();
				$address					    =	stripslashes(trim($getResRows["address"]));
				$suburb							=	stripslashes($getResRows["suburb"]);
				$post_code						=	stripslashes($getResRows["post_code"]);
				$home_phone						=	stripslashes($getResRows["home_phone"]); 	
				$mobile_phone					=	stripslashes($getResRows["mobile_phone"]);
				$pension_card_no				=	stripslashes($getResRows["pension_card_no"]);
				$pension_card_expiry_date		=	stripslashes($getResRows["pension_card_expiry_date"]);
				$is_keep					    =	stripslashes(trim($getResRows["is_keep"]));
                if($is_keep=="0"){
					$ArrgetMedicareDetails["responseCode"]        = 	1;
					$ArrgetMedicareDetails["message"]             =	"Success";
					$ArrMedicareDetails["patient_address"]	      =	$address;			
					$ArrMedicareDetails["patient_suburb"]	      =	$suburb;
					$ArrMedicareDetails["patient_postcode"]	      =	$post_code;
					$ArrMedicareDetails["patient_homephone"]	  =	$home_phone;
					$ArrMedicareDetails["patient_mobile"]	      =	$mobile_phone;
					$ArrMedicareDetails["patient_hcc"]	          =	$pension_card_no;
					$ArrMedicareDetails["hcc_ref_expiry"]	      =	$pension_card_expiry_date;
					$ArrgetMedicareDetails["patient_details"][]   = $ArrMedicareDetails;
				} else {

					$ArrgetMedicareDetails["responseCode"]=0;
				    $ArrgetMedicareDetails["message"]="No records found.";
				}
			} else {
				$ArrgetMedicareDetails["responseCode"]=0;
				$ArrgetMedicareDetails["message"]="No records found.";
		   }
	 } else{
		 
	    $ArrgetMedicareDetails["responseCode"]=0;
	    $ArrgetMedicareDetails["message"]=$isvalid;
	 }
		  
  //$json = preg_replace("#(/\*([^*]|[\r\n]|(\*+([^*/]|[\r\n])))*\*+/)|([\s\t]//.*)|(^//.*)#", '', $ArrgetMedicareDetails);
   echo json_encode($ArrgetMedicareDetails);
 
  
}


function validateInput($action){
			
	//****************
	/* @parameter: $action is string
	/* @return : true if validation is success or error message
	****************/
    
  $jsonParams = json_decode($_REQUEST['jsonParams'],true);
	$check_keys = array_keys($jsonParams);
	
	switch($action){
		
		case 'getPatientsList':
				$check_for = array('location');
			if(!empty($jsonParams['location'])){
				$rtnval = true;
			}else{
				
				$rtnval = "Invalid parameters supplied. Provide valid input ". implode(', ', array_diff($check_for, $check_keys));
			}
		break;
		case 'getPersonalDetails':
				$check_for = array('medicare');
			if(!empty($jsonParams['medicare'])){
				$rtnval = true;
			}else{
				$rtnval = "Invalid parameters supplied. Provide valid input ". implode(', ', array_diff($check_for, $check_keys));
			}
		break;
		case 'addPatients':
			
				$check_for = array('clinic_location','patient_title','family_name','given_names','birth_date','patient_medicareno','patient_referno','expiry_month_medi','preffered_doctor');
			if(!empty($jsonParams['clinic_location']) && !empty($jsonParams['patient_title']) && !empty($jsonParams['family_name']) && !empty($jsonParams['given_names']) && !empty($jsonParams['birth_date']) && !empty($jsonParams['patient_medicareno']) && !empty($jsonParams['patient_referno']) && !empty($jsonParams['expiry_month_medi'])&& ($jsonParams['preffered_doctor']!="")){

				$rtnval = true;
			}else{
				
				$rtnval = "Invalid parameters supplied. Provide valid input ". implode(', ', array_diff($check_for, $check_keys));
			}
		break;
		default:
			$rtnval = 'Invalid request';

	}

	return $rtnval;

}

function funlog($logmsg)
{
	$fp = fopen('log/walkingb_dob_'.date("d-m-Y").'_'.time().'.txt', 'a');
	fwrite($fp, $logmsg);
	fclose($fp);
}		
/*function funColorStatusCheck($patient_medicareno,$patient_address, $patient_suburb, $patient_postcode, $patient_mobile) {
	global $DBCONN;
	$getpatientQry="select * from tbl_patient where medicare_no='".$patient_medicareno."' order by patient_id desc limit 0,1";
	$prepgetPatientsQry=$DBCONN->prepare($getpatientQry);
	$prepgetPatientsQry->execute();
	$getResCnt	=$prepgetPatientsQry->rowCount();
	if ($getResCnt>0) {
	    $getPatientsRow	=  $prepgetPatientsQry->fetch();
	    $address  = strtolower($getPatientsRow["address"]);
		$suburb   = strtolower($getPatientsRow["suburb"]);
		$post_code = $getPatientsRow["post_code"];
		$mobile_phone = $getPatientsRow["mobile_phone"];
		if ($address == strtolower($patient_address) && $suburb == strtolower($patient_suburb) && $post_code == $patient_postcode && $mobile_phone == $patient_mobile) {
                $str="Matched";
		} else if ($address!= strtolower($patient_address) || $suburb != strtolower($patient_suburb) || $post_code != $patient_postcode || $mobile_phone != $patient_mobile) {
				$str="Not-matched";
		}
    } else {
    	$str="No";
    }
	return $str; 
}*/

function funadddevicedetails($request) {
	global $DBCONN;

	if (empty($request['device_id']) || empty($request['fcm_token'])) {
		$result['responseCode'] = 0;
		$result['message'] = 'Device id or FCM token is empty!';
		$result['Currenttime'] = date("Y-m-d H:i:s");
		funlog(json_encode($result)."\n");
		echo json_encode($result);
		exit;
	}

	funlog("Add device details \n");
	$getExistingDeviceQry = 'select * from tbl_device_details where device_id="'.$request['device_id'].'"';
	$prepExistingDeviceQry = $DBCONN->prepare($getExistingDeviceQry);
	$prepExistingDeviceQry->execute();
	$getResCnt = $prepExistingDeviceQry->rowCount();

	$result = array();
	if($getResCnt > 0) {
		$query = "update tbl_device_details set fcm_token = '".$request['fcm_token']."' where device_id = '".$request['device_id']."'";
		$exeQuery = $DBCONN->query($query);
	} else {
		$insertQry  = "insert into tbl_device_details(device_type, device_id, fcm_token) values(:device_type, :device_id, :fcm_token)";
		$sql_prepare = $DBCONN->prepare($insertQry);
		$query = array(":device_type"=>$request['type'], ":device_id"=>$request['device_id'], ":fcm_token" => $request['fcm_token']);
		$exeQuery = $sql_prepare->execute($query);
	}
	
	if ($exeQuery) {
		$result['responseCode'] = 1;
		$result['message'] = 'Success';
	} else {
		$result['responseCode'] = 0;
		$result['message'] = 'Unable to process the record.';
		$result['Currenttime'] = date("Y-m-d H:i:s");
	}
	funlog(json_encode($result)."\n");
	echo json_encode($result);
}

function fungetnotificationlist($fcm_token) {
	global $DBCONN;

	funlog($fcm_token."\n");
	$getNotificationListQry = "select * from tbl_notification_log where fcm_token = '".$fcm_token."' and status= 'success' and DATE(created_date) = CURDATE() and is_deleted = '0'";
	$prepNotificationListQry = $DBCONN->prepare($getNotificationListQry);
	$prepNotificationListQry->execute();
	$getResCnt = $prepNotificationListQry->rowCount();
	if($getResCnt > 0) {
		$notificationList['responseCode'] = 1;
		$notificationList['message'] = 'Success';
		$i = 0;
		foreach ($prepNotificationListQry->fetchAll(PDO::FETCH_ASSOC) as $key => $value) {
			$getNotificationList[$i]['id'] = stripslashes($value['notification_id']);
			$getNotificationList[$i]['patient_id'] = stripslashes($value['patient_id']);
			$getNotificationList[$i]['notification_title'] = stripslashes($value['notification_title'] ?? '');
			$getNotificationList[$i]['subject'] = stripslashes($value['subject'] ?? '');

    		$date = new DateTime($value['created_date'], new DateTimeZone('UTC'));
    		$timezone = new DateTimeZone('Asia/Singapore'); // GMT+8 timezone (Asia/Singapore)
			$date->setTimezone($timezone);
			$formattedDate = $date->format('Y-m-d H:i:s');

			$getNotificationList[$i]['created_date'] = $formattedDate;
			$getNotificationList[$i]['notification_content'] = stripslashes($value['description']);
			$i++;
		}
		$notificationList['notificationList'] = $getNotificationList;
	} else {
		$notificationList['responseCode'] = 0;
		$notificationList['message'] = 'No records found.';
		$notificationList['Currenttime'] = date("Y-m-d H:i:s");
	}
	funlog(json_encode($notificationList)."\n");
	echo json_encode($notificationList);
}

function funremovenotificationlist($fcm_token, $notification_id) {
	global $DBCONN;

	funlog("Delete-".$fcm_token."\n");
	$updateQuery = "update tbl_notification_log set is_deleted = '1' where fcm_token = '".$fcm_token."' and notification_id = '".$notification_id."'";
	$updateRes = $DBCONN->query($updateQuery);
	if($updateRes){ 
		funlog("Deleted-".$fcm_token."\n");
		$response['responseCode'] = 1;
		$response['message'] = 'Success';
		$response['Currenttime'] = date("Y-m-d H:i:s");
	} else {
		funlog("Delete Failed-".$fcm_token."\n");
		$response['responseCode'] = 0;
		$response['message'] = 'Failed';
		$response['Currenttime'] = date("Y-m-d H:i:s");
	}
	echo json_encode($response);
}
?>
