<?php
class Common
{
    public $dbconn;
    public function __construct(PDO $DBCONN) {
        $this->dbconn = $DBCONN;
    }

	/*Function to execute the SELECT queries*/
    public function funBckendExeSelectQuery($qry, $qryParams = array(), $fetchtype='')
    {        
        $prepareQry = $this->dbconn->prepare($qry);
        // Fetching all records
        if (count($qryParams)>0) {
            $execQry =  $prepareQry->execute($qryParams);
        } else { // Fetching one record
            $execQry = $prepareQry->execute();
        }
        $row_count = $prepareQry->rowCount();
        if ($row_count>0 && $fetchtype=='') {
			$fetch_data = $prepareQry->fetchAll(PDO::FETCH_ASSOC);
            return $fetch_data;
        } elseif ($row_count>0 && $fetchtype=='fetch') {
            $fetch_data = $prepareQry->fetch(PDO::FETCH_ASSOC);
            return $fetch_data;
        } else {
            return false;
        }
    }   

    /*Function to execute the Insert queries*/
    function funBckendExeInsertRecord($query,$insert_data)
    {
        $prepareInsertQry = $this->dbconn->prepare($query);
        $ExecPrepInsertQry = $prepareInsertQry->execute($insert_data);
        if ($ExecPrepInsertQry) {
            $last_insert_id = $this->dbconn->lastInsertId();
            return $last_insert_id;
        }
        return false;
    }

    /*Function to execute the update queries*/
    function funBckendExeUpdateRecord($qry, $qryParams)
    {
        $PrepUpdateQry =  $this->dbconn->prepare($qry);
        $ExecPrepUpdateQry = $PrepUpdateQry->execute($qryParams);
        if ($ExecPrepUpdateQry) {
            return true;
        }
        return false;
    }

    /*Function to execulte the SELECT single record queries*/
    public function funExeSelectQuery ($reqQryParams, $qryParams = array())
    {
        //create query
        $tableName = $reqQryParams["tableName"];
        $selectCluseCondtn = "*";
        $whereClauseCondtn = ""; 
        $orderByClauseCondtn = ""; 
        $groupByClauseCondtn = "";      

        if (!empty($reqQryParams["selectField"]))
            $selectCluseCondtn = $reqQryParams["selectField"];
        
        if (!empty($reqQryParams["whereCondition"]))
            $whereClauseCondtn = "WHERE ".$reqQryParams["whereCondition"]; 

        if (!empty($reqQryParams["orderByCondtn"]))
                $orderByClauseCondtn = "ORDER BY  ".$reqQryParams["orderByCondtn"];  

        if (!empty($reqQryParams["groupByCondition"]))
                $groupByClauseCondtn = "GROUP BY  ".$reqQryParams["groupByCondition"];         

        $qry = "SELECT $selectCluseCondtn FROM $tableName $whereClauseCondtn $groupByClauseCondtn $orderByClauseCondtn";

        $prepareQry = $this->dbconn->prepare($qry);
        if (!empty($qryParams) && count($qryParams) > 0) {
            $execQry = $prepareQry->execute($qryParams);
        } else {
            $execQry = $prepareQry->execute();
        }
        $rowCnt = $prepareQry->rowCount();
        $fetchData = array();
        if ($rowCnt > 0 ) {
            if (strtolower($reqQryParams["fetchType"]) == "singlerow")
                $fetchData = $prepareQry->fetch(PDO::FETCH_ASSOC);
            else
                $fetchData = $prepareQry->fetchAll(PDO::FETCH_ASSOC);
        }
        return $fetchData;
    }    

    /*Function to execulte the Insert queries*/
    function funExeInsertRecord($tblName, $qryParams = array())
    {
        if (!empty($qryParams) && count($qryParams) > 0) {
            // Remove ":" string from the key
            $tblFieldsArr = array();
            array_walk($qryParams, function (&$value,$key) use (&$tblFieldsArr) {
                $tblFieldsArr[ str_replace(":","",$key) ] = $value;
            });

            // retrieve the keys of the array (column titles)
            $tblFields = array_keys($tblFieldsArr);
            $tblValues = array_keys($qryParams);
            
            // Build the query
            $query = "INSERT INTO ".$tblName." (`".implode('`,`', $tblFields)."`) VALUES (".implode(",", $tblValues).")"; 
            $prepareInsertQry = $this->dbconn->prepare($query);
            $ExecPrepInsertQry = $prepareInsertQry->execute($qryParams);
            $lastInsertId = "";
            if ($ExecPrepInsertQry)
                $lastInsertId = $this->dbconn->lastInsertId();
            return $lastInsertId;
        }
    }

    /*Function to execulte the update queries*/
    function funExeUpdateRecord($reqQryParams, $qryParams = array())
    {

        $tableName = $reqQryParams["tableName"];
        $setClauseCondtn = "";
        $whereClauseCondtn = "";

        if (!empty($reqQryParams["setCondtn"]))
            $setClauseCondtn = "SET ".$reqQryParams["setCondtn"]; 
        
        if (!empty($reqQryParams["whereCondition"]))
            $whereClauseCondtn = "WHERE ".$reqQryParams["whereCondition"]; 

        if (!empty($reqQryParams)) {
            $qry = "UPDATE $tableName $setClauseCondtn $whereClauseCondtn";            
            $prepareQry = $this->dbconn->prepare($qry);
            if (!empty($qryParams) && count($qryParams) > 0) {
                $execQry = $prepareQry->execute($qryParams);
            } else {
                $execQry = $prepareQry->execute();
            }
            $rowCnt = $prepareQry->rowCount();            
            $updateRes = false;
            if ($rowCnt > 0 ) {
                $updateRes = true;
            }
            return $updateRes;
        }
    }
    
    /*Function to execulte the delete queries*/
    function funExeDeleteQuery($reqQryParams, $qryParams = array())
    {
        //create query
        $tableName = $reqQryParams["tableName"];
        $whereClauseCondtn = ""; 
        if (!empty($reqQryParams["whereCondition"])) {
            $whereClauseCondtn = "WHERE ".$reqQryParams["whereCondition"]; 
        }

        $qry = "DELETE FROM $tableName $whereClauseCondtn";
        $PrepDeleteQry = $this->dbconn->prepare($qry);
        $ExecPrepDeleteQry = $PrepDeleteQry->execute($qryParams);
        if ($ExecPrepDeleteQry) {
            return true;
        }
        return false;
    }

    // $field and $seperator are required otherwise both are empty
    public function funParseQryParams ($qryParams, $field = "", $seperator = "") {

        $setWhereCondtn = "";
        if (!empty($qryParams) && count($qryParams) > 1) {
            if (!empty($field) && !empty($seperator)) {
                $i = 1;
                foreach ($qryParams as $qryKey => $qryVal) {       
                    $qryKey = str_replace(":","" ,$qryKey); 
                    $setWhereCondtn .= $qryKey." = :".$qryKey;
                    if ($qryKey != $field)
                        $setWhereCondtn .= " $seperator ";
                    $i++;
                }
            } else {
                return "field and seperator required!";
            }
        } else {
            $i = 1;
            foreach ($qryParams as $qryKey => $qryVal) {       
                $qryKey = str_replace(":","" ,$qryKey); 
                $setWhereCondtn .= $qryKey." = :".$qryKey;
                $i++;
            }
        }
        return $setWhereCondtn;
    }

    /* Function to execulte the Leftjoin query */
    public function funExeTwoTblLeftJoinQuery($reqQryParams, $qryParams = array()) {

        if (is_array($reqQryParams) && !empty($reqQryParams)) {
            //create query
            $tbl1 = $reqQryParams["tables"][0];
            $tbl2 = $reqQryParams["tables"][1];
            $selectCluseCondtn = "*";
            $onClauseCondtn = "";
            $whereClauseCondtn = "";
            $orderByClauseCondtn = "";

            if (!empty($reqQryParams["selectField"]))
                $selectCluseCondtn = $reqQryParams["selectField"];

            if (!empty($reqQryParams["onCondition"]))
                $onClauseCondtn = "ON ".$reqQryParams["onCondition"];
            
            if (!empty($reqQryParams["whereCondition"]))
                $whereClauseCondtn = "WHERE ".$reqQryParams["whereCondition"]; 

            if (!empty($reqQryParams["orderByCondtn"]))
                $orderByClauseCondtn = "ORDER BY  ".$reqQryParams["orderByCondtn"];           

            $qry = "SELECT $selectCluseCondtn FROM $tbl1 LEFT JOIN $tbl2 $onClauseCondtn $whereClauseCondtn $orderByClauseCondtn";
            
            $prepareQry = $this->dbconn->prepare($qry);
            if (!empty($qryParams) && count($qryParams) > 0) {
                $execQry = $prepareQry->execute($qryParams);
            } else {
                $execQry = $prepareQry->execute();
            }
            $rowCnt = $prepareQry->rowCount();
            $fetchData = array();
            if ($rowCnt > 0 ) {
                if (strtolower($reqQryParams["fetchType"]) == "singlerow")
                    $fetchData = $prepareQry->fetch(PDO::FETCH_ASSOC);
                else
                    $fetchData = $prepareQry->fetchAll(PDO::FETCH_ASSOC);
            }
            return $fetchData;
        }
    }

    /* Function to execulte the Innerjoin query */
    public function funExeTwoTblInnerJoinQuery($reqQryParams, $qryParams = array()) {
        if (is_array($reqQryParams) && !empty($reqQryParams)) {
            //create query
            $tbl1 = $reqQryParams["tables"][0];
            $tbl2 = $reqQryParams["tables"][1];
            $selectCluseCondtn = "*";
            $onClauseCondtn = "";
            $whereClauseCondtn = "";
            $orderByClauseCondtn = "";
            if (!empty($reqQryParams["selectField"]))
                $selectCluseCondtn = $reqQryParams["selectField"];

            if (!empty($reqQryParams["onCondition"]))
                $onClauseCondtn = "ON ".$reqQryParams["onCondition"];
            
            if (!empty($reqQryParams["whereCondition"]))
                $whereClauseCondtn = "WHERE ".$reqQryParams["whereCondition"]; 

            if (!empty($reqQryParams["orderByCondtn"]))
                $orderByClauseCondtn = "ORDER BY  ".$reqQryParams["orderByCondtn"];
            $qry = "SELECT $selectCluseCondtn FROM $tbl1 JOIN $tbl2 $onClauseCondtn $whereClauseCondtn $orderByClauseCondtn";                        
            $prepareQry = $this->dbconn->prepare($qry);
            if (!empty($qryParams) && count($qryParams) > 0) {
                $execQry = $prepareQry->execute($qryParams);
            } else {
                $execQry = $prepareQry->execute();
            }
            $rowCnt = $prepareQry->rowCount();
            $fetchData = array();
            if ($rowCnt > 0 ) {
                if (strtolower($reqQryParams["fetchType"]) == "singlerow")
                    $fetchData = $prepareQry->fetch(PDO::FETCH_ASSOC);
                else
                    $fetchData = $prepareQry->fetchAll(PDO::FETCH_ASSOC);
            }
            return $fetchData;
        }
    }

    /*Function return JSON results from given Array*/
    public function jsonResponse($jsonEncodeArr){
        if (is_array($jsonEncodeArr)) 
            $responseJson = json_encode($jsonEncodeArr,JSON_UNESCAPED_UNICODE);
        return $responseJson;
    }

    public function jsonRespons($jsonEncodeArr){
        $responseJson = json_encode($jsonEncodeArr,true);
        return $responseJson;
    }

    // Convert base64image into image and move to assigned path
    public function base64toImage($base64image,$floderName) {
        if (!empty($base64image)) {
            $uploadDoc = "";
            if (!empty($base64image)) {
                $pos  = strpos($base64image, ';');
                if (!empty($pos)) {
                    $mimeType = explode(':', substr($base64image, 0, $pos))[1];
                    $imgData = str_replace('data:image/'.$mimeType.';base64,', '', $base64image);
                } else {
                    $imgData = str_replace('data:image/png;base64,', '', $base64image);
                }
                $imgData = str_replace(' ', '+', $imgData);
                $imgData = base64_decode($imgData);
                $randNumber = sha1($this->generateRandAlphanumeric(24));
                $genRandFilename = '../uploads/'.$floderName."/".$randNumber. '.png';                    
                $moveImgpath = file_put_contents($genRandFilename, $imgData);
                if ($moveImgpath)
                    $uploadDoc = 'uploads/'.$floderName."/".$randNumber. '.png';
            }
            return $uploadDoc;
        }
    }

    // Convert base64documents into documents and move to assigned path
    public function base64toDocs($base64docs,$floderName) {        
        if (!empty($base64docs)) {
            $uploadDoc = "";
            $type = pathinfo(base64_decode($base64docs), PATHINFO_EXTENSION);
            $imgData = base64_decode($base64docs);
            $randNumber = sha1($this->generateRandAlphanumeric(24));
            $genRandFilename = '../uploads/'.$floderName."/".$randNumber.".".$type;
            if(copy($imgData,$genRandFilename))
                $uploadDoc = 'uploads/'.$floderName."/".$randNumber.".".$type;            
            return $uploadDoc;
        }
    }

    public function generateRandAlphanumeric() {
        // return sprintf( '%04x%04x-%04x-%04x-%04x-%04x%04x%04x',
        return rand(10,100).sprintf( '%04x%04x',
            mt_rand( 0, 0xffff ), mt_rand( 0, 0xffff ),
            mt_rand( 0, 0xffff ),
            mt_rand( 0, 0x0C2f ) | 0x4000,
            mt_rand( 0, 0x3fff ) | 0x8000,
            mt_rand( 0, 0x2Aff ), mt_rand( 0, 0xffD3 ), mt_rand( 0, 0xff4B )
        ).rand(10,100);
    }

    public function generateVerficationCode($digits) {
       return $verfication_code=rand(pow(10, $digits-1), pow(10, $digits)-1);
    }

    /*Encryption and Decryption Password function start Here*/
    var $skey = "a2c4e6g8i0a2c4e6g8i02017"; // change this
    public function safe_b64encode($string) {
        $data = base64_encode($string);
        $data = str_replace(array('+','/','='),array('-','_',''),$data);
        return $data;
    }
    
    public function safe_b64decode($string) {
        $data = str_replace(array('-','_'),array('+','/'),$string);
        $mod4 = strlen($data) % 4;
        if ($mod4) {
            $data .= substr('====', $mod4);
        }
        return base64_decode($data);
    }
 
    public function encode($value){ 
        if (!$value) { return false; }
        $text = $value;
        $iv_size = mcrypt_get_iv_size(MCRYPT_RIJNDAEL_256, MCRYPT_MODE_ECB);
        $iv = mcrypt_create_iv($iv_size, MCRYPT_RAND);
        $crypttext = mcrypt_encrypt(MCRYPT_RIJNDAEL_256, $this->skey, $text, MCRYPT_MODE_ECB, $iv);
        return trim($this->safe_b64encode($crypttext)); 
    }
 
    public function decode($value){
        if (!$value) { return false; }
        $crypttext = $this->safe_b64decode($value); 
        $iv_size = mcrypt_get_iv_size(MCRYPT_RIJNDAEL_256, MCRYPT_MODE_ECB);
        $iv = mcrypt_create_iv($iv_size, MCRYPT_RAND);
        $decrypttext = mcrypt_decrypt(MCRYPT_RIJNDAEL_256, $this->skey, $crypttext, MCRYPT_MODE_ECB, $iv);
        return trim($decrypttext);
    }

    public function funlog($logmsg)
    {
        $fp = fopen('log/viop_call'.date("d-m-Y").'.txt', 'a');
        fwrite($fp, $logmsg);
        fclose($fp);
    }  

}
?>