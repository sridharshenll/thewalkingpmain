<?php
require_once 'Recaptcha/autoload.php';
$msg='';
if(isset($_POST["patient_title"])){
	/*if(isset($_POST['g-recaptcha-response']) && !empty($_POST['g-recaptcha-response'])){
		$secret = '6LdfbycTAAAAAFgg7waFvRphwrBZb_zNae5d-MNi';
		$recaptcha = new \ReCaptcha\ReCaptcha($secret);
		$resp = $recaptcha->verify($_POST['g-recaptcha-response'], $_SERVER['REMOTE_ADDR']);		
	    //get verify response data	   
	    if($resp->isSuccess()){*/
			$clinic_location = $_POST["clinic_location"];
			$getQry = "select * from tbl_settings where location = '".$clinic_location."'";
			$getRes = $DBCONN->query($getQry);
			$getRow = $getRes->fetch(PDO::FETCH_ASSOC);
			if($getRow['queue_system'] == 0) {

				$clinic_confirmation = $_POST["clinic_confirmation"];
				$patient_title = addslashes(trim($_POST["patient_title"]));
				$family_name = addslashes(trim($_POST["family_name"]));
				$given_names = addslashes(trim($_POST["given_names"]));

				if($_POST["birth_month"] != "" && $_POST["birth_date"] != "" && $_POST["birth_year"] != "")
					$patient_dob = $_POST["birth_year"]."-".$_POST["birth_month"]."-".$_POST["birth_date"];
				else
					$patient_dob = "";


			    $patient_medicareno	=	addslashes(trim($_POST["patient_medicareno"]));
				$patient_referno	=	addslashes(trim($_POST["patient_referno"]));
				$patient_ref_expiry	=	$_POST["expiry_month_medi"]."-".$_POST["expiry_year_medi"];

				$preffered_doctor	=	$_POST["preffered_doctor"];
				$patient_address	=	addslashes(trim($_POST["patient_address"]));
				$patient_suburb		=	addslashes(trim($_POST["patient_suburb"]));
				$patient_postcode	=	addslashes(trim($_POST["patient_postcode"]));
				$patient_homephone	=	addslashes(trim($_POST["patient_homephone"]));
				$patient_mobile		=	addslashes(trim($_POST["patient_mobile"]));
				$patient_hcc		=	addslashes(trim($_POST["patient_hcc"]));
				$email_address		=	addslashes(trim($_POST["email_address"]));
				$next_of_kin		=	addslashes(trim($_POST["next_of_kin"]));
				$next_of_kin_contact_number =	addslashes(trim($_POST["next_of_kin_contact_number"]));
				//$hcc_ref_expiry		=   $_POST["hcc_ref_expiry"];

				if($_POST["hcc_ref_expiry_month"] != "" && $_POST["hcc_ref_expiry_date"] != "" && $_POST["hcc_ref_expiry_year"] != "")
				{
					$hcc_ref_expiry = $_POST["hcc_ref_expiry_year"]."-".$_POST["hcc_ref_expiry_month"]."-".$_POST["hcc_ref_expiry_date"];
				}
				else
				{
					$hcc_ref_expiry = "";
				}

                //Function status medicare number using dot color added in Red,Blue,Orange start here
                if (strtolower($clinic_confirmation) == "yes") {
					$Checkstatus = funColorStatusCheck(strtolower($patient_title), stripcslashes(strtolower($family_name)), stripcslashes(strtolower($given_names)), $patient_dob, $patient_medicareno, $patient_referno, $patient_ref_expiry, stripcslashes(strtolower($patient_address)), strtolower($patient_suburb), $patient_postcode, $patient_homephone, $patient_mobile, $patient_hcc, $hcc_ref_expiry);

					if (strtolower($Checkstatus) == "matched") {
						$colorstatus = "#08559C";//blue color  
					} else if (strtolower($Checkstatus) == "not-matched") {
						$colorstatus = "#FF4500";//orange color
                	} else {
						$colorstatus = "#FF4500";//orange color
                	}
                } else {
					$colorstatus = "#a01414";//red color
                }

                $device_type = "Website";

                //Function status medicare number using dot color added in Red,Blue,Orange end here

				$getTknQry = "select count(*) as token_cnt from tbl_patient where register_date='".date('Y-m-d')."' and location='".$clinic_location."'";
				$getTknRes = $DBCONN->query($getTknQry);
				$getTknRow = $getTknRes->fetch(PDO::FETCH_ASSOC);
				$patient_token_number = $getTknRow["token_cnt"]+1;

				$getPosition = "select max(display_order) as displayrow from tbl_patient where location='".$clinic_location."'";
				$getPosRes = $DBCONN->query($getPosition);
				$getDisplayRow = $getPosRes->fetch(PDO::FETCH_ASSOC);
				$DisplayRow = $getDisplayRow["displayrow"]+1;


				$insertQry="insert into tbl_patient(doctor_id,patient_name,location,location_option,title,family_name,dob,medicare_no,reference_no,expiry_date,address,suburb,post_code,home_phone,mobile_phone,pension_card_no,pension_card_expiry_date,reg_time,token_number,register_date,patient_status,created_date,modified_date,display_order,color_status,device_type,email_address,next_of_kin,next_of_kin_contact_number) values('".$preffered_doctor."','".ucwords($given_names)."','".$clinic_location."','".$clinic_confirmation."','".$patient_title."','".ucwords($family_name)."','".$patient_dob."','".$patient_medicareno."','".$patient_referno."','".$patient_ref_expiry."','".ucwords($patient_address)."','".ucwords($patient_suburb)."','".$patient_postcode."','".$patient_homephone."','".$patient_mobile."','".$patient_hcc."','".$hcc_ref_expiry."','".date('Y-m-d H:i:s')."','".$patient_token_number."','".date('Y-m-d')."','Appointment fixed','".date('Y-m-d H:i:s')."','".date('Y-m-d H:i:s')."','".$DisplayRow."','".$colorstatus."','".$device_type."','".$email_address."','".$next_of_kin."','".$next_of_kin_contact_number."')";
				
				$insertRes = $DBCONN->query($insertQry);
				
				if($insertRes) {
					header("Location:patients_list.php?frm=appt&loc=".$clinic_location);
					exit;
				}
			} else {
				$msg="Patient registration is not available at this location (".$clinic_location."). Please try again later";
			}
		/*}else{
			$msg="Captcha incorrect, Please check the captcha";
		}
	}else{
		$msg="Please enter the patient details";
	}*/
}

function funColorStatusCheck($patient_title, $family_name, $given_names, $patient_dob, $patient_medicareno, $patient_referno, $patient_ref_expiry, $patient_address, $patient_suburb, $patient_postcode, $patient_homephone, $patient_mobile, $patient_hcc, $hcc_ref_expiry) {
	/*$getpatientQry="select * from tbl_patient where medicare_no='".$patient_medicareno."' order by patient_id desc limit 0,1";
	$getpatientRes=$DBCONN->query($getpatientQry);
	$getPatientsCnt=mysql_num_rows($getpatientRes);
	if($getPatientsCnt>0) {
		$i=1;
		while($getPatientsRow=mysql_fetch_array($getpatientRes)) {
            $title  = strtolower($getPatientsRow["title"]);
			$familyname  = stripcslashes(strtolower($getPatientsRow["family_name"]));
            $patient_name  = stripcslashes(strtolower($getPatientsRow["patient_name"]));
            $dob  = trim($getPatientsRow["dob"]);
            $reference_no = $getPatientsRow["reference_no"];
            $address  = stripcslashes(strtolower($getPatientsRow["address"]));
			$suburb   = stripcslashes(strtolower($getPatientsRow["suburb"]));
			$post_code = $getPatientsRow["post_code"];
			$expiry_date = $getPatientsRow["expiry_date"];
		    $mobile_phone = $getPatientsRow["mobile_phone"];
			$home_phone  = $getPatientsRow["home_phone"];
            $pension_card_no  = $getPatientsRow["pension_card_no"];
            $pension_card_expiry_date  = trim($getPatientsRow["pension_card_expiry_date"]);
			if (($title ==$patient_title) && (stripcslashes($familyname)==$family_name) && (stripcslashes($patient_name) == $given_names) && ($dob == trim($patient_dob)) && ($reference_no ==  $patient_referno) && ($expiry_date == $patient_ref_expiry) && empty($patient_address) && empty($patient_suburb) && empty($patient_postcode) && empty($patient_homephone) && empty($patient_mobile) && empty($patient_hcc) && empty($hcc_ref_expiry)) {
				$str="Matched";
			} else {
				$str="Not-matched";
			}
			$i++;
		}
	} else {*/
	    if (!empty($patient_title) && !empty($family_name) && !empty($given_names) && !empty($patient_dob) && !empty($patient_medicareno) && !empty($patient_referno) && !empty($patient_ref_expiry) &&  empty($patient_address) && empty($patient_suburb) && empty($patient_postcode) && empty($patient_homephone) && empty($patient_mobile) && empty($patient_hcc) && empty($hcc_ref_expiry)) {
	       $str = "Matched";
	    } else{
	    	$str = "Not-matched";
	    }
    /*}*/
    return $str;
}
?>

