<?php
include("../includes/configure.php");
include("includes/session_check.php");
if(isset($_POST["hidden_id"])) {
	$hidden_id = addslashes(trim($_POST["hidden_id"]));
	$hidden_prev_status = addslashes(trim($_POST["hidden_prev_status"]));
	$hidden_status = addslashes(trim($_POST["hidden_status"]));
	$hidden_token = addslashes(trim($_POST["hidden_token"]));
	if($hidden_status != $hidden_prev_status && $hidden_status == "Apointment fixed") {
		$getTknQry = "select count(*) as token_cnt from tbl_patient where register_date='".date('Y-m-d')."' and location='".$_SESSION["location"]."'";
		$getTknRes = $DBCONN->query($getTknQry);
		$getTknRow = $getTknRes->fetch(PDO::FETCH_ASSOC);
		$patient_token_number = $getTknRow["token_cnt"] + 1;
	} else{
		$patient_token_number = $hidden_token;
	}
	$updateQry = "update tbl_patient set patient_status='".$hidden_status."',token_number='".$patient_token_number."',modified_date='$dbdatetime' where patient_id='".$hidden_id."'";
		$updateRes = $DBCONN->query($updateQry);
		if($updateRes){
			header("Location:patient_queue_screen.php");
			exit;
		}
}
include("includes/header.php");
?>

<!-- Pickers -->
<script type="text/javascript" src="../plugins/pickadate/picker.js"></script>
<script type="text/javascript" src="../plugins/pickadate/picker.date.js"></script>
<script type="text/javascript" src="../plugins/pickadate/picker.time.js"></script>
<script type="text/javascript" src="../plugins/bootstrap-colorpicker/bootstrap-colorpicker.min.js"></script>

<script type="text/javascript" src="../plugins/fileinput/fileinput.js"></script>
<script type="text/javascript" src="../plugins/bootstrap-wysihtml5/wysihtml5.min.js"></script>
<script type="text/javascript" src="../plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.min.js"></script>

<!-- Form Validation -->
<script type="text/javascript" src="../plugins/validation/jquery.validate.min.js"></script>
<script type="text/javascript" src="../plugins/validation/additional-methods.min.js"></script>
<!-- Noty -->
<script type="text/javascript" src="../plugins/noty/jquery.noty.js"></script>
<script type="text/javascript" src="../plugins/noty/layouts/top.js"></script>
<script type="text/javascript" src="../plugins/noty/themes/default.js"></script>

<!-- Demo JS -->
<script type="text/javascript" src="../assets/js/custom.js"></script>
<script type="text/javascript" src="../assets/js/demo/form_validation.js"></script>
<script type="text/javascript" src="../assets/js/demo/ui_general.js"></script>
<style type="text/css">
	.widget.box .widget-content {
		padding:0px;
		padding-top: 10px !important;
		padding-bottom: 10px !important;
		position: relative;
		background-color: #fff;
	}
</style>
		<!-- Center Main page Content -->
		<div id="content">
			<div class="container">
				<!--=== Page Header ===-->
				<div class="page-header">
					<div class="page-title">
					<!-- <h3>Patients Queue</h3> -->
					</div>		
				</div>
				<!-- /Page Header -->

				
				<!--=== Responsive DataTable ===-->
				<div class="row" >
					<div class="col-md-12">
						<div class="row">
							<div class="col-md-12">
									<div style="padding-top:5px;padding-bottom:5px;float:right;"><h5>Date:<?php echo date('d/m/Y');?></h5>
									</div>
							</div>
					</div>
						<div class="widget box">
							<div class="widget-header">
								<h4><i class="icon-reorder"></i>Patients Queue</h4>
                                  <!--  <input type="button" class="btn btn-primary pull-right" onclick="funexport();" value="Export">  -->
								
							</div>
												
							<div class="widget-content">
							<form name="patient_form" id="patient_form" method="post">
							<INPUT TYPE="hidden" NAME="hidden_id" id="hidden_id" >
							<INPUT TYPE="hidden" NAME="hidden_prev_status" id="hidden_prev_status">
							<INPUT TYPE="hidden" NAME="hidden_status" id="hidden_status">
							<INPUT TYPE="hidden" NAME="hidden_token" id="hidden_token">
							<table class="table table-striped table-bordered table-hover table-checkable datatable">
									<thead>
										<tr>
											<th>No</th>
											<th>Doctor</th>
											<th>Patient</th>
											<th>Date of Birth</th>
											<th>Full Details</th>
											<th>Time (waiting since)</th>
											<th width="30%" class="hidden-xs visible-md visible-sm visible-lg">Status</th>
											<th width="30%" class="visible-xs hidden-md hidden-sm hidden-lg">Status</th>
											
										</tr>
									</thead>
									 <tbody>
									 <?php
									 $getPatientsQry="select * from tbl_patient where location='".$_SESSION["location"]."' and register_date='".date('Y-m-d')."' and patient_status='Apointment fixed' ORDER BY reg_time asc";
									
									 $getPatientsRes=$DBCONN->query($getPatientsQry);
									 $getPatientsCnt=$getPatientsRes->rowCount();
									 if($getPatientsCnt>0){
										 $i=1;
										 foreach($getPatientsRes->fetchAll(PDO::FETCH_ASSOC) as $getPatientsRow){
											 $doctor_id=$getPatientsRow["doctor_id"];
											 $getDocQry="select * from tbl_staff where staff_id='".$doctor_id."'";
											 $getDocRes=$DBCONN->query($getDocQry);
											 $getDocRow=$getDocRes->fetch(PDO::FETCH_ASSOC);
											 $doctor_name=stripslashes($getDocRow["staff_name"]);
											 $Patient_status=stripslashes($getPatientsRow["patient_status"]);
											
									 ?>
										<tr>
                                             <td><?php echo $i;?></td>
											<td><?php echo $doctor_name;?></td>
											<td><?php echo stripslashes($getPatientsRow["patient_name"]).' '.stripslashes($getPatientsRow["family_name"]);?></td>
											<td><?php echo date('d/M/Y', strtotime($getPatientsRow["dob"]));?></td>
											<td><a href="edit_patient.php?redirec=pq&staff_id=<?php echo $getPatientsRow["patient_id"];?>">View Details</a></td>
											<td><?php echo date('g:i:s A',strtotime($getPatientsRow["reg_time"]));?></td>	
											<!-- <td><div >
												     <select  id="patient_<?php echo $i;?>" class="select full-width-fix" onchange="getvalue('<?php echo $getPatientsRow["patient_id"];?>','<?php echo $Patient_status;?>',this.value,'<?php echo $getPatientsRow["token_number"];?>')" style="border:1px solid gray;">
																
																<option value="Apointment fixed">Apointment fixed</option>
																<option value="Visited">Visited</option>
																<option value="Canceled">Cancelled</option>
																
															</select>
													</div>
													<SCRIPT LANGUAGE="JavaScript">
                                                      $('#patient_<?php echo $i;?>').val("<?php echo $Patient_status;?>");
													  
													</SCRIPT>
											
											</td>	 -->										
											<td class="hidden-xs visible-md visible-sm visible-lg"><div >
												     <select  id="patient_bg<?php echo $i;?>" class="select full-width-fix" onchange="getvalue('<?php echo $getPatientsRow["patient_id"];?>','<?php echo $Patient_status;?>',this.value,'<?php echo $getPatientsRow["token_number"];?>')">
																<option value="">Select</option>
																<option value="Visited">Visited</option>
																<option value="Canceled">Cancelled</option>
																
															</select>
													</div>
													<SCRIPT LANGUAGE="JavaScript">
                                                      $('#patient_bg<?php echo $i;?>').val("<?php echo $Patient_status;?>");
													  
													</SCRIPT>
											
											</td>
											<td class="visible-xs hidden-md hidden-sm hidden-lg"><div >
												     <select  id="patient_<?php echo $i;?>" class="select full-width-fix" onchange="getvalue('<?php echo $getPatientsRow["patient_id"];?>','<?php echo $Patient_status;?>',this.value,'<?php echo $getPatientsRow["token_number"];?>')">
																
																<option value="Apointment fixed">Fix</option>
																<option value="Visited">Vis</option>
																<option value="Canceled">Can</option>
																
															</select>
													</div>
													<SCRIPT LANGUAGE="JavaScript">
                                                      $('#patient_<?php echo $i;?>').val("<?php echo $Patient_status;?>");
													  
													</SCRIPT>
											
											</td>
											
										</tr>		
											
										<?php
											 $i++;
										 }
										
									    }else{

										?>
										<tr>
											<td colspan="7"><center>No patients Found.</center></td>
										</tr>
										<?php
										}
										?>
									</tbody>
								</table>
								</form>
								
							</div>
						</div>
					</div>
					<!-- /Table with Footer -->
							
						</div>
					</div>
				</div>
				<!-- /Responsive DataTable -->
				<!-- /Page Content -->
			</div>
			<!-- /.container -->

		</div>

		<!-- /Center Main page Content -->
<?php
include("includes/footer.php");
?>
<SCRIPT LANGUAGE="JavaScript">
function getvalue(id,prev_value,status,token){
$("#hidden_id").val(id);
$('#hidden_prev_status').val(prev_value);
$('#hidden_status').val(status);
$('#hidden_token').val(token);
document.patient_form.submit();
}

function funexport()
{
 try
 {
  with(document.patient_form)
  {
	 
   action='patient_list-export.php';
   submit();
   return true;
   action='';
  }
 }
 catch(e)
 {
  alert(e)
 }
}
</SCRIPT>