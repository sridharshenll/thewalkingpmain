<?php
include('../includes/configure.php');
include("includes/session_check.php");
$Patient_id = $_GET['staff_id'];

if($Patient_id != ""){
	$Dob = "";
	$Pension_card_expiry_date = "";
	$getpatientQry = "select * from tbl_patient where patient_id='".$Patient_id."'";
	$getpatientRes = $DBCONN->query($getpatientQry);
	$getpatientRow = $getpatientRes->fetch(PDO::FETCH_ASSOC);
	$Doctor_id = stripslashes($getpatientRow["doctor_id"]);
	$Patient_name = stripslashes($getpatientRow["patient_name"]);
	$Title = stripslashes(trim($getpatientRow["title"]));
	$Family_name = stripslashes($getpatientRow["family_name"]);
	$Medicare_no = stripslashes($getpatientRow["medicare_no"]);
	$Reference_no = stripslashes($getpatientRow["reference_no"]);
	if($getpatientRow["dob"] !=  "") {
		list($year,$month,$day) = explode("-",$getpatientRow["dob"]);
	if(strlen($day) == 1)
		$day = "0".$day;
	}

	$Expiry_date = stripslashes($getpatientRow["expiry_date"]);
	
	if($Expiry_date != "")
	{
		if(strpos($Expiry_date,"-") !== false)
			list($expmonth,$expyear)=explode("-",$Expiry_date);
		else
			list($expmonth,$expyear)=explode("/",$Expiry_date);
	}
		
	
	$Pension_card_expiry_date = stripslashes($getpatientRow["pension_card_expiry_date"]);

	if(stripslashes($getpatientRow["pension_card_expiry_date"]) != "")
		list($hccyear,$hccmonth,$hccday)=explode("-",$getpatientRow["pension_card_expiry_date"]);

	if(strlen($hccday) == 1)
		$hccday = "0".$hccday;


	$Address = stripslashes($getpatientRow["address"]);
	$Suburb = $getpatientRow["suburb"];
	$Post_code = $getpatientRow["post_code"];
	$Home_phone = stripslashes($getpatientRow["home_phone"]);
	$Mobile_phone = stripslashes($getpatientRow["mobile_phone"]);
	$Pension_card_no = $getpatientRow["pension_card_no"];
	$Token_number = stripslashes($getpatientRow["token_number"]);

	
}
if(isset($_POST["patient_title"])){
	//$clinic_location	=	$_POST["clinic_location"];
	$clinic_confirmation=	$_POST["clinic_confirmation"];
	$patient_title		=	addslashes(trim($_POST["patient_title"]));
	$family_name		=	addslashes(trim($_POST["family_name"]));
	$given_names		=	addslashes(trim($_POST["given_names"]));
//	if($_POST["patient_dob"] != ""){
//	  list($day,$month,$year)=explode("/",$_POST["patient_dob"]);
//		$patient_dob=$year."-".$month."-".$day;;
//	}
//	else{
//		$patient_dob		="";
//	}

	if($_POST["birth_month"] != "" && $_POST["birth_date"] != "" && $_POST["birth_year"]  != "")
		$patient_dob = $_POST["birth_year"]."-".$_POST["birth_month"]."-".$_POST["birth_date"];
	else
		$patient_dob		="";


	//$patient_ref_expiry	=	addslashes(trim($_POST["patient_ref_expiry"]));

	$patient_ref_expiry	=	$_POST["expiry_month_medi"]."/".$_POST["expiry_year_medi"];

	$patient_medicareno	=	addslashes(trim($_POST["patient_medicareno"]));
	$patient_referno	=	addslashes(trim($_POST["patient_referno"]));
	$preffered_doctor	=	$_POST["preffered_doctor"];
	$patient_address	=	addslashes(trim($_POST["patient_address"]));
	$patient_suburb		=	addslashes(trim($_POST["patient_suburb"]));
	$patient_postcode	=	addslashes(trim($_POST["patient_postcode"]));
	$patient_homephone	=	addslashes(trim($_POST["patient_homephone"]));
	$patient_mobile		=	addslashes(trim($_POST["patient_mobile"]));
	$patient_hcc		=	addslashes(trim($_POST["patient_hcc"]));
	//$hcc_ref_expiry		=   $_POST["hcc_ref_expiry"];

	if($_POST["hcc_ref_expiry_month"] != "" && $_POST["hcc_ref_expiry_date"] != "" && $_POST["hcc_ref_expiry_year"]  != "")
		$hcc_ref_expiry = $_POST["hcc_ref_expiry_year"]."-".$_POST["hcc_ref_expiry_month"]."-".$_POST["hcc_ref_expiry_date"];
	else
		$hcc_ref_expiry		="";

	$updateQry="update tbl_patient set 
	doctor_id='".$preffered_doctor."',patient_name='".ucwords($given_names)."',location='".$_SESSION["location"]."',title='".$patient_title."',family_name='".ucwords($family_name)."',dob='".$patient_dob."',medicare_no='".$patient_medicareno."',reference_no='".$patient_referno."',expiry_date='".$patient_ref_expiry."',address='".ucwords($patient_address)."',suburb='".ucwords($patient_suburb)."',post_code='".$patient_postcode."',home_phone='".$patient_homephone."',mobile_phone='".$patient_mobile."',pension_card_no='".$patient_hcc."',pension_card_expiry_date='".$hcc_ref_expiry."',modified_date='$dbdatetime' where patient_id='".$Patient_id."'";
	$updateRes = $DBCONN->query($updateQry);
	if($updateRes){
		if(!isset($_GET['redirec']) && $_GET['redirec'] != 'pq') {
			header("Location:manage_patients.php?msg=2");
			exit;
		} elseif(isset($_GET['redirec']) && $_GET['redirec']=='pq') {
			header("Location:patient_queue_screen.php?msg=2");
			exit;
		}
	}
}
include('includes/header.php');
?>
<!-- Pickers -->
<script type="text/javascript" src="../plugins/pickadate/picker.js"></script>
<script type="text/javascript" src="../plugins/pickadate/picker.date.js"></script>
<script type="text/javascript" src="../plugins/pickadate/picker.time.js"></script>
<script type="text/javascript" src="../plugins/bootstrap-colorpicker/bootstrap-colorpicker.min.js"></script>
<script type="text/javascript" src="../plugins/fileinput/fileinput.js"></script>
<script type="text/javascript" src="../plugins/bootstrap-wysihtml5/wysihtml5.min.js"></script>
<script type="text/javascript" src="../plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.min.js"></script>
<!-- Form Validation -->
<script type="text/javascript" src="../plugins/validation/jquery.validate.min.js"></script>
<script type="text/javascript" src="../plugins/validation/additional-methods.min.js"></script>
<!-- Noty -->
<script type="text/javascript" src="../plugins/noty/jquery.noty.js"></script>
<script type="text/javascript" src="../plugins/noty/layouts/top.js"></script>
<script type="text/javascript" src="../plugins/noty/themes/default.js"></script>
<!-- Demo JS -->
<script type="text/javascript" src="../assets/js/custom.js"></script>
<!-- <script type="text/javascript" src="../assets/js/demo/form_validation.js"></script> -->
<script type="text/javascript" src="../assets/js/demo/ui_general.js"></script>
<div id="content">
			<div class="container">				
				<!--=== Page Header ===-->
				<div class="page-header" style="margin-top:40px;">
					<div class="page-title">
						<!-- <h3>Patient Registration Form</h3>
						<span>Please enroll the patients using the below form. </span> -->
					</div>					
				</div>
				<!-- /Page Header -->

				<?php if($msg != ''){
				?>
				<div class="alert fade in alert-success">
					<i class="icon-remove close" data-dismiss="alert"></i>
					<?php echo $Message; ?>
				</div>					
				<?php }
				?>

				<!--=== Page Content ===-->
				<!--=== Full Size Inputs ===-->
				<div class="row">
					<div class="col-md-12">
						<div class="widget box">
							<div class="widget-header">
								<h4><i class="icon-reorder"></i>Patient Registration Form</h4>
								
							</div>
							<div class="widget-content">
								<form class="form-horizontal row-border" method='POST' id="validate-1" action="">									
										<div class="form-group">
														<label class="control-label col-md-3">Title: <span class="required">*</span></label>
														<div class="col-md-4">
															<select name="patient_title" id="patient_title" class="full-width-fix required" data-msg-required="Please select title.">
																<option value="">Select Title</option>
																<option value="Master">Master</option>
																<option value="Mr">Mr</option>
																<option value="Mrs">Mrs</option>
																<option value="Ms">Ms</option>
																<option value="Miss">Miss</option>
															</select>
															<script language="javascript">document.getElementById("patient_title").value="<?php echo $Title ?? ''?>";</script>
														</div>
													</div>
													<div class="form-group">
														<label class="control-label col-md-3">Family Name: <span class="required">*</span></label>
														<div class="col-md-4">
															<input type="text" class="form-control required letterswithbasicpunc" name="family_name" id="family_name"/ value="<?php echo $Family_name ?? '';?>" data-msg-required="Please enter family name.">
															<span class="help-block">(as printed on your medicare card).</span>
														</div>
													</div>
													<div class="form-group">
														<label class="control-label col-md-3">Given Name: <span class="required">*</span></label>
														<div class="col-md-4">
															<input type="text" class="form-control required letterswithbasicpunc" name="given_names" id="given_names"/  value="<?php echo $Patient_name ?? '';?>" data-msg-required="Please enter given name.">
														<span class="help-block">(as printed on your medicare card).</span>
														</div>
													</div>
													<div class="form-group">
														<label class="control-label col-md-3">Date of Birth: <span class="required">*</span></label>
														<div class="col-md-4" style="padding-left:0px;">
															<!-- <input type="text" class="form-control datepicker required" name="patient_dob" id="patient_dob" value="<?php echo $Dob ?? '';?>" data-msg-required="Please enter date of birth." readonly> -->
															<!-- <span class="help-block">Enter date of birth.</span> -->
															<div class="col-md-4" style='padding-bottom:5px'>
															<select name="birth_date" id="birth_date" class="form-control full-width-fix" data-msg-required="Please select date.">
																	<option value="">Date</option>
																	<?php
																		for($d = 1; $d <= 31; $d++) {
																			if(strlen($d) == 1)
																			$d = "0".$d;
																			echo "<option value='".$d."'>".$d."</option>";	
																		}
																		?>
																	</select>
																	<script language="javascript">document.getElementById("birth_date").value="<?php echo $day ?? '';?>"</script>
																<!-- <input type="text" class="form-control datepicker1 required" name="patient_ref_expiry" id="patient_ref_expiry" readonly data-msg-required="Please select expiry date."/> -->
																	
																</div>
																<div class="col-md-4"  style='padding-bottom:5px'>
																	<select name="birth_month" id="birth_month" class="form-control full-width-fix " data-msg-required="Please select month.">	
																	<option value="">Month</option>
																		<?php
																		for($i = 1; $i <= 12; $i++) {
																				if(strlen($i) == 1)
																					$i = "0".$i;
																			echo "<option value='".$i."'>".$i."</option>";
																		}
																		?>
																	</select>
																	<script language="javascript">document.getElementById("birth_month").value="<?php echo $month ?? '';?>"</script>
																</div>
																<div class="col-md-4"  style='padding-bottom:5px'>
																	<select name="birth_year" id="birth_year" class="form-control full-width-fix" data-msg-required="Please select year.">
																	<option value="">Year</option>
																		<?php
																			for($y = 1900; $y <= 2030; $y++) {
																				echo "<option value='".$y."'>".$y."</option>";	
																			}
																		?>
																	</select>
																	<script language="javascript">document.getElementById("birth_year").value="<?php echo $year ?? '';?>"</script>
																</div>
														</div>
													</div>
													<div class="form-group">
														<label class="control-label col-md-3">Medicare Number: <span class="required" >*</span></label>
														<div class="col-md-4">
															<input type="text" class="form-control required medicare integer" name="patient_medicareno" id="patient_medicareno" maxlength="10" onblur="auto_fill(this.value)"  value="<?php echo $Medicare_no ?? '';?>" data-msg-required="Please enter  medicare number.">
															<!-- <span class="help-block">Enter Medicare Number.</span> -->
														</div>
													</div>
													<div class="form-group">
														<label class="control-label col-md-3">Reference Number: <span class="required">*</span></label>
														<div class="col-md-4">
															<input type="text" class="form-control required referno integer" name="patient_referno" id="patient_referno" maxlength="1"  value="<?php echo $Reference_no ?? '';?>" data-msg-required="Please enter  reference number.">
															<!-- <span class="help-block">Enter Reference Number.</span> -->
														</div>
													</div>
													<div class="form-group">
														<label class="control-label col-md-3">Expiry Date: <span class="required">*</span></label>
														<div class="col-md-4" style="padding-left:0px;width:360px;">	
														<div class="col-md-4"  style='padding-bottom:5px' >
															<!-- <input type="text" class="form-control datepicker1 required" name="patient_ref_expiry" id="patient_ref_expiry"  value="<?php echo $Expiry_date ?? '';?>" data-msg-required="Please enter  expiry date." readonly> -->
															<!-- <span class="help-block">Enter expiry date.</span> -->

															<select name="expiry_month_medi" id="expiry_month_medi" class="form-control required" data-msg-required="Please select expiry month."  style="width:100px">
																<option value="">Month</option>
																<?php
																	for($m = 1; $m <= 12; $m++) {
																		if(strlen($m)==1)
																			$m = "0".$m;
																		echo "<option value='".$m."'>".$m."</option>";
																	}
																?>															
																</select>
															<script language="javascript">document.getElementById("expiry_month_medi").value="<?php echo $expmonth ?? '';?>"</script>
															</div>
															<div class="col-md-4"  style='padding-bottom:5px' >
															<select name="expiry_year_medi" id="expiry_year_medi" class="form-control required" data-msg-required="Please select expiry year." style="width:100px">
															<option value="">Year</option>
																<?php
																	for($y = 2013; $y <= 2050; $y++) {
																		echo "<option value='".$y."'>".$y."</option>";	
																	}
																?>
															</select>
															<script language="javascript">document.getElementById("expiry_year_medi").value="<?php echo $expyear ?? '';?>"</script>
														</div>
														</div>
													</div>
													<div class="form-group">
														<label class="control-label col-md-3">I prefer to see: <span class="required">*</span></label>
														<div class="col-md-4">
															<select name="preffered_doctor" id="preffered_doctor" class="select2 full-width-fix required" data-msg-required="Please select prefered doctor." >
																<option value=""></option>
																<option value="0" <?php echo ($Doctor_id == 0 || $Doctor_id == "") ? " selected": '';?>>First Available Doctor</option>
																<?php
																	$doctQuery = "select * from tbl_staff where location='".$_SESSION["location"]."' and staff_name != ''";
																	$doctResult = $DBCONN->query($doctQuery);

																	foreach($doctResult->fetchAll(PDO::FETCH_ASSOC) as $docRows) {
																		$doctorId = stripslashes($docRows['staff_id']);
																		$doctorName = stripslashes($docRows['staff_name']);
																?>
																
																	<option value="<?php echo $doctorId; ?>" <?php echo $Doctor_id == $doctorId ? "selected" : '';?>><?php echo $doctorName; ?></option>
																	<?php
																	}
																	?>	
																
															</select>
															<!-- <span class="help-block">Please select prefered doctor.</span> -->
														</div>
													</div>
													<div class="form-group">
														<label class="control-label col-md-3">Address: <span class="required">*</span></label>
														<div class="col-md-4">
															<textarea class="form-control required" rows="3" name="patient_address" id="patient_address" data-msg-required="Please enter address." ><?php echo ucfirst($Address ?? '');?></textarea>
															<!-- <span class="help-block">Enter address.</span> -->
														</div>
													</div>
													<div class="form-group">
														<label class="control-label col-md-3">Suburb: <span class="required">*</span></label>
														<div class="col-md-4">
															<input type="text" class="form-control required letterswithbasicpunc" name="patient_suburb" id="patient_suburb" value="<?php echo ucfirst($Suburb ?? '');?>" data-msg-required="Please enter suburb.">
														<!-- 	<span class="help-block">Enter suburb.</span> -->
														</div>
													</div>
													<div class="form-group">
														<label class="control-label col-md-3">Postcode: <span class="required">*</span></label>
														<div class="col-md-4">
															<input type="text" class="form-control required postcode integer" name="patient_postcode" id="patient_postcode" value="<?php echo $Post_code ?? '';?>" data-msg-required="Please enter postcode." maxlength="4">
															
														</div>
													</div>
													<div class="form-group">
														<label class="control-label col-md-3">Home Phone: </label>
														<div class="col-md-4">
															<input type="text" class="form-control homephone integer" name="patient_homephone" id="patient_homephone" maxlength="10" value="<?php echo ($Home_phone == "" ||$Home_phone == "0") ? "" : $Home_phone;?>" >
															
														</div>
													</div>
													<div class="form-group">
														<label class="control-label col-md-3">Mobile Phone: <span class="required">*</span></label>
														<div class="col-md-4">
															<input type="text" class="form-control required medicare integer" name="patient_mobile" id="patient_mobile" maxlength="10" value="<?php echo $Mobile_phone ?? '';?>" data-msg-required="Please enter mobile number.">
															<!-- <span class="help-block">Enter mobile number.</span> -->
														</div>
													</div>
													<div class="form-group">
														<label class="control-label col-md-3">Centrelink HCC/Pension Card No: </label>
														<div class="col-md-4">
															<input type="text" class="form-control" name="patient_hcc" id="patient_hcc" maxlength="10" value="<?php echo $Pension_card_no ?? '';?>" data-mask='999999999a' data-msg-required="Please enter valid pension card number." style="text-decoration:none">
														<!-- 	<input type="text" class="form-control" name="patient_hcc" id="patient_hcc" maxlength="10" value="<?php echo $Pension_card_no ?? '';?>" data-mask="99999999a"> -->
															
														</div>
													</div>
													<div class="form-group">
														<label class="control-label col-md-3">Expiry Date:</label>
														<div class="col-md-4" style="padding-left:0px;">
															<!-- <input type="text" class="form-control datepicker" name="hcc_ref_expiry" id="hcc_ref_expiry" value="<?php echo $Pension_card_expiry_date ?? '';?>" readonly> -->
															<!-- <span class="help-block">Enter expiry date.</span> -->
															<div class="col-md-4"  style='padding-bottom:5px'>
                                                           <select name="hcc_ref_expiry_date" id="hcc_ref_expiry_date" class="form-control required" data-msg-required="Please select date." style="width:100px">
																<option value="">Date</option>
																<?php
																	for($d = 1; $d <= 31; $d++) {
																		if(strlen($d) == 1)
																			$d = "0".$d;
																		echo "<option value='".$d."'>".$d."</option>";	
																	}
																	?>																
																</select>
																<script language="javascript">document.getElementById("hcc_ref_expiry_date").value="<?php echo $hccday ?? '';?>"</script>
																<!-- <input type="text" class="form-control datepicker1 required" name="patient_ref_expiry" id="patient_ref_expiry" readonly data-msg-required="Please select expiry date."/> -->
																
															</div>
															<div class="col-md-4" style="float:left;padding-bottom:5px">
																<select name="hcc_ref_expiry_month" id="hcc_ref_expiry_month" class="form-control required" data-msg-required="Please select month." style="width:100px">
																	<option value="">Month</option>
																	<?php
																		for($i = 1; $i <= 12; $i++) {
																			if(strlen($i) == 1)
																				$i = "0".$i;
																			echo "<option value='".$i."'>".$i."</option>";	
																		}
																	?>
																	
																</select>
																<script language="javascript">document.getElementById("hcc_ref_expiry_month").value="<?php echo $hccmonth ?? '';?>"</script>
															</div>
															<div class="col-md-4" style="float:right;padding-bottom:5px">
																<select name="hcc_ref_expiry_year" id="hcc_ref_expiry_year" class="form-control required" data-msg-required="Please select year." style="width:100px">
																<option value="">Year</option>
																<?php
																	for($y = 2013; $y <= 2050; $y++) {
																		echo "<option value='".$y."'>".$y."</option>";	
																	}
																	?>
																</select>
																<script language="javascript">document.getElementById("hcc_ref_expiry_year").value="<?php echo $hccyear ?? '';?>"</script>
															</div>
														</div>
													</div>
												  <!--   <div class="form-group">
														<label class="control-label col-md-3">Status: <span class="required">*</span></label>
														<div class="col-md-4">
															<select name="patient_status" id="patient_status" class="select2 full-width-fix required">
																<option value=""></option>
																<option value="Apointment fixed" <?php if($Patient_status=="Apointment fixed"){echo "selected";} ?>>Apointment fixed</option>
																<option value="Visited" <?php if($Patient_status=="Visited"){echo "selected";} ?>>Visited</option>
																<option value="Canceled" <?php if($Patient_status=="Cancelled"){echo "selected";} ?>>Cancelled</option>
																
															</select>
															<span class="help-block">Please select title.</span>
															</div>
													</div>	 -->
												
											

																
									<div class="form-actions">
									  <input type="button" class="btn btn-primary pull-right" onclick="document.location='<?php if(!isset($_GET['redirec']) && $_GET['redirec'] != 'pq'){ echo "manage_patients.php";}else{ echo "patient_queue_screen.php"; }?>'" value="Cancel">
										<input type="submit" value="Submit" class="btn btn-primary pull-right">
									</div>
									
								</form>
							</div>
						</div>
					</div>
				</div>				
				<!-- /Page Content -->
			</div>
			<!-- /.container -->

		</div>
	
		
<?php
include("includes/footer.php");
?>

<script language="javascript">
function validateHCC(str)
{
var pattern = /[A-Za-z]{1}/g;

if (pattern.test(str)) {
	i=0;
	var numarray = new Array();
	var chararray = new Array();
	while(i<str.length)
	{
		if(isNaN(str[i]))
			chararray = str[i];
		else 
			numarray = numarray + ""+str[i];
		i++;
	}
	if(numarray.length != 9)
	{
		alert("Centrelink HCC/Pension Card No should have 9 numbers.");
		//return false;
	}
  } 
  else
  {
	  alert("Centrelink HCC/Pension Card No should have alteast one alphabet.");
		//return false;
  }
}
</script>