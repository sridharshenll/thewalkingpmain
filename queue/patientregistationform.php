<?php
include('includes/configure.php');
$msg=$_GET['msg'];
if($msg!=""){
	if($msg==1){
		$Message="Patient Information added successfully.";
		$class="success";
	}
}
if(isset($_POST["patient_name"])){
	$patient_name=addslashes(trim($_POST["patient_name"]));
    $address=addslashes(trim($_POST["address"]));
	$phone=addslashes(trim($_POST["phone"]));
	$dob=date('Y-m-d',strtotime(addslashes(trim($_POST["dob"]))));
	$medicare_no=addslashes(trim($_POST["medicare_no"]));
	$doctor=addslashes(trim($_POST["doctor"]));
	$getTokenQry="SELECT count( * ) as token_count FROM tbl_patient WHERE register_date = '".date('Y-m-d')."'";
	$getTokenRes=$DBCONN->query($getTokenQry);
	$getTokenRow=$getTokenRes->fetch(PDO::FETCH_ASSOC);
	$token_number=$getTokenRow["token_count"]+1;
	$insertQry="insert into  tbl_patient(doctor_id,patient_name,address,phone,dob,medicare_no,token_number,patient_status,register_date,created_date,modified_date) values('".$doctor."','".$patient_name."','".$address."','".$phone."','".$dob."','".$medicare_no."','".$token_number."','Apointment fixed','$dbdatetime','$dbdatetime','$dbdatetime')";
	$insertRes=$DBCONN->query($insertQry);
	if($insertRes){
		header("Location:patientregistationform.php?msg=1");
		exit;
	}
}
include('includes/header.php');
?>
<div id="content">
			<div class="container">				
				<!--=== Page Header ===-->
				<div class="page-header">
					<div class="page-title">
						<h3>Patient Registration Form</h3>
						<span>Please enroll the patients using the below form. </span>
					</div>					
				</div>
				<!-- /Page Header -->

				<?php if($msg!=''){
				?>
				<div class="alert fade in alert-success">
					<i class="icon-remove close" data-dismiss="alert"></i>
					<?php echo $Message; ?>
				</div>					
				<?php }
				?>

				<!--=== Page Content ===-->
				<!--=== Full Size Inputs ===-->
				<div class="row">
					<div class="col-md-12">
						<div class="widget box">
							<div class="widget-header">
								<h4><i class="icon-reorder"></i>Patient Registration Form</h4>
								
							</div>
							<div class="widget-content">
								<form class="form-horizontal row-border" method='POST' id="validate-1" action="">									
									<div class="form-group">
										<label class="col-md-3 control-label">Patient Name:<span class="required">*</span></label>
										<div class="col-md-9"><input type="text" name="patient_name" class="form-control required"></div>
									</div>
									<div class="form-group">
										<label class="col-md-3 control-label">Address:<span class="required">*</span></label>
										<div class="col-md-9"><input type="text" name="address" class="form-control required"></div>
									</div>
									<div class="form-group">
										<label class="col-md-3 control-label">Phone:<span class="required">*</span></label>
										<div class="col-md-9"><input type="text" name="phone" class="form-control required" data-mask="+9 (999) 999-9999"></div>
									</div>
									<div class="form-group">
										<label class="col-md-3 control-label">Date Of Birth:<span class="required">*</span></label>
										<div class="col-md-9"><input type="text" name="dob" class="form-control datepicker required" ></div>
									</div>
									<div class="form-group">
										<label class="col-md-3 control-label">Medicare Number:<span class="required">*</span></label>
										<div class="col-md-9"><input type="text" name="medicare_no" class="form-control required"></div>
									</div>

									<div class="form-group">
										<label class="col-md-3 control-label">Doctor:<span class="required">*</span></label>										
										<div class="col-md-9">
											<select class="form-control required" id="culture" name="doctor">
												<option value="">Select a Doctor</option>
												<?php
												$doctQuery = "select * from tbl_doctor";
												$doctResult = $DBCONN->query($doctQuery);
												foreach($doctResult->fetchAll(PDO::FETCH_ASSOC) as $docRows) {
													$doctorId = stripslashes($docRows['doctor_id']);
													$doctorName = stripslashes($docRows['doctor_name']);
												?>
												<option value="<?php echo $doctorId ?? ''; ?>" ><?php echo $doctorName ?? ''; ?></option>
												<?php
												}
												?>				
												 
											</select>
										</div>
									</div>

																
									<div class="form-actions">
									 
										<input type="submit" value="Submit" class="btn btn-primary pull-right">
									</div>
									
								</form>
							</div>
						</div>
					</div>
				</div>				
				<!-- /Page Content -->
			</div>
			<!-- /.container -->

		</div>
	
		
<?php
include("includes/footer.php");
?>